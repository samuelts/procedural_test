{
    "id": "bf8b5704-6b03-4b68-86bf-535730734305",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fDmg",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Corbel",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ef032c62-ccd7-461a-a33e-471cdd349d0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 229,
                "y": 86
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c3272adc-54ef-4369-bdb7-80ff0d4ee3b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 10,
                "y": 114
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "76b56d44-9028-49cb-847b-5a5b41831340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 147,
                "y": 86
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f7ee8638-e336-4a54-871a-746ca8bb7990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7af1a3ec-ecf9-4ece-8c1b-c2448a1d5c5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 145,
                "y": 58
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d5ba4a05-c4fa-4775-90f7-90e36d8137b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "51ff3c18-9968-4a68-8da4-fc1e6e340ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "dc6873bb-32bf-44b2-b2cc-04ed690392ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 251,
                "y": 86
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "cf790f10-4e05-4985-881e-8bfb17868f35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 194,
                "y": 86
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2f260808-6357-4f20-8ce0-d63ec2038b7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 222,
                "y": 86
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "57f73f32-a227-4bcc-b24d-43a1c5e425c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 178,
                "y": 30
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "aa31835f-53c6-4348-8b8e-2151008bb12b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 233,
                "y": 58
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "32e2ccf0-78c2-4f24-ba53-7f1dbd372e01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 241,
                "y": 86
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "671fc009-458b-4aac-be96-8747875fa3b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 201,
                "y": 86
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ef112a8c-6d3a-46e6-af0b-da8414e27bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4581cc9f-34cc-474f-a78e-c5b19eb5b20f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 139,
                "y": 86
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fe31d402-4f35-430e-b9b9-bce37ac63fbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 112,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "bd472493-d06f-4c12-ab34-ff9fb8a4c5c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 86
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7ded9cc4-96b6-4611-b57f-c0ba2f788b56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 123,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "69dda8b0-c598-400b-a254-0eb1eda6021b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 134,
                "y": 58
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "50454b11-a174-452b-8056-efc6b24325bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 166,
                "y": 30
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c5fb7eb5-8fb7-46da-9775-178dc0a00dc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 154,
                "y": 30
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5e856f5c-87cb-48c0-84d9-a03ea1b214ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 156,
                "y": 58
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "836939d6-114a-4850-a2aa-7ca7a7ed726d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 58
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "42e86e5f-b346-4627-87b8-1f4f7530621c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 189,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d7f5c005-edc9-404c-97f7-65b7c914aa8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 178,
                "y": 58
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3291fbee-d84f-4c98-9acd-00fb374c6fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 14,
                "y": 114
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7ad3d111-1def-40ab-8b1a-9c57a2852395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 235,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b8e6e878-c99e-4952-a9a6-6179190e7984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1ba51e96-c015-4d97-b1e0-3740e1110bf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 200,
                "y": 58
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5a9570b8-a2cc-4fad-a9f1-4ba63ea44de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 211,
                "y": 58
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8d9497df-fc21-4a89-a12c-935c67f6ecc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 44,
                "y": 86
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "19a54753-4eab-457b-abb5-fc440b14341b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c3007e1d-e41b-4954-bbd9-884110de5abb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "11a4f78f-36af-4f15-847f-84cf7a74274a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 118,
                "y": 30
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "93041a35-c222-4304-a193-11a06d0773fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 142,
                "y": 30
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e852dc27-5c94-4873-96bb-bf5e250c5a1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 81,
                "y": 30
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "113f0ce7-f7a5-4585-bf7b-6f4313f37a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 222,
                "y": 58
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "eab6cc84-26b4-4b0c-8c67-93cc517dac41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 244,
                "y": 58
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e5982090-8fa2-4c1b-9e15-ef5013221d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3d41cadb-c539-428d-8bff-6e439180d9cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 130,
                "y": 30
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0d2b0fc9-f006-4561-9179-62420b72b558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 18,
                "y": 114
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "48e0f5ca-118f-4979-94f0-81f4bc8adcf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 171,
                "y": 86
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0f335303-a5d6-4020-8098-df9992036685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 226,
                "y": 30
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "74b4e7d9-5eaf-492c-844d-764d044d526a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 58
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a434d8fc-4bcf-45ab-8f4e-253f48bca054",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "43b26d40-eca3-48e4-b2a7-a43188c60a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 68,
                "y": 30
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8f850321-f6bb-4bd2-81e3-0cc4ccab9135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "362ec481-6483-446a-a03b-efa78fdf31fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 68,
                "y": 58
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "60c5cbd5-a511-4708-88e4-41079c4928ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5cb79b91-507e-4fcf-9473-96ec01b11b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 106,
                "y": 30
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "625d4b62-7efa-45ec-ac8b-94134542b68d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 94,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b16ba433-5c5f-45c8-83ec-c284a4386b75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e313b37b-3bcf-42eb-968c-7b2dc8a62c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 29,
                "y": 30
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a6d4d274-f862-4afe-a1f2-88e85ee58f59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "dcb888ee-20c5-4543-bc7e-bf373ad1a3f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e5b8192c-e52d-4dc2-8d8d-f8d102246b89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bbde2068-370f-49b8-a317-44f66a691b7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a91b6e26-9fb7-4512-bb6f-4e95ea252942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 16,
                "y": 30
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b9247d94-3c0b-4e96-8a6e-bdce2b55c5c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 208,
                "y": 86
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6c0196bc-3b5c-43be-be72-bd24b4a0a621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 122,
                "y": 86
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "821cbd9b-6f1c-4ba1-8982-08c69c7eb422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 187,
                "y": 86
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e9323e10-6e51-4eb6-9f94-4376dc9bdd5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 42,
                "y": 30
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "54657ecc-84c7-41aa-a9ca-bc0b2c453311",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 55,
                "y": 30
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ef02f571-8b2b-413e-8a8b-86d659da7156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 179,
                "y": 86
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6033b59d-55a4-4848-87c8-7709b4a035f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 84,
                "y": 86
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "95042945-b7a8-49cb-bc4c-232acb55f2c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 94,
                "y": 86
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "cdbee340-7238-4382-a628-74d01ffbe833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 34,
                "y": 86
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "79c141ba-a866-4dbf-87bd-871c5c21a0ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 238,
                "y": 30
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "73194895-56a8-4492-8054-5f97809e435c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "59d96f55-8213-48db-b08c-d6845e575283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 74,
                "y": 86
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4198a567-f76c-4463-938b-af145ac3ae75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 58
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "451958cd-3df9-480d-adf9-b5af923d6f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 64,
                "y": 86
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "730f34e4-425f-479b-9277-edb4980b3e74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 6,
                "y": 114
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f787b259-4cbe-4fb1-8e7b-99a23cdf3f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 155,
                "y": 86
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f899c2be-5ab8-4321-b81d-96bab9db187c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 2,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 58
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "97e50cea-772d-4684-a0ae-5de718a061c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 247,
                "y": 86
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "59e716f4-34ca-44f7-93b8-e68440aa916d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "47a8c8b1-4d8e-4f40-aaa2-58906eb08528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 54,
                "y": 86
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "97a0efaf-bddd-43c4-9cbc-04ae2c7ec782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 58
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8bf6cc8e-5ad8-42b5-8cb4-22343458ec50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 46,
                "y": 58
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "303957ae-6376-4819-9453-17cdfaa8525c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 57,
                "y": 58
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1473b155-b6b6-4487-a3e7-9015a2ad52c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 2,
                "shift": 7,
                "w": 6,
                "x": 131,
                "y": 86
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b660b33d-8cbe-4f29-a5c9-bcd3f98f7ab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 113,
                "y": 86
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b688a440-fcce-4f75-aa72-4fa0e9e6060e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 104,
                "y": 86
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "81a2c59e-5c83-45a7-ad46-ad633272e2e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 79,
                "y": 58
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b5897dd3-cc0a-446f-b106-36d0d4852f3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "794fdda0-f073-4b32-a3c6-076c9ca7a6cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "80ac2e1a-6dad-419a-b704-28c51d873396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 202,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7b4d97d5-2807-4484-98ad-c008f672e997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 214,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "499d5e19-18fc-4735-a28e-f66b97b7154a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 24,
                "y": 86
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "124250db-e172-45ec-951e-47f09db8949a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 215,
                "y": 86
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "723e9a4f-37a5-4a91-9061-0c1eef2264fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 2,
                "shift": 5,
                "w": 1,
                "x": 22,
                "y": 114
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "59562d2f-42cc-4532-8252-409b52e00e08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 163,
                "y": 86
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9277cdd0-6039-4b46-9224-e26dc7041594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 101,
                "y": 58
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "a1896300-49e9-4a9e-99f1-ae8d945e1beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "7d23ca56-86c0-44f3-a2e1-901d3218b883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 67
        },
        {
            "id": "30ec77f3-6842-4470-aef5-68dbbe51d9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 71
        },
        {
            "id": "b7f9cb08-46d2-4371-92ca-f8e696419130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 79
        },
        {
            "id": "8f49ef5b-b842-42cb-bb0a-f1991cbd59a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 81
        },
        {
            "id": "54376a9b-76dd-4847-bddf-f8e22364870f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "22c9c0aa-23e3-44db-8a4c-13e2c9679d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 85
        },
        {
            "id": "02293327-42b6-409f-9c7b-1347c0deaf9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 86
        },
        {
            "id": "1fe2e07f-2b8f-4379-a026-27d3c7c73d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 87
        },
        {
            "id": "1e96d584-8439-4991-b9f2-e78ddd69d6a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 88
        },
        {
            "id": "114c757f-30da-400a-b27b-d090dd75257a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 89
        },
        {
            "id": "e417d7c4-6c7a-4f64-b0ae-be65bea1aa32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 90
        },
        {
            "id": "14d597d0-978a-460e-83e3-8a284488c3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 192
        },
        {
            "id": "ffb61f03-8cc1-42af-94be-dd99920a48f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 193
        },
        {
            "id": "48cac68b-d6e0-45d0-a34d-d754bf0b322b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 194
        },
        {
            "id": "5987f701-b6c9-4c25-8509-8c8b62abd91a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 195
        },
        {
            "id": "2dd8b1b0-0357-4d9e-b229-66fdf5c2289c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 196
        },
        {
            "id": "7e6f5a6f-a634-40cd-b6af-8e5da6811259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 197
        },
        {
            "id": "a2be5f92-2c42-4574-817c-e1ac0ca56ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 198
        },
        {
            "id": "9acf2653-47f1-480a-a95f-7ac669363767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 199
        },
        {
            "id": "e75318ed-f715-4264-b57c-34ce8de242de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 210
        },
        {
            "id": "b23a891b-a560-412a-bd30-5f2988462c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 211
        },
        {
            "id": "288ca54f-a171-4877-a856-b1ec9950d47b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 212
        },
        {
            "id": "cbf63559-692d-46d3-8e8c-6dc75789c027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 213
        },
        {
            "id": "4918ce9a-dce0-4cef-af04-63dece835a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 214
        },
        {
            "id": "94491581-7cc6-49ba-84ef-624aa16467aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 216
        },
        {
            "id": "4e4331a9-afc9-4d52-b5aa-0f0a4ffa0721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 217
        },
        {
            "id": "427a23fa-ed6c-486f-a449-0aef14a3cd90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 218
        },
        {
            "id": "b2ffeee7-4cde-41e9-bb64-5741cfb70018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 219
        },
        {
            "id": "8529f8c3-c442-4e66-befd-18f65d719ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 220
        },
        {
            "id": "f7ecd093-b8bf-4fde-8a45-e3b22a56bfc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 221
        },
        {
            "id": "fa7ba041-5d49-4eb5-9826-a4e8853b3d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 256
        },
        {
            "id": "228add86-44c2-4a5f-8079-8a0c5374333b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 258
        },
        {
            "id": "c7a8cd76-e112-4e70-85a3-848ebce828f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 260
        },
        {
            "id": "6e696cf6-0274-47e4-96da-ebef97e89cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 262
        },
        {
            "id": "e3b05f1b-5523-479b-adcf-7c0b85593972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 264
        },
        {
            "id": "c8bf7cf0-b5d6-47c9-9a8d-1068ec68452b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 266
        },
        {
            "id": "ee263b90-85c0-4fe9-84d3-5129ed3f1251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 268
        },
        {
            "id": "e3bbd6cf-f468-4b24-b97f-e9e22f358e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 284
        },
        {
            "id": "7743fea6-786d-4273-91da-dd17d179f65e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 286
        },
        {
            "id": "10eb2739-0f9a-40fd-9018-6f8a0ef0dc5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 288
        },
        {
            "id": "caf40ce1-fb1f-4393-8daf-34983ad72b1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 290
        },
        {
            "id": "ffea6a59-9a0a-42d8-9eba-d4abeab02372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 332
        },
        {
            "id": "1cac12e4-8979-4f63-ba86-398c6f91befb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 334
        },
        {
            "id": "99d421e3-1b16-440a-8598-5b698c5c4ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 336
        },
        {
            "id": "325e9055-3c26-421b-af35-1f7a2c9f49bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 338
        },
        {
            "id": "5a7c803e-dd6c-495d-baac-fd0bc5b9bdf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 354
        },
        {
            "id": "3ba15bfc-8f23-470e-8a0b-69b8dd74dddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 356
        },
        {
            "id": "e47ffc6b-db37-4069-bb30-874c5860ae93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 358
        },
        {
            "id": "72e30594-98f1-4392-a7fd-6ce00faa7461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 360
        },
        {
            "id": "c98445e9-204a-49ec-b866-3402ae3608c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 362
        },
        {
            "id": "d1a7ab2c-49c7-4a5f-a6ec-1e7fe38824d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 364
        },
        {
            "id": "fdf65fa8-b8cd-4809-b87d-a2275088631e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 366
        },
        {
            "id": "5093d23b-d115-43dc-8e3a-8cdf8367f23d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 368
        },
        {
            "id": "c4bae738-efb7-4cdc-bde2-979e38018a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 370
        },
        {
            "id": "5792ed73-61df-49ab-8ee4-ef5430e1eacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 372
        },
        {
            "id": "64990336-3bac-412d-8d86-a07e763df14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 374
        },
        {
            "id": "07f547dd-fbdd-4df9-a362-8d261a6a0b3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 376
        },
        {
            "id": "9a4a0860-153d-4d7b-b36a-86d6038de4e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 377
        },
        {
            "id": "eb219f4e-41bb-4283-9985-bc2ae53e9274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 379
        },
        {
            "id": "618518d7-a8d8-4e40-b0e3-94b701d177e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 381
        },
        {
            "id": "f6b67309-9838-4e46-a767-fc6a4fc73a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 416
        },
        {
            "id": "d541c814-5dc4-4345-872f-84921e991eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 431
        },
        {
            "id": "6b750b95-ed36-479e-b9d1-4eb512f7dc47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 506
        },
        {
            "id": "8871becc-5338-4d6c-a87b-5f1687c131b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 508
        },
        {
            "id": "1870e404-cdd6-43cc-b54d-72dbbb0e5faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 510
        },
        {
            "id": "8607dab3-6a69-4422-9157-9a5ee371e92b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 538
        },
        {
            "id": "8735f64a-a7c3-4f9e-9bf6-f67a45578abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 904
        },
        {
            "id": "d2c4ea11-e3d1-4f4e-bc9d-c38cbb91385b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 905
        },
        {
            "id": "b7c3ca7a-b356-4828-94fa-7ff3cc9dbd4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 906
        },
        {
            "id": "21ea39ea-4c32-4110-b65b-d55c55f8104e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 908
        },
        {
            "id": "09f1ced3-c11b-4faf-a48b-9f24c45a3f2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 910
        },
        {
            "id": "842c1459-6c0b-4f3e-bdba-7d9797fadbdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 911
        },
        {
            "id": "b3f475f5-010d-4d18-ad01-f05f8c269ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "31825307-6a35-48ad-9cda-b27f0ba25d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "377dc2a0-0445-4ae9-9857-99c681c94488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 918
        },
        {
            "id": "a2fe6b35-ac72-4a62-842f-5ac3c56ae393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 920
        },
        {
            "id": "181756c3-5985-41eb-8676-b4e2679c3b70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "751f6ce1-1560-4b1a-9c81-04f83f010208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 927
        },
        {
            "id": "ef2035b6-045b-4b02-adab-c165740eab65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 931
        },
        {
            "id": "c723b4d4-8dfe-4252-aab2-9079e76def6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "10f76a00-00e6-4c46-8734-9566419978c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "eb963a64-268e-4a19-9a65-ceb030b0dd0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 934
        },
        {
            "id": "e1b3df86-70f9-44a6-838d-3375ac7e7488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 935
        },
        {
            "id": "7a91b00e-fc0b-40e9-b954-036f7873ea68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 936
        },
        {
            "id": "61c74c09-f0ec-49f0-907b-f86bdbdc2ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 937
        },
        {
            "id": "1a5662ee-50ec-4f58-ac92-7035af357d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "63de4582-150c-4adf-915d-4979d16e1515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1026
        },
        {
            "id": "d7498f7f-56d5-4046-94eb-6baae751769a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1028
        },
        {
            "id": "f28a345d-29e0-43d0-862a-df9e1552d637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1033
        },
        {
            "id": "7cc5e300-cf79-46d0-b4cd-01cae0286ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1035
        },
        {
            "id": "19e774a6-db29-46ed-a258-9f783fa3e261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1040
        },
        {
            "id": "fd2a59b3-5397-4775-8086-5efdaa137852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1044
        },
        {
            "id": "b965c411-f96c-4cd2-bb29-1090f8a636a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1051
        },
        {
            "id": "c247cb9a-2dce-431e-98b1-ffc31f3ca81a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1054
        },
        {
            "id": "f9b18e1f-6e5d-46fc-9be3-c87f952669dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1057
        },
        {
            "id": "f2fa581a-a04e-4a23-8bfb-e9f394bfc89e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1058
        },
        {
            "id": "ad402926-2923-4129-83c5-ef44c6501efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1060
        },
        {
            "id": "e709cbc1-2c7e-4155-b4f4-71a054787abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1061
        },
        {
            "id": "bd5d1bd0-9428-4fea-a170-ea4974938f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 1063
        },
        {
            "id": "13936dd3-746d-4547-ae05-66e78c146023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1066
        },
        {
            "id": "37d2f701-3a7d-43c4-b11d-6fa7cb611c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1122
        },
        {
            "id": "8ed7de1f-91fa-42d5-a21b-b31cfc90fb12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1140
        },
        {
            "id": "b3dd3a0d-cff2-4efd-982e-3377eba19a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7808
        },
        {
            "id": "34af709e-61a7-4bc5-83e2-bf25d3c5f04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7810
        },
        {
            "id": "8f9b51d7-a6d5-407c-a45c-09429a48dc4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7812
        },
        {
            "id": "b038410f-71fa-4cd7-b5ec-63a83c2f98ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7840
        },
        {
            "id": "7eb6198e-072f-40f7-a41a-a3419d700cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7842
        },
        {
            "id": "6dc0558e-c179-4751-8a27-c42744145dba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7844
        },
        {
            "id": "267de250-854b-4148-a60d-34381b1c158f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7846
        },
        {
            "id": "bbcee08c-0f85-4076-9b67-6c1990d26030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7848
        },
        {
            "id": "6b307915-8738-4c1d-bed2-d898ed50568d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7850
        },
        {
            "id": "b567cf64-9c32-4af1-82dd-6dc89a3ec2b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7852
        },
        {
            "id": "9c933608-68d5-4ef3-b769-82ca4b0f6a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7854
        },
        {
            "id": "5b2009db-1bd6-457d-ab41-e9089d80d05f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7856
        },
        {
            "id": "18f2aa0c-6ce5-47dc-a369-b4a649a03b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7858
        },
        {
            "id": "59b30890-e756-4d93-9a6f-25af72629cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7860
        },
        {
            "id": "d9cf195c-7bf9-4784-bcf8-9891888c4836",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7862
        },
        {
            "id": "615deabc-1d33-4b41-ae33-9d33ea5e3db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7884
        },
        {
            "id": "ab5647e4-ecf6-42bc-91ce-016cb120664a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7886
        },
        {
            "id": "659620fb-55d5-413a-8975-e99699571562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7888
        },
        {
            "id": "786e2055-d809-458a-9e8c-a791f9d70869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7890
        },
        {
            "id": "3abd7660-085f-4cd0-a4d6-30c43a7fae8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7892
        },
        {
            "id": "d5e4f044-67ea-492d-a665-6a70abee2be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7894
        },
        {
            "id": "b9bb1adc-6ce1-4e5f-808e-8f2914104759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7896
        },
        {
            "id": "587b25bb-4f71-44cd-bda4-18cb6082eef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7898
        },
        {
            "id": "3f30fe7b-aa5c-43a8-ac66-062d78c8d1be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7900
        },
        {
            "id": "4d6a18b8-66e2-406b-9ff5-48c8f3a57f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7902
        },
        {
            "id": "dddfc256-b769-417b-a8bc-725d4d8d71ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7904
        },
        {
            "id": "cca44ac2-9e1c-43a1-b8b0-cf4608fc7425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7906
        },
        {
            "id": "17360243-3827-4a8d-ba9f-ae9ad93db080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7908
        },
        {
            "id": "76bd5a95-3185-44d4-93a5-c709d69b7fe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7910
        },
        {
            "id": "e2c77629-3e94-4ef2-b660-7d0f182923e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7912
        },
        {
            "id": "4ea07db8-db9f-4520-b737-d1199af86287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7914
        },
        {
            "id": "53ce6972-1afe-4efd-8aa8-3b60c311e4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7916
        },
        {
            "id": "4f08712f-10a2-4c1f-982f-0bb67eaa5cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7918
        },
        {
            "id": "2db07113-54ad-4aba-bef1-a1277c7dc94e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7920
        },
        {
            "id": "b2093652-ec68-4887-96a8-71a488ce74f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 7922
        },
        {
            "id": "5d1ee080-4967-4949-9c2f-675af5845780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "d52968f8-e890-40d8-9496-08ede54ae5d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "2897a1d8-583b-49ba-a2f2-6ebcfb39fc8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "2ae31d34-0da8-419d-a2de-1a1bcf8206b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "110b3000-ff4e-4ad0-8328-294a0d015a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "a5fa569b-84f6-4d5b-9bb5-2c78fff4f7d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "2cc0c599-a656-4103-a5d9-346f19fa0395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "9844477b-90e8-4160-912b-62115f259b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 354
        },
        {
            "id": "89ae4100-de2f-42b5-b33f-5ff9a53222f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "8b18cf71-f55f-4ac2-9227-70709eb99d92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 358
        },
        {
            "id": "db15aa72-7d98-4e84-9cbc-3ea0036f4922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "4e3af868-c17d-4621-aaaf-2fe59dad11a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "24ad1f30-0ec3-4e43-9c15-3c7353311ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "5c3fa85b-ad9e-4a7c-9788-668a36371b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "81175037-f590-4bfd-9d96-93b1c04b7dd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7808
        },
        {
            "id": "f8db8752-091a-4092-a356-7e47ffb7ce3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7810
        },
        {
            "id": "daefa266-1602-4311-8c13-c6073458b1f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7812
        },
        {
            "id": "7b23e24b-7709-4818-8dbf-fc68142200c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "bcd428f9-6a5e-448a-a35e-27d86cc7671f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "a93df311-a338-4252-bafd-7d8cd09c3a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "d5f0e64c-e456-4429-92f8-ef60bb4c0b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "b688f3bb-b5ce-4d33-bc2e-5aac956a125f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "66ff597b-0e70-400f-8c4b-48faf87d0c11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8319
        },
        {
            "id": "e00b3950-a4b8-45b9-a202-230ab38a3b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "c6d34f58-91e2-4219-b438-99227e082ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 119
        },
        {
            "id": "c714ce6e-b309-4b78-a209-7ce18e0171f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 121
        },
        {
            "id": "b4539a9d-7052-4598-9f12-b25f1f621690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 253
        },
        {
            "id": "f95cefca-a3a4-4e52-bed4-e15c0e27afb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 255
        },
        {
            "id": "98b51e40-e8ed-4a34-8878-85e282b012b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 373
        },
        {
            "id": "e5a45396-fd23-41fd-9b07-33b5a0769591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 375
        },
        {
            "id": "6b556e2b-290d-4542-9efb-8aeb96eb4d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7809
        },
        {
            "id": "42d95168-a5bc-4d99-bb33-af4322403f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7811
        },
        {
            "id": "8dfba29f-cb39-4bb8-925c-d7b180fd8031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7813
        },
        {
            "id": "c148d3fd-8bc0-43b0-bcb9-5643e571f1c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7923
        },
        {
            "id": "51bac79c-5322-4d5c-ab5d-3c384aaf2ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7925
        },
        {
            "id": "08e8f1a8-70eb-4ede-b4a6-affeb7f630f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7927
        },
        {
            "id": "722a73fe-6740-41b3-8f5c-68617a78659c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7929
        },
        {
            "id": "1adff2fc-c949-4f2c-bb8e-1685b10a4f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 41
        },
        {
            "id": "4d792fa2-f72e-4c48-89a4-08f24e67b818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "cb25365d-470e-4ebe-99e5-7282de55e40f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 93
        },
        {
            "id": "87ac863e-f058-4242-bc31-6584bd4f8530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 308
        },
        {
            "id": "6b61972c-1f2d-4440-bd44-083f4ebbfa12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "e8a85701-a80e-4c32-902a-be913c517258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "50b9d064-7a22-41b0-9587-0a178289062d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "de51bb43-abeb-4043-a1d7-d7149d8bf113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "ad5732e9-7203-4383-85b3-90b01d78aefa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "76fce7b6-109f-4605-b9bc-35e037f838dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "d9f13a65-1ddc-4102-ba94-e14f7ff3f05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "55adaa87-0939-47b3-9b7b-8f68e837568b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "3b4ca064-0618-4600-9923-3766da05d165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "65111f9c-cbb5-44c3-8f9a-3f40919baf57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "32dcf3cc-ef0b-4003-ad96-e63002fa29a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "1e5bee06-f8b1-4f55-b34e-52b1df219f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "8f8db231-7fd5-47e3-874b-891d6dbf9875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "764cd194-c530-4bf1-a789-0ec66cdbd03d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "6baa0028-9a2a-46c6-80a0-a3d9f847d55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 297
        },
        {
            "id": "3b9b08cf-a6e7-4f96-9161-203321a9c498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 301
        },
        {
            "id": "651391a5-dbe0-4b8c-a67d-c06e1135e8a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "39b9b8d9-6d07-420f-8438-61360acbb961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "582d8a88-658f-4e6a-a9c5-e97e3735e5c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 508
        },
        {
            "id": "b7caaf22-ebbd-45f3-8761-92e897ca061a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "2d59f1ee-1da8-465a-a1e1-c1f87756adfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "48b17489-63f8-4635-8ccf-79f97bdae4db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "2fc08311-3191-41a9-a485-dfee24ba66fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "792be90f-5c82-40df-a9d1-34399b58824a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "becaf311-9132-4285-8f40-4b43b1320a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "a5667e56-db7b-467a-9956-592646805ee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "92a7e5d8-4296-426d-8099-d1657f0b7f1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "e5e9531e-3f2c-4c8f-87d5-bfcb81bc7caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "81656d4e-8ed9-4680-a1a8-9a0a8ed243f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "95a46db9-6236-4a8c-ac2f-84ca546fe545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "49a67adc-ddc5-4675-9aa3-4a4ca3bdcbcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "10ee858c-908c-464c-abdf-9275cc32507f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8230
        },
        {
            "id": "2d85a334-dab8-421f-9082-d69633b3b036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "f857b4a5-c3a9-4fdd-b04b-ef0e8686f5e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "69bf3eca-cdb8-4eca-bde2-818960e84160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "35c7a96a-85c0-4f12-a0c3-da90dccddbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "d936f88b-63ac-4ca0-844e-f968b68aecbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "58956da8-59b9-4856-8e25-e2fecd475d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "4b2789e7-5bc4-4b59-ac1f-fa101fcbbafe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "7a2c18f4-44fb-4d73-9c8f-91c95aa98b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "457eaaf3-74af-4b0e-83b3-5e0b3b0025b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 171
        },
        {
            "id": "5377eb93-017b-4a79-b6db-7e25380b95ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 173
        },
        {
            "id": "799e75c5-9cc6-42cc-a7f0-8b3a6690adf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "bf1b8de0-ed05-42a0-86bd-009654acbab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "334a63f6-16d1-4085-9a46-c9f2995bb33a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "2a62c887-aa2b-4472-85ad-08f507cd9f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "c9618fb2-432b-4ed2-8fc3-be54f6814136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "be706c77-66a4-4f53-a062-9edb0508d230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "30209806-ec6a-4ef2-a8bf-f58ea096244e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "c66e4d7b-c0ee-4d02-8305-d9aae4e9a4e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 253
        },
        {
            "id": "7094ae15-e3e8-418f-855a-ac88b181d3d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 255
        },
        {
            "id": "5f385f45-cb4e-4527-ad79-a4a32396805b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "54e3448f-c30c-41d2-861a-2884fa5832b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "e2b771a9-dc5f-44e4-846a-893c36a693cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "987cfe0d-64b2-4f13-b6a3-e697e02417af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "16f9bcdf-7e7b-44ef-8831-a7e203971909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "1d888bd9-f0a1-4dc7-8b1e-bb16eadc3d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "758cfffd-4261-494d-9b69-1273089c057a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "a17d1772-548b-471a-a203-48750c5e02c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "416291c5-6039-44e7-a91d-2d89ed88e6cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "df8499ec-2922-4a74-8bcf-bbf25a2c0ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "f1fd1307-ac67-4cb7-a69b-413acc26e4f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "1af65699-d0d8-4870-874b-e70c1629ebdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 373
        },
        {
            "id": "8b54d7e6-828c-4f5c-98f3-f4084367218c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 375
        },
        {
            "id": "d006c7f5-fca6-419e-9e53-ddff61114d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 416
        },
        {
            "id": "ca28550c-0c29-4c22-b7c4-eef9b204d5b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "9fc20f97-16db-48aa-b812-aac53713d851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7809
        },
        {
            "id": "70129409-47bd-4888-8122-a69c569a9cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7811
        },
        {
            "id": "829ed8b4-1ced-42f6-80c9-758a09195acb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7813
        },
        {
            "id": "cad949cd-018d-4007-a266-37c29c594fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7884
        },
        {
            "id": "709b832b-741f-42d8-ae2b-9c60254ff84a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7886
        },
        {
            "id": "e08065d9-ff68-44b9-8b4e-57ca64b556f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7888
        },
        {
            "id": "125ff757-181d-49b0-88c5-50d56ba20be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7890
        },
        {
            "id": "c54552e3-0ef6-4dc3-9ac1-fe5dff7a36e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7892
        },
        {
            "id": "fe008247-936d-4230-92e7-4d4a2ed547eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7894
        },
        {
            "id": "7bba8fdb-4476-4bd1-86fd-8c55a129a861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7896
        },
        {
            "id": "4bea4371-74c3-4dd3-9ee1-d51802e23cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7898
        },
        {
            "id": "bdb6fd4e-5e3a-415e-ab2f-1bc2deb66609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7900
        },
        {
            "id": "902dae65-b2cd-4e40-b909-fa39060a4185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7902
        },
        {
            "id": "8d219c8d-3db3-48c2-b9b1-89de8cc091c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7904
        },
        {
            "id": "23996447-cc7c-4430-a38b-0c7dc9064391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7906
        },
        {
            "id": "b87a96a0-3473-4822-9a2f-9dceb8a08a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7923
        },
        {
            "id": "6389e6ef-d5e2-4c47-8cae-6be6c0edd11a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7925
        },
        {
            "id": "108ccc0d-e9fe-4b62-816a-5d74a0eeae56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7927
        },
        {
            "id": "e3f6a50a-368c-4e20-8457-daaffec5bec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7929
        },
        {
            "id": "2e3c7611-fd7f-47ea-b9a2-bc268aae12e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8208
        },
        {
            "id": "c4fff2b9-0c8e-48f3-9b94-9780e2ddb732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8211
        },
        {
            "id": "1e5d1194-cc7c-445d-9b64-3faafdc1795b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8212
        },
        {
            "id": "bf88bdf3-2780-4074-8460-1571e5d1a880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8213
        },
        {
            "id": "c130a750-70ac-40f1-893c-89db6cc47a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8249
        },
        {
            "id": "4e727109-b6f9-4f0e-9f72-edd0a99aaca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 34
        },
        {
            "id": "ad541576-f20a-47e3-9840-16fdf2815be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 39
        },
        {
            "id": "3094210d-893f-42ad-b71b-671dd433ebe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "5b122c83-221a-482c-b7b1-52009ad40f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "87373ab4-65f8-4696-8cc2-f41993652613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "1d682519-bee8-4aa0-89e1-df5779884bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "f93eaa2b-a8e9-4de1-b70e-673d442782d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "eca62041-4e88-4896-8f47-cf41cd9e8233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "862e295d-176f-41de-8bff-3a7c0251b723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "7b6f69d4-8053-4b19-bda1-f21d5f2c2b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "710376cb-2f7a-4ace-9a4e-b19fe57ec984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "aedc8ec0-692a-4050-a4d5-ecc1141d148c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "b65127aa-3f30-4094-95bc-b807b4f06540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "f50e81a1-af4a-4cf8-9f6c-e9c062ef4752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "a65e7405-604d-488e-b773-5f9fc1c9e44b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "9040ac75-6c3e-47aa-b8b0-625456b40731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 171
        },
        {
            "id": "77803dca-49c7-4c60-9f62-238cb62e9e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 173
        },
        {
            "id": "23b7f324-3a4e-472f-bac5-ec55cfa1ab69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "bebda765-e15e-4fd2-a9a0-167a6f499674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "5613e935-185c-4c33-a1ea-db33f43cb920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "acfe1af0-ec71-4735-a90a-f46f43aba8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "2d29f89e-5ef5-4946-9d77-a51f93d5d43b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "c7afbbee-41be-466f-b25d-d3f059c1dcb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "2c71a447-91d5-4f9c-8085-4b91a7f1b495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "3310be59-a013-4442-be07-1cc3e73e634d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 221
        },
        {
            "id": "13d4c6f0-1212-4e11-9db1-b00163a9d76a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "4e360d84-0820-4ef2-be1d-687b19f16843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "970ca01e-3925-4909-a4a3-ac7a3c503020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 262
        },
        {
            "id": "1095078e-3df1-47d8-82c7-f22526cdbb10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 264
        },
        {
            "id": "00a81c6a-4563-4ad6-b728-0b61b1b3cc15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 266
        },
        {
            "id": "06778b7c-c659-4f94-a4fa-28797c6dad2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 268
        },
        {
            "id": "b6f805af-3781-4f31-a236-c7d7c041016f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "4c071b04-61fe-4d05-8449-66cacb8d5110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "cb96061a-beb8-4647-a412-96af4fc2557a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "65758554-6e1c-4962-9ad8-544c0166c6e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "9526e9e6-a215-498a-83ef-9cc659485405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "1c13fe8a-7b1e-497c-a733-34b45b22fdfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "79dc2ac9-ff65-4770-8d69-68c5caa92abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "4ce84f85-15c1-415a-9195-485b7e725cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "6bb59c8b-fa50-4312-806f-2866a9066fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 354
        },
        {
            "id": "0d45a576-1926-4669-b3f1-485cfae345e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 356
        },
        {
            "id": "3e4bdd10-88d7-449c-a861-f4c9fe5b13d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 358
        },
        {
            "id": "5e8db275-97a2-459e-940d-dba594b41c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "73a90fe7-bb93-41dc-83d6-557f27be67f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "22119757-c7a6-46b1-b696-3bde576ac1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 374
        },
        {
            "id": "b2fc1a52-4a22-4008-87c1-7d245a8be22d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "e296e18e-bd67-4791-9740-40bd482dd8e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "efb2985e-bd5f-44ae-92b5-a7b962c0a464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 416
        },
        {
            "id": "a2c2de4a-da9a-4155-ac59-ae2ec0d0d525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 510
        },
        {
            "id": "85f1d1a0-3815-45a5-8f88-f03a9ef05908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 538
        },
        {
            "id": "c32aee79-b2c6-4967-9cdd-3e59f45ce734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "87a02b4a-b6a4-4532-a261-5e7f58e00e3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "8bbb8b26-4338-4200-b1bb-efa3caa0f331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "a1a99538-9b32-48fe-9bb0-7d73657c3e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "c6ca1cb0-8f9c-4e85-a617-5aeb953f97a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "25b09503-b93d-422d-9ee7-e65ac63c306d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "b7a5fb2a-ec3c-47c5-967b-e48faa3efee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7884
        },
        {
            "id": "123035e6-7d32-4572-8fb0-da4903cbd857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7886
        },
        {
            "id": "80925702-91c3-4e55-b738-4d72eef63173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7888
        },
        {
            "id": "78f0517c-bba2-4628-8820-f321124acdc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7890
        },
        {
            "id": "32b266f7-2608-4a3c-bfd2-b73d0d4a6e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7892
        },
        {
            "id": "fb0f5532-dd72-4e4d-b37c-66bf8817c29f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7894
        },
        {
            "id": "fc87be22-47ad-43ca-9027-ae34923992ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7896
        },
        {
            "id": "ef67786c-7d22-446c-aecf-3d7146d24700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7898
        },
        {
            "id": "aa377982-a894-440d-b940-06d919180844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7900
        },
        {
            "id": "a8b338fa-8153-40ad-b085-ad5d6003d6b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7902
        },
        {
            "id": "a4d92828-4489-4a36-a2cd-239c18e4d489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7904
        },
        {
            "id": "1e492c63-7762-4327-a27b-83999bfff5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7906
        },
        {
            "id": "cd3bb1bd-468d-47c8-90dc-90973b7e7312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7922
        },
        {
            "id": "cdf9b3ab-4c0a-48fa-88b1-0e1a699e78bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "89135131-7cfb-4235-b629-50ae6a68707e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7925
        },
        {
            "id": "3752a5a6-ef98-4934-b3eb-c5ba951cb71a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7927
        },
        {
            "id": "2e832873-9b23-4aa5-b03f-9555eb64da84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7929
        },
        {
            "id": "0ebcd22e-ec79-4eec-8253-98a9e4b06499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8208
        },
        {
            "id": "96c5830c-1c46-47f0-afba-7777f6dad640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8211
        },
        {
            "id": "d0dadf2b-05bd-45ff-a65d-94b57ae6f2da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8212
        },
        {
            "id": "d8394e36-60de-4873-b76b-9d2dd14a0d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8213
        },
        {
            "id": "03991d00-b288-4053-ac47-de4fe1663b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8216
        },
        {
            "id": "ced758f1-36c9-415f-a6e0-bc14b57479a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "24a401ec-a640-426f-8cad-2f26b6fd46b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8220
        },
        {
            "id": "cf8f1054-9eea-4551-af9c-4655fc999618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "04258942-3880-49e9-808b-a3edc02ecc2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8249
        },
        {
            "id": "b04a9432-d459-4427-a350-18da857a165c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8319
        },
        {
            "id": "22240059-73c1-48a4-aec9-28732058e7fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 41
        },
        {
            "id": "1a2e384a-212b-460a-88c8-bd13f75e5764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 93
        },
        {
            "id": "a7ccb7e3-d04f-4851-8140-8172c3a15218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "71ea3ea8-4beb-44d0-b16c-071f4c072075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "f7151840-ac75-464e-aeb1-c4ce40b57384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "1859e819-e052-46fe-8eec-60bbb54a5d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "eb6b0db8-02ad-4d5a-81d5-ee8c7fa0dccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "e829417a-253d-48e3-a519-e4dcbd92c3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "c62967d5-28d9-4b58-bc46-9c038624a912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "442e5c38-201c-4307-851c-b1165a0a2b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "c23f0944-0f2d-472a-95f8-e038f3052244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "7b969375-3a27-43be-9d84-b1b073ec0ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "8ebae197-3645-48af-a9c1-ef025179ae1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 171
        },
        {
            "id": "c8fcd17b-eb93-49af-adbf-1e0dbb0d5b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 192
        },
        {
            "id": "2867436a-ec9a-4929-8166-6455d5d9586a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "451af435-d3fc-41e9-a92e-8739229ce4e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "f6c0d027-16c1-44ef-9a1f-9771f513850a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "f7b8c010-8fcf-4372-82b9-f7ccec31e002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "3fd39ef0-7a65-4ad2-93ab-08b79bc350a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "b84ca9ba-03a7-43ff-95c5-8f3eaca80a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 198
        },
        {
            "id": "8e49c438-02e6-415d-8dfc-d9189e764a9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 231
        },
        {
            "id": "62b0ae1d-e2c2-4670-b2ed-76f73ac10e46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "6eb24d94-15d4-46bf-8441-f2ddf32ee5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "641287cc-f4b7-457b-9668-7ea8affa5d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "4cf41b8f-cb40-4603-a5df-22ac1a8b28b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 235
        },
        {
            "id": "060d8a96-4b75-40c0-8d37-58ba162c353b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 240
        },
        {
            "id": "8c3c3e99-d92f-4943-b5e7-8782c5c7da29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "9fc30a65-6de5-4346-bb11-ed3e182679bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "63bc5cc5-a112-4d80-81a6-a05ea6ac13e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 244
        },
        {
            "id": "8df2b2cc-bed1-436a-adc8-ae959a705511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 245
        },
        {
            "id": "13ce00d6-dc63-4fd3-8006-2b267d7c6177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 246
        },
        {
            "id": "47826e72-ebb2-434e-a1b2-dbbafbd9a2b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 248
        },
        {
            "id": "1d57bb53-ebd8-479a-ae55-8d513d67e34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 256
        },
        {
            "id": "a0d37f44-dcab-45b0-be8d-8768430f44a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 258
        },
        {
            "id": "160bb955-2bc0-4497-b55f-de4fc6837e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 260
        },
        {
            "id": "831a959c-80e1-4463-aaa8-756c9e51f50d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 263
        },
        {
            "id": "7f5fe629-c5ad-4b40-a374-b41be4ca9bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 265
        },
        {
            "id": "b41d9a52-1f0f-4fa0-bc95-0aaa68f06589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 267
        },
        {
            "id": "9d86931d-7294-4f0a-a575-471216388208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 269
        },
        {
            "id": "4bf446ac-978e-48e1-9a44-e37d2063b9de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 275
        },
        {
            "id": "dab99e3e-64e8-4e80-9ea0-dd8eb5f48615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 277
        },
        {
            "id": "7517d5a3-3040-4157-a34c-ece06f3cb9b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 279
        },
        {
            "id": "bdc57158-df49-4811-8612-7aaefeb2d07a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 281
        },
        {
            "id": "57158d9c-f193-4f36-8f6c-14ea091f9465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 283
        },
        {
            "id": "e4db5dde-e743-46f7-8ef5-7b2c8687a8e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 285
        },
        {
            "id": "575c5c4d-fd95-43bb-bdff-90cb8d030c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 287
        },
        {
            "id": "bebcc3f2-3cf0-4d66-afee-a2e4709c2872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 289
        },
        {
            "id": "9ecaa2fc-c6ce-4a53-a38e-d562f3d90217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 291
        },
        {
            "id": "436eb734-472d-44e2-ae21-bad1e1cb3d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 308
        },
        {
            "id": "69e76510-2e4e-498a-a3bb-ce38a89c0d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 333
        },
        {
            "id": "d23ede35-ddbf-490d-be90-e058104ce98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 335
        },
        {
            "id": "d16e7310-9ec1-4e1b-8a04-747827116dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 337
        },
        {
            "id": "53a2e847-1ead-4498-8d49-97ad001a238f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 339
        },
        {
            "id": "27961f8b-7608-474c-bbc3-bc2a1b788381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 506
        },
        {
            "id": "a470f47f-4aeb-4de2-a12f-ba4a6c9eaf3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 508
        },
        {
            "id": "79e9007b-2a5c-401a-be33-f8dd63754626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 511
        },
        {
            "id": "0d6b8cc4-111f-4246-b58d-cfee8aec4ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7840
        },
        {
            "id": "977682f3-cefc-45b3-ad86-252100b91aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7842
        },
        {
            "id": "aa035ed7-0bbe-4c60-9a87-e7dea3628ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7844
        },
        {
            "id": "3f4f7b95-d0f2-47e6-8133-970b131acf3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7846
        },
        {
            "id": "b17a318b-328a-4fb8-bf2d-737f65760bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7848
        },
        {
            "id": "48a565ce-ae95-45e6-93df-f050717177c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7850
        },
        {
            "id": "e3c2680f-6beb-4014-b7e3-83ac3d6e8a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7852
        },
        {
            "id": "6e80d750-924e-412b-8cf6-dd82f80c8249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7854
        },
        {
            "id": "ea2b44d9-3a31-4f3f-976a-a3879b34be83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7856
        },
        {
            "id": "e38bbbd2-fdb6-4042-a7ef-0c77eb5158db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7858
        },
        {
            "id": "19a28c4c-85ac-4f34-8111-904b120efeb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7860
        },
        {
            "id": "03a02017-380a-4670-a63c-a7ab9d1fb1bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7862
        },
        {
            "id": "46a96342-a9d9-4702-a102-36206dd9921a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7865
        },
        {
            "id": "45afdfca-c88c-404d-9fbf-ed0d9f35cd2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7867
        },
        {
            "id": "dd46d8a3-f05a-4c1b-9c92-4fef944e17d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7869
        },
        {
            "id": "4d263c11-a92b-4226-bd8d-a4403348112a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7871
        },
        {
            "id": "01143d2c-eeb3-4731-b4f2-b8b0fe13fc88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7873
        },
        {
            "id": "d1824b26-9f4f-4a61-a3c6-73af518deb1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7875
        },
        {
            "id": "4c331dd9-29e4-49ee-8f2e-6d0598475d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7877
        },
        {
            "id": "57731e06-68b8-4b02-b627-71a05a0f777a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7879
        },
        {
            "id": "d2f6cd35-ac59-4e53-ad01-3581fec33f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8230
        },
        {
            "id": "bbb3286b-8a80-47b5-b2f1-c4a78b44472b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8249
        },
        {
            "id": "13ad0306-9bda-4543-9c43-a75b3247bbb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 45
        },
        {
            "id": "00754f81-42b2-4b80-abcf-36d1e3fb86cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 171
        },
        {
            "id": "49549f38-f4cc-440e-9ebe-cecfc2419b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 173
        },
        {
            "id": "c011a314-85bb-437b-8a70-07165e294a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8208
        },
        {
            "id": "0fa5c82c-aa47-4647-9426-dbf9e4febd7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8211
        },
        {
            "id": "dc7ed787-9448-483f-960b-f0df1bd48093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8212
        },
        {
            "id": "a34d937f-ddf2-48bb-94d9-5ccb9023131d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8213
        },
        {
            "id": "5bf459d1-f9e1-42f3-b6ea-12ccd0798b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8249
        },
        {
            "id": "e4e5c826-1f70-45fb-9211-f070c15dd098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "943dbaf3-a459-48a4-8d42-8fd405c51fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "8b9eb6d4-64f7-46a1-9b2e-f61fa0d6f8de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "f679ae98-3f10-4fe4-a446-4da010123899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "0f227854-21ea-4850-8784-4fea44d922ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "1564f24e-83f7-4473-be53-52d4a8570515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "f7c2e3fe-280a-446a-8319-5684002a2570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "b56834a3-8d35-4b0f-b798-8c7142b23a1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "9f243f79-7cbe-4360-94f0-a4669e54a03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "204ecc3f-f159-41eb-9624-af76632d2657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "f01b94d6-65a4-4493-a293-bf10363af5bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "033cb0fc-6786-4f56-9fa5-51ef747e4fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "b5bac549-495b-4a50-885c-42d185c03f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "f8059550-ea16-40d7-8f20-914c4066ec15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "becc7a18-663a-473e-a2d9-b305aa0a5476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "de1b63da-c870-4dae-8f02-ff00c647150b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "bfb9cc97-8308-4438-9347-19f4ae11aaec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "e77d15b1-3a59-4d64-95a5-dc7866105fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "44e79f0e-cb46-470c-9840-075e03acb20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "7e7751e5-9c0b-4b5b-8aed-7d033f20a71f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "6b4f5001-7437-49c0-8c3d-b5a7bc36a596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "acfb2cc5-7605-430f-9d2c-6f8424d05149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "659f07ae-c443-49e6-b356-1d9716113f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "0e19c09f-9e98-415c-a9b6-801bb7c6d35a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "c47f5b76-48d2-4e25-908e-e4b5823c8a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "bc16a349-17e5-4c09-8af0-2a3aff0465e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 171
        },
        {
            "id": "8bc2087a-f49d-429c-8c25-395dcb4cd3f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "3a6b1300-bf69-47ff-8743-78f33f080061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 187
        },
        {
            "id": "b8f0e5f4-61ae-46e6-98a9-9c05c1c5642c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "ca4128a5-847d-41c8-8beb-1840f4cc1b6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "47ee188b-9358-4894-863f-cf4e3b86e958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "fa4cb666-f5d4-44ff-8791-ff513a1260aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "1826e1b6-ba2e-4db7-b406-fa3dba9a3cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "7cb6ddcb-36f9-4108-a581-6cbe006f31c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "b5034dec-0afc-445e-9cb9-34365169706a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "aeab1ba9-3e24-4bf7-91d8-5bad0e088cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "86f8560e-6bda-4400-8778-0d3b79eef4ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 225
        },
        {
            "id": "30632056-ee51-46c6-bdc5-6440ca773272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "f1c024ac-bbf0-4e96-807f-9810325755dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "7891b0bd-0291-47a6-92cc-b8dcf77286f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "2828a141-f816-488f-b321-2db8d4915458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "eaa4e2e5-6c64-4bed-b794-451a42057e52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 230
        },
        {
            "id": "a2e8e074-a7b9-4fc9-973e-e8ac495fd8d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 231
        },
        {
            "id": "ac53a262-dcc5-4601-b484-eb9598ad2c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 232
        },
        {
            "id": "669a2ac6-eeac-4a26-b7dd-b796872265fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 233
        },
        {
            "id": "36b7e48b-b6c3-4c40-8e27-85f5c4003cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 234
        },
        {
            "id": "6c503ca8-f546-4071-9256-b18f9531769c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 235
        },
        {
            "id": "2f92bb07-0984-4640-b83f-c907dbb2578d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 238
        },
        {
            "id": "de0eabad-10f6-41a8-9923-07ab781b026c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 239
        },
        {
            "id": "3142795b-62e2-46cb-932a-ba49933cebff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 240
        },
        {
            "id": "f6a63653-1a4d-49d1-896d-e4b6a8431bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 241
        },
        {
            "id": "57994ce3-ebfc-4311-a9fd-8b419a0f9526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 242
        },
        {
            "id": "33e80322-40d6-4fa7-b50a-f85eeb2061be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 243
        },
        {
            "id": "92e807b0-0f86-4d3b-ba1b-47aa81feba9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 244
        },
        {
            "id": "c46764a4-6a99-4eec-9b9a-a0612ec49690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 245
        },
        {
            "id": "25598121-ca4e-44d7-a2c5-24abbb808d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 246
        },
        {
            "id": "40058b37-09be-4895-ab79-d4e7f88004c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "2f23646b-1a92-49de-9b8d-ea104bd68a75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 249
        },
        {
            "id": "95ffc8ed-ecf4-424d-b94f-7201b06ebb38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 250
        },
        {
            "id": "febb0ce4-fd31-4cf7-bdcc-2e1fdc80e6d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 251
        },
        {
            "id": "f7aa6d59-dd7c-4aaa-9eaf-7e4a62a9bbd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "5edc2e91-271a-486c-9ea0-ea324561c46c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "fa766831-931d-485f-bb3d-9ce1a6a38d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "b2f20621-66b4-44f1-a255-cac71dd097fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "607d051a-2618-43bf-aecf-7cac1104c18c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "f70caee1-eb03-4840-8f02-31a3f0cc6497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "03888984-3094-4270-ac3d-dd644021b37d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "9ca284b0-e6e5-43ad-b9f6-3bd4274350f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "e38da959-0cfa-467b-ac10-0f7e708e60ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 261
        },
        {
            "id": "edecdf8a-5b18-4fee-8d43-570acae8cf4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 263
        },
        {
            "id": "90b82b12-0ec5-4a09-ab91-10677f5b4fa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 265
        },
        {
            "id": "6cf5a6c5-003a-4978-a72f-67462a1e41df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 267
        },
        {
            "id": "12056df3-07c2-409a-b4f4-54d1325dbbdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 269
        },
        {
            "id": "79173241-e186-4d93-b432-45fd7c0932c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 275
        },
        {
            "id": "1491b446-9f67-4000-809e-f8b9f8912083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 277
        },
        {
            "id": "4733cd5d-8171-48e8-9db9-55f8e6f02ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 279
        },
        {
            "id": "72270de7-9891-44f6-bfa7-42637c4a3858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 281
        },
        {
            "id": "dc0c70d9-aabc-46c7-9208-7e2042ee06ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 283
        },
        {
            "id": "91f719b3-c70e-42da-913c-ad30de168567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 285
        },
        {
            "id": "393506c2-2e0c-4c01-b615-5fee831569fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 287
        },
        {
            "id": "945b0833-6c57-4e67-8c0a-c3438b603be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 289
        },
        {
            "id": "3e94685e-94ff-489b-9fd4-55265e7edf71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 291
        },
        {
            "id": "a6585469-8f8d-4857-b719-0245f53fad60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 297
        },
        {
            "id": "eaf69d7f-fbbd-4581-8540-bcc623f55d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 299
        },
        {
            "id": "7564a9bc-2556-4849-8c8a-3e9f3693f0d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 301
        },
        {
            "id": "260840aa-38da-42bb-af04-f18398661e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 305
        },
        {
            "id": "c4fc3f4b-6d0b-489a-87db-028428820d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 308
        },
        {
            "id": "3dbe7583-2daa-4128-a0f1-0463e611dee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 324
        },
        {
            "id": "6be48ace-07a5-4d25-abf8-681c7e6a5db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 326
        },
        {
            "id": "a0dcf024-8dc6-4102-a7ab-58e032e9c654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 328
        },
        {
            "id": "6373e370-1499-4765-ad03-4abffc0f7db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 331
        },
        {
            "id": "d842af80-cde3-4c0b-bc21-b354d2b4ad4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 333
        },
        {
            "id": "9fd0ba51-07ae-4485-95ff-e3a689e54af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 335
        },
        {
            "id": "f2fef8c6-2a5b-476d-84ce-1d290919fe25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 337
        },
        {
            "id": "3c8347af-c413-4cdc-963b-1e5d09fdd27c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "3b39db69-cede-4992-ab15-548fcb5ba83e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "76ecb341-5b4a-470f-b761-ecbc5fd7bc3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "9e8e6adc-6b37-446a-befe-feaadef6f009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 345
        },
        {
            "id": "c6720d83-0060-45fd-8553-0886c07db43f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 347
        },
        {
            "id": "a92b7821-3e90-4cae-abf7-db72f6b3e7ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 349
        },
        {
            "id": "fc8dd164-6410-42b6-8783-c51af973c1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 351
        },
        {
            "id": "d635faf2-2f65-48b6-8678-d198dd9d3576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 353
        },
        {
            "id": "b4fb001f-1b92-4368-82e3-af2769329514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "32bfc110-1b07-4cb6-9dcc-b5bebd288ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "c4ee819e-e675-4bff-9baf-5cd3fd0a38bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 365
        },
        {
            "id": "9304b9be-0299-4d4a-a2f6-1f9a92bba711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 367
        },
        {
            "id": "59da3563-ba26-42f9-9cf8-0f436d1a335a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 369
        },
        {
            "id": "ec8bff00-f3aa-464c-84cf-22b5ed47c117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 371
        },
        {
            "id": "a6bcb326-c607-4023-ba26-722e0074ac85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "2269238c-28f4-4e9d-8f2c-48c35271cd54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 375
        },
        {
            "id": "fcd975f8-de61-464c-8c7e-6389ccc87256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 378
        },
        {
            "id": "35144af5-286b-4b27-a9ff-fe13b30c5b7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 380
        },
        {
            "id": "5816e7ff-199c-4ca2-ad93-4f8a5fc56cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 382
        },
        {
            "id": "00eb985a-b971-4924-8267-20650824aff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 506
        },
        {
            "id": "4c851b71-08ca-45f8-b014-73aaaf3f9f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 507
        },
        {
            "id": "9dcc9987-b845-430b-b32c-d504d67edc38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 508
        },
        {
            "id": "37640619-d84d-4cd4-9a25-9b68c5050312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 509
        },
        {
            "id": "b3d12545-9655-43af-a73c-665e765aaa2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 511
        },
        {
            "id": "d2097476-c3c4-4d7c-aa89-15eccddfaad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 537
        },
        {
            "id": "fc07b29d-8676-4822-bfd6-785c7bbd1e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "aa79cd66-d215-4d84-95c6-e8bd37284bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "ef9f8c8b-11ae-4d25-9704-f5846f4dfcfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7813
        },
        {
            "id": "1d772ab1-d41e-469d-a6f1-83a11ff99a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7840
        },
        {
            "id": "4b3b1a98-3e9e-4100-8428-6dfb03434f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7841
        },
        {
            "id": "61bfeb49-95f0-4638-925a-f63972db2d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7842
        },
        {
            "id": "7ee4b42d-785f-4c8f-b954-7ac348be82ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7843
        },
        {
            "id": "473e7019-689a-4877-b47c-4265b2e10c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7844
        },
        {
            "id": "ac6d793c-600a-4c43-b04e-0d7681a9897a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7845
        },
        {
            "id": "50f5bd35-fb6f-406f-bf50-57952b09e620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7846
        },
        {
            "id": "13235a46-58a7-4361-b9ec-3718d8e26fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7847
        },
        {
            "id": "1442c865-8f9a-4988-a522-1d29b1c47628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7848
        },
        {
            "id": "c4ab1da6-1874-4c93-948c-612633505e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7849
        },
        {
            "id": "f0643161-6502-4631-ba5f-6f7e2a7dae4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7850
        },
        {
            "id": "7e93fd08-c372-4fe5-97a9-0b0995b76dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7851
        },
        {
            "id": "fc344957-6d94-4ea0-b680-cf23f545ab42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7852
        },
        {
            "id": "78d66e8c-3fff-4ae1-bf37-31341e3c8764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7853
        },
        {
            "id": "3c8c1ae7-7e12-4341-93e3-acac96a79cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7854
        },
        {
            "id": "69cc1ae8-487f-40da-96b0-4eaf13554923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7855
        },
        {
            "id": "c6bae41a-f86a-4cc1-9496-2a13bee190a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7856
        },
        {
            "id": "107f4c82-acbc-4e8e-bb2e-2bac1e819d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7857
        },
        {
            "id": "c23918e2-db7d-4f50-9036-22f0e7a3d6b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7858
        },
        {
            "id": "ca75cf5b-c0c7-4802-bfb4-c95da24b8c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7859
        },
        {
            "id": "d4ca2d33-fce6-4456-98ea-0dc5d8a11787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7860
        },
        {
            "id": "69ef681c-aeb7-4f53-a48f-5353cff4a4ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7861
        },
        {
            "id": "9e564c9f-7936-40a8-9c47-716c39b6380e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7862
        },
        {
            "id": "36a2eca7-2b08-4454-bd29-efcc7da1b26d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7863
        },
        {
            "id": "34726ca3-dd81-420e-aeb0-0c13ca40a827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7865
        },
        {
            "id": "08bdc669-1efa-4fa3-b803-dc1336d121f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7867
        },
        {
            "id": "e430a607-9d0d-447a-a38c-6328e0911387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7869
        },
        {
            "id": "32108918-4952-4fdd-b123-e9da1505095f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7871
        },
        {
            "id": "11240617-d48a-4b8b-8f66-3c62d6bb2e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7873
        },
        {
            "id": "cac76f28-06dd-4746-94e4-3b0ed100030d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7875
        },
        {
            "id": "c9f95575-4a07-4cbd-835b-4d2ee0214690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7877
        },
        {
            "id": "c25ed75a-e1a0-4210-89ed-9e189b2eb3c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7879
        },
        {
            "id": "75860dfd-73c0-4405-86fb-125460c9e890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7923
        },
        {
            "id": "40ec6dde-d565-4a3f-b224-697fbd012f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7925
        },
        {
            "id": "7cd5907d-65fb-469e-95cb-d50503df2950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7927
        },
        {
            "id": "70e8df09-671e-49d7-886f-0eadf6cf51eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7929
        },
        {
            "id": "e697e393-b6ed-4926-9b66-e89d696dd89e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8208
        },
        {
            "id": "67667cc5-112c-4aee-8b6c-3a9e162b29c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8211
        },
        {
            "id": "b5b067a2-9370-4ba0-8d51-42ef301867b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8212
        },
        {
            "id": "3928599c-8b68-440c-b9d5-e4150e08cf04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8213
        },
        {
            "id": "45702c75-8404-47ee-ad1c-5626fa4c6284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8230
        },
        {
            "id": "3dd6fd33-5852-4a3d-8d57-acc9fa70e0bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8249
        },
        {
            "id": "25b8e511-33d9-4e4d-bc4a-b64b45e5037e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8250
        },
        {
            "id": "fa3388ff-ed2f-4d3f-950c-45b24381a0e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "3a9b0d69-6ba3-431c-a63d-a1a4fdf13b31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "77d92d39-5804-4737-8d2b-3563eace499e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "137d1804-01fa-4b0f-8a89-d3794bc71afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "314032f5-ee80-4783-b44a-ce26334f0a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "3fc85d66-75b5-4117-b6c3-b2eaa4db0c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "d669d1db-e19a-4cae-a06d-c1550511a9cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "dcfa3f43-6486-4f13-926c-009ae52aa6f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "2381f6ec-6e4c-4bf5-865a-8a5c0ac888b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "2cec0972-9e1a-44b7-82c8-af2cc092f0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "7a5b71b2-41ea-4421-958c-bf28ced01748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "430ee712-a9d6-401b-8688-e03af2300ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "dda48f6d-1927-4318-8f2d-6381f341d49b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "78a93b82-4458-45a8-ac81-554e5503c55c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "f97d4deb-8046-4550-9f50-54aabfb4654a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "fb7d8c6d-9279-4f79-b3a3-f9c8c77f86d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "1d21bb5c-f86a-4fe8-96c4-00787b1bc4e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "fbe57b1e-116f-47a3-b6d9-025bf3757b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 171
        },
        {
            "id": "24e56767-1708-420a-a343-b42ce46e0c12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "1fac8e00-1893-4421-b1b2-f83eff55b4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 187
        },
        {
            "id": "573b430b-6b78-4a63-bded-484dbf175c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 192
        },
        {
            "id": "9d1f883d-49c4-49a1-ba09-474124f656ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 193
        },
        {
            "id": "3761e452-0c48-495b-ad53-47f6cf452b14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 194
        },
        {
            "id": "ec41bd3f-dbeb-4fa4-92ec-d0b9132c828b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 195
        },
        {
            "id": "e3bb44d6-a703-4388-856e-10237dd76c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "fa36c2a5-1608-411a-ac79-2ca343cdb3a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "c18b8703-d0cc-4bd5-8b9c-c0fe3b67c150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 198
        },
        {
            "id": "41e4eba5-0de0-46af-ac17-73c276825c4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "86afcdf8-0bfa-4478-9eac-e6145dbdbb5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "d4a000b2-db53-4372-8db6-302aa202445c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "24193330-3810-4954-b215-4cd3b01608ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "a9540034-1d1b-4b9c-86bd-28fa933dd9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "62ffa22c-459c-4f53-9bc8-1df55597d332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 239
        },
        {
            "id": "1dae495f-e599-4cb6-af23-4e356f599d96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 240
        },
        {
            "id": "2fb6f47e-3d4c-42ca-a05f-62d81710dc8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 241
        },
        {
            "id": "07a712da-0466-43fa-a64f-718fc66552b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "eabcab42-2f79-4ab6-854e-867f20f7a9c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "cbb47a9b-1549-4540-b36e-7d07ac8f8171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "68b57f08-de75-4e19-bb0b-c016ae8acc97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "1706ed32-21ae-413b-bd76-674124109379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "2feefbeb-0d61-4dd0-b321-ead90072677e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "d1b638a9-a6ad-4c5a-b4c2-c5f6372e653d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 256
        },
        {
            "id": "21493035-32b4-45ee-bf4a-322a64fb3565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 258
        },
        {
            "id": "93c9db7f-8e05-44cb-8471-6854347d93c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 260
        },
        {
            "id": "abb57cfc-a30d-46f1-b2e8-8ad94775510c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 263
        },
        {
            "id": "1d5bbd54-032b-46f6-95ad-937c6eb66992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "a1c8dc59-cf17-4505-8638-f9c4ec717c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 267
        },
        {
            "id": "f23316a8-1414-428c-9279-376d4ca9ed3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "7f3d0f24-149b-4dfc-a12a-b2143ecb7d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "e8743409-069e-4b6a-a252-7a8f947aae68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "7492ac2b-12d3-485e-a356-ce5b1370a307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 279
        },
        {
            "id": "09dd0327-5f27-4bb0-a142-bfc4389dd984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 281
        },
        {
            "id": "79cad2be-9db8-4dfb-b7a0-f98ee04b89d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "354404ef-5f98-437c-988a-56b255386368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 285
        },
        {
            "id": "718d5cba-1b05-4944-a8ff-973551b12c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 287
        },
        {
            "id": "4f1ee1d0-0345-40f0-b531-2f14a728eea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 289
        },
        {
            "id": "e9d7d9f1-5ba5-4b94-8f98-7b031d63c4b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 291
        },
        {
            "id": "57002557-3f86-44d9-ab29-4ede36000b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 297
        },
        {
            "id": "a4517167-89f8-4828-b9ae-c5ca792b385b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 299
        },
        {
            "id": "e750f1a3-b103-4d71-8098-e5379ba50b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 301
        },
        {
            "id": "9e411a52-63a5-436d-bc0a-48bc5c343ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 308
        },
        {
            "id": "41371208-06d7-4acc-90b1-325ea183b6fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 324
        },
        {
            "id": "a625cae5-7bc1-4274-89ad-ba5071505b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 326
        },
        {
            "id": "87fa1cd6-72ce-43b6-8b45-58f6b2cd8101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 328
        },
        {
            "id": "f3031c09-d008-4b24-83b7-37ddfc779f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 331
        },
        {
            "id": "b49926d0-0a42-419a-8153-bc184812da6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "e1faeff1-7f9f-47a9-b0cc-8bf40904cdff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "c2d7642a-fefb-4eb8-8ae9-d7dd37461487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 337
        },
        {
            "id": "213d89d0-9340-4070-bbab-494b73025674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "7b0f12dd-0456-42a0-95c9-015262c59de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 341
        },
        {
            "id": "2700f405-3ebe-40f5-8c37-e0171aab24d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 343
        },
        {
            "id": "9cd74dce-dafd-4737-a55e-6d9e6faa9c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 345
        },
        {
            "id": "0bcc5b18-3656-4719-b8ad-c085c41e4578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 347
        },
        {
            "id": "a90ee7c2-a3d1-45fe-ac31-d4774abb988d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 349
        },
        {
            "id": "02d818ce-0ea9-4a4e-b139-44f7cab3d3a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 351
        },
        {
            "id": "f4764ef7-ac7a-4d3a-8a3d-24606dc4b222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 353
        },
        {
            "id": "f119f1a1-0311-4111-b162-8563aad08fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 506
        },
        {
            "id": "518d5498-2054-44f3-8b9b-e04037de3d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 508
        },
        {
            "id": "1351fabb-0f73-4c45-b2b5-86167537e7b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 511
        },
        {
            "id": "d698c43c-ff85-42d3-8447-d2c6f68fd3be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 537
        },
        {
            "id": "53af5a5e-a3dc-48d3-8941-ef08990f2fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7840
        },
        {
            "id": "358ca798-e408-4307-a5e1-e367fae9076c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7842
        },
        {
            "id": "2c137b37-f72e-4158-8c94-6055e6f6a300",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7844
        },
        {
            "id": "b4090a91-e3fa-4520-a59c-ba1a11c50834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7846
        },
        {
            "id": "90bf4dc7-0fe4-4fa3-a0bf-31ca910d30bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7848
        },
        {
            "id": "87e3415a-0edf-4b8b-9e9a-8ec76162ef78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7850
        },
        {
            "id": "07a3464f-cb54-42b3-8f16-b9db3142ad71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7852
        },
        {
            "id": "9cef6703-3f34-4924-8e57-42a994620d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7854
        },
        {
            "id": "36f58f82-fb25-4928-a196-3d60a49a9a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7856
        },
        {
            "id": "d05eb479-0072-47ad-971a-0138de7f9281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7858
        },
        {
            "id": "1bfb1240-cff7-4f7e-bf07-099c8e18c57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7860
        },
        {
            "id": "44d4ba6a-09da-45c1-82df-8f714041ba58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7862
        },
        {
            "id": "53067984-c1f4-4315-b831-ffcbf3883caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7865
        },
        {
            "id": "ef847e5d-d135-4c52-a83c-f3741edfc880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7867
        },
        {
            "id": "d0049f03-00a8-433d-9b3a-b247f139f2e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "db58c04d-39d9-4c13-81e0-11123f105a7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "bdad29cb-48cd-4825-83d4-37ee5c4d7da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7873
        },
        {
            "id": "0001ab57-0960-49c8-95dc-b0fa17dd44cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "b831cec3-ba6e-4b53-b0af-ee9c1be0889f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "e0df8f48-f3bf-41d3-8e54-8ed335c08fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "a1f686dc-29b2-4b52-bbac-5e853a3ecff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "35f14bc9-b4da-40a3-a4ce-700ea6676b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8211
        },
        {
            "id": "3382300b-7c75-4ae4-abf8-9c958794dd0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8212
        },
        {
            "id": "763f6f04-74d9-40be-80c8-008bdc82cbe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8213
        },
        {
            "id": "c02029e3-6044-49d1-9160-c40a153a9d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8230
        },
        {
            "id": "5cf5f78b-077d-4276-af44-406e4c57aaef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8249
        },
        {
            "id": "8cbc11a5-95bd-40e3-9daa-58787bbb8c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8250
        },
        {
            "id": "c8534d76-72e3-49d9-8649-eeabab746202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "7f3d82ff-d9d1-4d39-b30e-40d29a090792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "e7ef20cc-b9b3-4195-a097-d2b0cd436820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "70835374-1403-4629-8ce9-d18df48a339c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "48185c6a-8d4d-49e0-a370-d60742c5b84d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "6f2fe16f-c953-4ff6-b70d-b616e51cad44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "fd2b7cbe-e2d6-4545-a3e7-3523c089112d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "3462aad0-6cd6-4d65-bfd6-b09d21a00d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "0a495c3f-ded9-4e55-adec-3d8c79e02e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "59380551-92bb-46be-803b-8edefee0540e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "97b0189e-df91-4b05-8bbd-4dca86270e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 115
        },
        {
            "id": "c16af27d-3eee-4560-89bb-2c7f9b0f2664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 171
        },
        {
            "id": "e5b09f5c-87e2-4171-ba24-3fcef821edcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "8bf64208-5697-4aea-b773-829a0aa40363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "74e35ff1-df8b-4260-9bec-3f8583aceac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "b3e556e2-6731-478b-9257-90ad16846e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "4ba48afa-21bd-4b57-9801-1f3d30e98161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "15728a57-144c-4be1-b6e2-1c0b5b9d4af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "359ae170-e365-43da-8068-abb86fa67de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 198
        },
        {
            "id": "762328b1-0c5d-4fd4-b486-a8ff0d3a066f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "11b9551a-81dd-4078-80ca-6e01aaa06a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "26ddb9cf-02af-4937-a38b-b3e9fe4b6141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "34b61f15-9629-40fe-b3de-25cf9a4925cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "190a8813-242d-4cfb-97a9-8b62bbf809c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "a96b3494-8ee3-474a-9c64-2b2c23b4bdc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 239
        },
        {
            "id": "cb78fb7f-ea80-41a3-a2a0-ee2ff32527df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 240
        },
        {
            "id": "6dd8278e-a4b4-49af-9f07-606c7bd819ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "cef60ce4-0ddc-435c-bb13-f36edf7dc232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "40fc8bbd-56c5-4955-846d-db3f8c5c72eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "92b73fd9-9986-4688-a14f-99f2f97163c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "fa982807-33b4-4e59-ba46-f48fe2f0f247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "502d3639-df75-400e-9bea-4a1c73a53728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "9d4d8516-0a10-45b1-88a7-1136352e0735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 256
        },
        {
            "id": "07dd7157-b3fc-4842-aee4-26556638486f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 258
        },
        {
            "id": "8b6184a0-4e0a-4ac3-bcb2-a7335badafe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 260
        },
        {
            "id": "2559093b-d8c4-4096-a53f-03cf9f0f3c57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "7972989d-0725-4aec-a4cf-610bb62d254d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "1d170fe8-2389-4ae7-809e-05da1ef2c959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 267
        },
        {
            "id": "17f23b49-69a8-47dd-8c47-2762a6b13b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "1149c2e4-f136-4803-9b96-2870bbb7f688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "c28f5eb6-b30c-4be3-9a14-73fa1819ac23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "6099f449-d075-40c3-adca-1d8d0c5107d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "da720c5a-12b2-4c99-8ec3-b2d319ca312e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "9309f050-e158-46d7-8fe9-550b1693e7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "b47a4b39-9806-4d7e-980c-680a11497f80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 285
        },
        {
            "id": "0753d945-ce4d-4925-ad5a-8f5ef1844728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 287
        },
        {
            "id": "84c0cb8f-9a79-4fbb-b9a1-a4f793684a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 289
        },
        {
            "id": "547d4c5b-82e9-46e1-be41-37f0a5652fe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 291
        },
        {
            "id": "5af75243-7cdf-4125-8a16-71fb13226bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 297
        },
        {
            "id": "e9c7785b-8d6d-4a13-95fc-234c63b78554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 299
        },
        {
            "id": "74103d08-75ae-4b89-a41f-447b5d97390a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 301
        },
        {
            "id": "44e0a50e-ce14-483a-9a7e-eaac9e2cab38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 308
        },
        {
            "id": "20ecfab5-5f16-4e94-b229-d703da9d811d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "1999d586-558c-44ce-9e5f-ce020214e5a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "da169adb-9d6a-4757-a869-016c61f68b9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "b9a0f9ed-ef14-40b7-b167-f6aab9b4255d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "9851a633-6cdb-4c6d-b955-d7b7b719756d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 347
        },
        {
            "id": "48c5d8ff-2a13-45a2-a80c-350d8df2e0b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 349
        },
        {
            "id": "33c0dad0-3c8a-4e8b-8db0-2beb24e0322b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 351
        },
        {
            "id": "3bec6f11-be54-4ada-a813-5c995c93fe3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 353
        },
        {
            "id": "f3d06be8-b8e2-4769-8023-bf1114a7d4f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 506
        },
        {
            "id": "93f070e5-4ff4-41c5-8bd2-b9eb206019aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 508
        },
        {
            "id": "a5240551-4039-4dac-b80f-ac4ffd11b59b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "8cab8cbd-2be4-463b-8a40-f704f8a266a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 537
        },
        {
            "id": "f5e509f8-c8e8-45b3-88b9-769462cf3c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7840
        },
        {
            "id": "4a6a03dd-ff10-439a-9251-a1ed54fd2666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7842
        },
        {
            "id": "94b8357c-f324-4522-89dd-16283cc01978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7844
        },
        {
            "id": "563a1e68-96e4-4ceb-8b73-b82c9d05f38c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7846
        },
        {
            "id": "a351ae22-08a7-4718-b714-8520b0a9cdfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7848
        },
        {
            "id": "fa7083d6-15d3-4a68-b2ea-aebfa67a77a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7850
        },
        {
            "id": "a5dc87e7-a39f-435a-a46d-2b4a25043b1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7852
        },
        {
            "id": "88a9640f-29d8-48aa-911f-8105bf267fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7854
        },
        {
            "id": "b1e98946-1b75-467a-8ce9-37d1a377377c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7856
        },
        {
            "id": "1c2787a6-549d-4f59-af20-8392764b5852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7858
        },
        {
            "id": "ce445769-a061-403b-8be2-71a3604633ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7860
        },
        {
            "id": "a9d87a4d-e3d2-4494-9d81-5086532a07fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7862
        },
        {
            "id": "37a81247-b080-456f-87bb-d59c00b4410c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7865
        },
        {
            "id": "1e0675ae-d245-450d-801d-a379a6c3ced2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7867
        },
        {
            "id": "5f9c0155-ea18-49a4-aad7-e93ffbd4bce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "027edc51-172f-4540-9126-6afb54494273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "2e166e5b-b916-433d-a016-ef3dc9633f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7873
        },
        {
            "id": "3527cd31-591a-47fa-8452-d70d3a2667a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "dbb9c5dc-b8a4-4e5c-bc87-0966dd60a058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "890101b5-c210-477b-b4f2-fc454e8a5d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "332dd5ec-24a9-4007-9f4a-2fe1ed3a954b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "88e6d0d0-37e7-437a-8a99-bd00b8126b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8249
        },
        {
            "id": "5b5f5a08-c9fc-4e7f-a152-359ec9a57bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "e668bd8c-fd78-4e5a-a5a5-79168714f4b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "070d3433-42ab-4e50-b27a-a2963066b59e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "8b00b77a-c239-45e3-b81b-a50b738eb776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "89d4f263-3ace-4126-bc9a-f9a6abc56d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "68d48017-5341-494f-84a9-872deee6f6ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "1f3103b5-46ba-4e89-85be-a7b0265c4c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "659e03fe-172c-432d-a997-39110741f7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "1e266513-f109-4f9c-a24d-c15ce5cd6514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "4e17e088-a6be-4a41-9a2b-6efdfa1446d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "3dea9299-ff15-48b3-ae19-7e0f76cecfdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "0b5a6a38-f2c5-4a8a-a661-2c8c994ac616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "58d36641-7583-4fcf-b4e3-0b8b31afc458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "1bbb06b5-464d-48e0-aa23-4d0c10413e3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "a9e39a22-88c7-4a74-8042-cac22c6e67d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "0b85378b-df32-4342-8748-47e2278638e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "e8c20d0b-7c9b-4150-9d4f-8564a8787b1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "ee15303a-ed87-4c25-bdd9-d1084df5a4b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "0a0219a6-99f7-4624-90d3-f26bfde2106b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "65f91951-23a4-42f2-9d57-0ae40c9921f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "d0938283-ba83-4b8d-9d17-928652d469a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "0d429c90-1ed7-4534-b8b9-54b6d8032f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 119
        },
        {
            "id": "35183994-38f1-4ac8-ba6c-e86281d703ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 120
        },
        {
            "id": "30c77982-8b2f-4c3c-8564-1ef05e91cb67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 121
        },
        {
            "id": "d3a7db34-9b5f-457b-b9ae-c26e47f72cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "697f5cb8-4cc5-478b-8b26-3d2e43cec432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 171
        },
        {
            "id": "d70a80e5-54c5-4e76-baa9-f2843cc4511c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "e667d5ad-42de-4e59-af69-b33c7da7a868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 187
        },
        {
            "id": "696f395f-9db7-44ba-b32f-0c66d6b7710b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 192
        },
        {
            "id": "e940d154-ce07-42d7-abb1-e8157b30a115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 193
        },
        {
            "id": "9bd20a0b-546d-47bd-b4a0-ff17f5e3607e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 194
        },
        {
            "id": "5b62059b-a8d1-4a6d-8a89-d03f93ae2f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 195
        },
        {
            "id": "df2c1059-cab7-49ec-a6e1-9c22ba193bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "7eeefc62-1c27-4aec-a27a-b4b3a430d87b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "cf9a2028-6d93-4fc7-ba6f-c17d8491f37a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 198
        },
        {
            "id": "a4e1996e-c9b7-4b82-bf76-c29fc92893c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 224
        },
        {
            "id": "8a8388dd-8baa-4aa3-b18c-abe8d991acd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 225
        },
        {
            "id": "de40d28c-eaf7-4589-adbd-31557bda83d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 226
        },
        {
            "id": "c5b8c717-f99e-4a39-b2bc-f1a970749a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 227
        },
        {
            "id": "93d3b95b-c93d-422b-bdbb-a13b6c91d657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 228
        },
        {
            "id": "b01fc0c7-82b2-4749-a082-ab45df54869d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "c032f31a-0cf4-40ff-a841-eec3ce5b541b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "b0516a45-c04e-4410-baab-12c52fba93ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 231
        },
        {
            "id": "3128ed74-fdaf-467e-b6f6-8a0eb1784502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "169a7ca3-9e2a-441b-a070-ef3b6083c9f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 233
        },
        {
            "id": "ac1663d9-143e-47e1-a8b7-99a4ce972431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 234
        },
        {
            "id": "619117b1-aa32-463e-8b02-9c68eb1003c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "dab4ce3b-9d67-48a1-bf24-5b02a843235d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 239
        },
        {
            "id": "82ad3f94-c902-4ef2-a256-772f4ff7efed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 240
        },
        {
            "id": "16f40302-8590-4f7d-a807-efc7bdff020a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 241
        },
        {
            "id": "24039c4f-54ae-445f-b8bd-8b1b2736f859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 242
        },
        {
            "id": "261a3b61-ada6-4777-85e6-20e6343ef268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 243
        },
        {
            "id": "8270720e-ffb5-47d4-9ac0-20562613dde4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 244
        },
        {
            "id": "b7ddb9db-67bf-4db0-a521-fc9cd9e32911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "1388398d-cb3a-4368-8cf7-f68f8e14b548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "a324b50e-5b4a-4624-96aa-8212acef7beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "5e95021b-a2d3-4773-a11a-6e669c4fafda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "33b650cc-721b-4ad0-9298-baa47e4649f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "f1207664-dbf0-4312-9d11-0fc200f0b233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 251
        },
        {
            "id": "33eb305c-6f20-4463-8fc3-e3ff11d9fa03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "170ac99d-1686-4c7e-bb92-3fc84a63eedc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 253
        },
        {
            "id": "d18a86dd-e20c-42a0-8fb4-ae6776b04769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 255
        },
        {
            "id": "96e4be8f-95f7-4aa5-b53e-e26da2fa2eea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 256
        },
        {
            "id": "34450aab-20e4-4e62-ab3c-e5d90812eb7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 257
        },
        {
            "id": "de6bc056-8c1c-4847-aebc-b94dcc9ecee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 258
        },
        {
            "id": "ac976be9-364c-4e16-8ff9-75fb9c6145ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 259
        },
        {
            "id": "ef884759-8bc2-4d45-b6f8-8c58b7d37b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 260
        },
        {
            "id": "e9544257-2893-414b-8693-91a68c746577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 261
        },
        {
            "id": "1a4ea6e6-8b3f-4be1-ad83-6af8a608e88b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 263
        },
        {
            "id": "3f211826-8bfe-4b31-a3e0-fcab984a1b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 265
        },
        {
            "id": "5a262482-a3ff-41b3-9bf7-e735bdb7b515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 267
        },
        {
            "id": "b330cb37-d1df-483c-931d-43070682cb60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 269
        },
        {
            "id": "212db631-00e9-481e-bd08-1d433a99e8db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 275
        },
        {
            "id": "1a8e372e-d55d-4c99-8471-1e0a9dfccb55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 277
        },
        {
            "id": "70bf5944-8567-4ded-a9bb-9e9916735aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 279
        },
        {
            "id": "55e365bd-af0d-49b6-be82-522006884b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 281
        },
        {
            "id": "b44bc5f0-ef82-4ba2-ae73-169aa2292f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 283
        },
        {
            "id": "53992274-0d65-456b-ab7f-3c509571bd3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 285
        },
        {
            "id": "6cc2e92a-1969-4b18-8009-3b28922fa6d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 287
        },
        {
            "id": "4f8813d0-7fce-4c5a-9efe-4dd1c094182b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 289
        },
        {
            "id": "a9816564-499f-4b35-88e8-80240984df2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 291
        },
        {
            "id": "12bdd248-2652-4804-add1-db3edf76e2f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 297
        },
        {
            "id": "5f40dd55-2e49-4235-94b1-e02cd064644a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 299
        },
        {
            "id": "a5098037-4c65-4ff5-84d6-3cd13a0bbd33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 301
        },
        {
            "id": "7c0cafe7-310c-41f4-9d1b-90e554a3a866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 308
        },
        {
            "id": "cab49eb9-5ac1-4e8a-935a-ec3db318910a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 324
        },
        {
            "id": "384da11c-2447-4b8b-b690-7904793bb0a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 326
        },
        {
            "id": "c68b9990-2a66-4d94-b1c4-4020496decf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 328
        },
        {
            "id": "b4d9b440-0e38-449b-b4a9-dbff96f3b28a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 331
        },
        {
            "id": "a9518c05-3dbb-409e-8d2c-6a580799bc81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 333
        },
        {
            "id": "06207dfd-27a9-4e86-8158-da1e1475b969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 335
        },
        {
            "id": "6ce94bca-2327-45ad-a622-e9a3c729a484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 337
        },
        {
            "id": "262cfc94-03f7-4154-a5a2-b3d819b63916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "653f31ae-1185-4b3c-bc7c-96fdcfdbdd16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 341
        },
        {
            "id": "416e9626-337c-405b-a619-62202d853f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 343
        },
        {
            "id": "c66c7268-6136-498f-b5f1-2f4e190516e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 345
        },
        {
            "id": "c648d947-3736-41be-bb8a-1c5170c4c185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 347
        },
        {
            "id": "973fec72-b82c-4732-bf96-3d2a79c3fb19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 349
        },
        {
            "id": "e398058a-166b-43d8-a1c5-e8fbfdb1c488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 351
        },
        {
            "id": "ab11b297-f377-4e0f-bba0-f6772cdf1a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 353
        },
        {
            "id": "99f2ba23-6a7b-4cdc-aae9-089b46b9ecc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 361
        },
        {
            "id": "347b89d3-a693-4be0-bb32-c08f2a762f84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 363
        },
        {
            "id": "b2fc29e7-866e-4dc5-88ef-06eb9dc4b4cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 365
        },
        {
            "id": "7ddd42e3-2c2a-49b3-9be8-576e24bba767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 367
        },
        {
            "id": "4eacc73d-df72-4654-871a-334de9213019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 369
        },
        {
            "id": "6277f213-1659-4f5b-a6db-b1b9ba39bbf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 371
        },
        {
            "id": "6512aef3-ee11-40b3-8e5c-9923d081e956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 373
        },
        {
            "id": "4984eba6-7aa6-4a37-a086-fadf0afd1651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 375
        },
        {
            "id": "2cb8a9ec-6c3b-4821-8a8e-25ab25f60bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 378
        },
        {
            "id": "1b32576c-e342-4633-8095-a3b3126dde2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 380
        },
        {
            "id": "f4e64af1-c696-4281-951d-f67b0f0e8267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 382
        },
        {
            "id": "f15c6f69-0b9d-4876-bd2a-52dadcf18275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 506
        },
        {
            "id": "f504e9b9-9f45-4580-a123-39eab50572cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 507
        },
        {
            "id": "c881a90d-5622-414c-bf61-ced708a5eaaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 508
        },
        {
            "id": "5eafd291-86ff-443d-9331-59c1deb646c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 509
        },
        {
            "id": "ec1bed2f-9aa0-424e-a6d6-23e308863158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 511
        },
        {
            "id": "e266ca54-ef40-4490-91dc-06ea8bec1fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 537
        },
        {
            "id": "addf4c20-11ea-4dd0-b701-caf2a1224f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7809
        },
        {
            "id": "11828d71-4af8-445c-99e6-bfb801e122b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7811
        },
        {
            "id": "aae980c6-dc2d-4b99-b3d8-b24ca42bf47b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7813
        },
        {
            "id": "da6cc74e-0a1d-4a5b-8fd5-dfb859ebdc7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7840
        },
        {
            "id": "0bd4535e-078c-4174-9bf1-0f5e8288af64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7841
        },
        {
            "id": "cf7ca0fa-d1a2-40af-87ed-b5da0a3ad19f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7842
        },
        {
            "id": "36557878-8334-435b-b5b8-0f289f55f325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7843
        },
        {
            "id": "062685bb-4e43-41e7-9065-38239521ba9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7844
        },
        {
            "id": "fe2d9037-1a0d-4005-8471-9190b189ca13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7845
        },
        {
            "id": "54e4ffa0-5b56-4c45-a34f-319eb872c6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7846
        },
        {
            "id": "6484e14d-3acf-412b-9681-0371bc4d905a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7847
        },
        {
            "id": "920bb629-98b9-4cab-b59b-2d9a57212e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7848
        },
        {
            "id": "97e2d428-94f6-4102-90df-281bdc6acf8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7849
        },
        {
            "id": "dff96f4f-5bd6-4265-a6bb-9cb2c508aacd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7850
        },
        {
            "id": "4967817e-f14f-4f29-b746-63de09efa126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7851
        },
        {
            "id": "ea11eb28-77d9-4c91-b4c4-498027d70345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7852
        },
        {
            "id": "48a7c8e1-5c64-4f7c-a4aa-d89c3c890b14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7853
        },
        {
            "id": "16c91879-2af6-46b5-866c-77bc66c1e86a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7854
        },
        {
            "id": "1ca90919-e946-4bf7-b667-05e6ab1a660d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7855
        },
        {
            "id": "8fab98be-ea17-4ec3-bbbc-9e86ee217b1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7856
        },
        {
            "id": "5c120d72-5186-43f4-8157-cd9f562af6a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7857
        },
        {
            "id": "5902e7ab-5b6d-4e93-9a64-57acb71af578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7858
        },
        {
            "id": "280d5740-8671-4ac9-af39-eb474e61495c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7859
        },
        {
            "id": "16ae309b-4888-4388-9773-48dfa95ea91d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7860
        },
        {
            "id": "1a618b0b-06cd-41cb-8cd8-ffd4ae83e7d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7861
        },
        {
            "id": "e6865063-207b-4cfb-9ed7-c7f18b7838ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7862
        },
        {
            "id": "b22fda45-cbda-47b1-a028-279f74aa3931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7863
        },
        {
            "id": "55482a46-2f10-4cf1-9fc5-56829b6db6c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7865
        },
        {
            "id": "29049bb8-a316-4d5f-8957-5ac0a77338f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7867
        },
        {
            "id": "a6ad9210-581c-4b90-be2a-a17f4768562f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7869
        },
        {
            "id": "9e1e11fb-4fee-41eb-870f-dfd705b70adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7871
        },
        {
            "id": "5a7202ce-1422-4b1f-bcd0-32072f2ef246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7873
        },
        {
            "id": "393052af-0908-4099-aabf-449415d02850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7875
        },
        {
            "id": "a3ec238b-dc4f-4fc9-9c26-92f628271225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7877
        },
        {
            "id": "05dfb956-1be5-460a-8c34-e77f238b9bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7879
        },
        {
            "id": "2983375a-cacb-4c5c-897b-6d1ec1cf018b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7923
        },
        {
            "id": "0cc8d4e3-87fb-4a14-b333-f6e6c01160cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7925
        },
        {
            "id": "75dadc4a-ffea-4ca8-b9b8-88fab9d0ea5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7927
        },
        {
            "id": "0c47bc1f-a8f0-408e-ad66-30ba962b7550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7929
        },
        {
            "id": "0d1b80ae-8651-489b-9105-f28c49494859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8208
        },
        {
            "id": "1cc59311-1993-4145-b635-195c9a79d6ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8211
        },
        {
            "id": "e299fed2-500e-44f6-aa0e-9cf0754e474f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8212
        },
        {
            "id": "8711a65a-cc79-46ff-b568-9e556b462662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8213
        },
        {
            "id": "cc4cb169-8c94-4d31-9519-cbae55731b37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "331c877f-f579-4063-95cf-fab34dd58e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8249
        },
        {
            "id": "58c76601-2e43-478f-b961-a57b3b670a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8250
        },
        {
            "id": "55efbf23-d5cb-4295-a897-deb1ae6b7135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 171
        },
        {
            "id": "9c04116d-632e-42e4-a150-5ea3ee874212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8249
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
{
    "id": "c74d918e-6b9d-4552-b7fe-42333f767eea",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fStun",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Corbel",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "213698d0-4ce9-497b-934d-6cb91e56269f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 63,
                "y": 65
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "942a9571-cd16-4cce-9104-70617146b235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 81,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "212f2d87-8d3f-447c-a0e7-a2057b359266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "690f65a4-b321-48c5-8770-f3625e57b3bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "78dd6a5b-aca0-4037-9b42-754f8f95f54e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b085b770-9c14-4e8b-866e-aa279f26dbf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6a24669c-6539-46f4-841b-c1643b152e7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6733cf3a-a2bf-447f-836c-504fc8eda500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 58,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6fbc7026-c42e-4314-805c-de6e18bccf8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 23,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4f9315d0-07c9-47fb-9bcf-b5fddcc774c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 212,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "73497f49-752b-49a9-a5b7-ea70d54150c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 113,
                "y": 23
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "af7084d3-dc3b-48a1-87d6-1d3323340d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 173,
                "y": 23
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "759d2da1-8f6a-405d-acd0-e50bc6a6ef01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 68,
                "y": 65
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b369bcb6-a1fa-48fd-8c7f-46f77a210da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 240,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2853b0e7-896d-41e9-98d7-0f0cd2ac9e54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 85,
                "y": 65
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6ff74647-de76-453b-8c93-499409f22d17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 233,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5d58a8dc-1a83-470c-b2ff-1371b1aff9b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 227,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "573f2cc3-fde6-459d-a64e-604695ff452c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f0da92e5-77bc-47a7-b72b-d0c426707765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "840126ba-e9d7-41b4-a550-2b0c297c07f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 155,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f713a9d7-9cc7-4c49-ad07-fc9dc5077a6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 163,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "acf21853-c861-4e12-94c3-29354c83d609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 128,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "72d058c5-e6e7-4e39-ab1c-a1ca9a3065b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 146,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "979ad37f-eeb7-49d9-90a0-c4f927a02232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2bca02d5-3e7d-479c-81dc-ae7c5392dbdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "10b228ef-967a-4d57-afc5-c3405b5c3d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 153,
                "y": 23
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1949cb5c-fe8c-40ca-9905-4dd30e8a4190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 89,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "63a87442-e189-480b-938c-108b675acea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 53,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4475ac56-39da-47d7-bd25-31118530e597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 133,
                "y": 23
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "760f82fd-7743-4901-9a7c-accd6fc4607c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 123,
                "y": 23
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c7f47c7c-858c-4828-884f-a2e754ff1d0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 143,
                "y": 23
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "03a7c38b-1ce4-4f8c-ab97-509fdf8efa0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "20e27aeb-0e70-47df-83d8-4108927f8f3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "07d1c5e1-82d2-4f2f-b5af-05b1db2922c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "254aa6f7-dd03-4524-80d1-8e0b2ef83c7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 73,
                "y": 23
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "db642f99-17e3-49c1-9dcf-58fca8bb43fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 63,
                "y": 23
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f0bcda19-5308-4bd2-bb4c-a26e036c89e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d1598efa-35a1-4c20-b57c-0e053e60d28a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 74,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "63439a9a-22e5-4245-a76c-5862a439d2a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cd8beb38-cd22-4df9-8ea4-236778d778bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6defdbbb-b7cd-4ac4-ac83-d0d729cd8e5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 33,
                "y": 23
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "edef4124-d574-4afe-aa9a-a8d7b6627166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 77,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3288aab6-d046-44d3-b153-363115e02335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f00b7770-ea57-43c3-9e4f-66b3e938ac30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "87517abb-5392-450e-9c87-0c9a9cb7033f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 119,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d19f2880-e4d5-4286-aaa1-6922e6d454b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "64f6065b-1142-4959-a175-cc379bcfbff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6a1f8629-1d1a-4bec-8648-99d99eed7e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ae8057bf-8bd8-492a-acdc-6474095bf755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 137,
                "y": 44
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "292a05b8-ae39-40f4-94a4-59f31c260bf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "aa4e6338-4621-4d78-8058-da90e59a3b4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 83,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "848cee36-6ab4-4d5e-818c-3881ad36ed70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4546a661-3751-41b0-b5e4-89b150aebac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "784b707b-32a8-43d6-bb8e-bc3923c1553e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e7da2a22-d2d8-4497-88db-012c13c3bba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "83de215f-a97d-4c84-8f7a-cf01cce7bd48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "052c762e-c63c-4c4e-b050-9d1f401e7a6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c2e9e516-3507-4a86-9e98-a2d2bf494cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7ad73f7b-64d7-4438-9c7c-2fb6331f45c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f3be081a-c818-43ed-a17d-77aa9941e35a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 35,
                "y": 65
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c4431981-9d2b-4d6c-8d6e-dc80742787ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 164,
                "y": 44
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "49bbe5c0-6c1d-45b8-9738-dd5aac8a839c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 65
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c9008b71-d820-4999-8705-1c247c97ba64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 53,
                "y": 23
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0b3b1cfa-4735-4f81-977b-00356b144a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 43,
                "y": 23
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3aff44de-859e-4b92-8166-9094e44c4b2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 47,
                "y": 65
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "71e8e181-4cfd-4b2a-9008-e625e8cb9fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 182,
                "y": 23
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3583f3e6-6533-4b0c-a887-d61b5496caa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 191,
                "y": 23
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a3fb10a4-084e-4174-ba03-4b4c19fab862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 172,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4f4f0ab8-cd54-4bb8-9ccc-6a8b0d650a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 200,
                "y": 23
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "814ebffe-cf13-4a3a-98b8-5ae7f7bad741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 209,
                "y": 23
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c65eff73-299a-46e9-8a64-1d1537d1ad05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 188,
                "y": 44
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ffa65b9e-9cfc-4d02-814e-ed61fc5e87fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 44
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0ee31bfa-3204-43f6-ac1d-b77c76fb371d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 218,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2773edd8-e6f7-4d99-8a7c-c09959e5af63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 93,
                "y": 65
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "601475cc-ecc0-4904-94d2-57cb445adc52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -2,
                "shift": 4,
                "w": 5,
                "x": 247,
                "y": 44
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "01285f71-ccee-4c31-aab3-0916345b7b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 236,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f89378e0-8ec8-4492-96eb-942ed6686d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 73,
                "y": 65
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8cae03d0-18eb-40be-992b-46eda41b1554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "482b7adf-685f-460c-acfa-9d0b5e0d9ece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 196,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "40873897-8645-4bfd-ba53-739ec51b9219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 245,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "62e8b576-04f0-4d3c-9837-2be08e17153e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6e884d4f-cd30-4ce5-acd4-47dda9f556c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 44
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bf76acd6-6af8-4602-b9d2-0c77e30a3b98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 226,
                "y": 44
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d8098ecc-a1bb-47b1-9f41-9516364835e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 219,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "639dca80-19da-49fc-94f6-da53030fd162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 180,
                "y": 44
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b61e33fa-3525-4114-818f-ad1ea0fb0fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "83a90559-45b9-4175-8fe1-03afdb4646f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 13,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "9a5680af-0e74-40d9-82d5-c5bddf18d616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f7995804-efbb-4304-b265-66ed3c2d596d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 93,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7ca758f0-9ec3-430f-8468-16bcd24ffbd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 23,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8e37c561-5b18-4893-9357-08d49a01a888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 204,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0523d3d7-1369-4ae6-8b4c-2e509851b440",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 29,
                "y": 65
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "5dfe9c6a-f977-4130-b1d6-ffcebb3923eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 97,
                "y": 65
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f408307a-3f30-4e88-b365-f10cc8499f33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 41,
                "y": 65
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "865f20a9-707b-4bc6-8a97-639331ade32b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 103,
                "y": 23
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "cb11456e-0d80-4325-9c99-3c202b7c5eb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "d449eb8e-e198-4ac1-89e9-6e43468727d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 86
        },
        {
            "id": "106cb98b-ce39-4678-9981-c093f86b5fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 87
        },
        {
            "id": "84197ec8-25b8-46a7-b6ee-7b7c2c0751d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "ceed3356-c533-455f-bb2c-df4acda4fa13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 221
        },
        {
            "id": "557914d7-04ac-4afb-a14f-3e7946ea24ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 354
        },
        {
            "id": "acc350b5-ddb9-4485-8ec9-e74efcebf58b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 356
        },
        {
            "id": "46da3f47-d817-4dd1-b158-3737db9f4760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 358
        },
        {
            "id": "294be4f1-3ab2-48ca-b5ba-9f173c886ee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 372
        },
        {
            "id": "ea4a250e-ae24-454f-abaa-9b84354c7686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 374
        },
        {
            "id": "826acc92-b9b8-4a98-bb9d-079545ab3460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 376
        },
        {
            "id": "9566913c-9e81-4009-889c-aa239db1815c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 538
        },
        {
            "id": "66af65af-c1f1-4213-8696-24d90a8600f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 904
        },
        {
            "id": "e3df4d98-8be9-453e-9b09-05b335ea8b95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 905
        },
        {
            "id": "e50ded71-6f58-47d6-a59b-b05334eb1e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 906
        },
        {
            "id": "ba09c91d-f809-4417-b4c6-a99cb7966d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 908
        },
        {
            "id": "e9772c7e-005c-424e-8713-cae521686fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 910
        },
        {
            "id": "c715774c-d498-4e2c-8ba9-64ad0c3e4ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 911
        },
        {
            "id": "10cb5e82-2409-4a46-aabd-5599361f0d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "b71a668d-8852-4861-82f6-f297dbd3a32e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "c39a2fe5-a511-45b3-bcde-ac6457625fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 936
        },
        {
            "id": "a76e56b3-da21-40cb-80b6-9868229ffe71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "2b14ce0f-2b73-44b6-b1c8-c491a6455930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1026
        },
        {
            "id": "0969b108-4d51-47af-b531-c90bf631973a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1035
        },
        {
            "id": "19486110-e09c-4076-b12e-593438793fb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1058
        },
        {
            "id": "5c61d6ec-e1f5-45a3-850f-f81870e1322f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1063
        },
        {
            "id": "425b91b5-ffcc-4dc6-be74-ec8057f938b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1066
        },
        {
            "id": "7ee0d07d-dd07-4c87-981e-275b51d7af43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1140
        },
        {
            "id": "033ea58c-59ba-427c-a5e8-fecdddea06f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7808
        },
        {
            "id": "42205ee2-139a-480c-b648-31904e595cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7810
        },
        {
            "id": "0f6705d2-53a1-4727-9e97-294f4b9199f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7812
        },
        {
            "id": "69f622a2-0bfe-468e-a2d5-0f918470e8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7922
        },
        {
            "id": "da3008fb-c7e9-43a9-af5e-3f6746b52662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "6564f1dc-e212-446d-bd6f-9b834cfac6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "962cd990-5392-4deb-a323-1f1c11c14d2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "f22bd9ac-c448-4f00-8b8d-2b7de6944079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "c3f1b5e7-9615-45da-9cf5-15d3cd7ebca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "0028f2f0-25a3-44ce-9c85-f78caae6115f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "74dae318-73c8-4dbf-87b7-b4999f09f258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 354
        },
        {
            "id": "4ef05ead-f0ab-4db1-99fc-dcde859a22c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "bf09a2ba-421a-4ce4-a2ee-9374f617459e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 358
        },
        {
            "id": "a1a861f9-2a67-4d53-af88-6da43cbbe90a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "3a8299e3-2682-488c-911a-236e5d2faf40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "d0401f86-3954-4d30-8a31-6b802199a957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "82669171-c9e9-4d32-b618-cb659fa747a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "db6e0d2d-29e8-4e15-8b71-6f5787b2c172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "188a6dd1-7572-497f-ab29-697a7b23abf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "4a75f1cd-7c8c-4522-8e88-40e00dc5ef11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "cdba616c-a087-4d09-ad4a-8ea1a3895117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "0ead313a-a079-494b-b786-9422eb843e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "d8f040df-c397-4bb4-a0ba-45c427beaa8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "b146241e-9954-4d86-94f7-0709c132c840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "4663eca4-493b-4093-b5f7-017ba7d8676d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 508
        },
        {
            "id": "aadf667e-2725-452f-b960-f38d3c14cddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8230
        },
        {
            "id": "75a6bea8-8c4d-4ab3-a47e-ac330a2fdc1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "c46f914b-9034-40f6-8942-39290993f047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "1427a721-f6e1-4eaf-8cc7-0e5cfba7c9bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "07edff2a-e38c-4690-bb92-f158c1fa1459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 253
        },
        {
            "id": "0ea28ac3-d289-433e-b239-d70f94431c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 255
        },
        {
            "id": "91038a7f-1340-4657-89db-1323735eaff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 373
        },
        {
            "id": "ad55c3bb-adae-492d-af67-cf0612f0c146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 375
        },
        {
            "id": "25d3c64c-df16-4445-8d3b-8407fdd32460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7809
        },
        {
            "id": "f0140d84-3201-4141-bb53-8b5fdff9da9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7811
        },
        {
            "id": "6cdd5f1b-cc50-46fc-ace4-f5a481604496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7813
        },
        {
            "id": "5fa7a851-a95f-4720-809c-64d0d4af30ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7923
        },
        {
            "id": "aceb989f-b08b-4031-b6fb-690fb502f76b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7925
        },
        {
            "id": "26744f47-95a7-409f-838a-d91148e1ab30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7927
        },
        {
            "id": "f6ea20e8-314c-4f44-a5a3-156723e0b7e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7929
        },
        {
            "id": "8e897d27-78dc-468e-b45e-f66b236c7b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 34
        },
        {
            "id": "ea10024e-98a4-47c2-846c-fdc2ebfe2a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 39
        },
        {
            "id": "0dd49867-01b8-48a2-bca2-d46502698cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "1032240b-fb5e-4ff6-b55d-7a17b6e6e5a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "9f7f178a-efe4-4647-aeb9-02d695fd99ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "394ec281-20c3-4bcd-91ae-00ba834a1dcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "c1eaf471-cfd9-42b4-9fdf-0d9bdc64c20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "14af5a1c-8098-4a34-9ce2-cdec30e1206a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "1b735203-d3af-43a7-875c-eb60719b12d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "43a7b33a-8bb3-4172-b005-8fedf01961a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "ea859cb6-dea1-4f2b-b4b1-652f441b55ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "a636ebb3-9bd8-4ed0-836a-fda52f8a9d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "f403f0b7-5dd8-4740-a5ba-bff9a5245a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 354
        },
        {
            "id": "631ffd4b-1f81-46e2-af9d-6bda8791f4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "1a9df899-b198-45eb-bd08-da45fde84f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 358
        },
        {
            "id": "7e68381d-d792-40f2-aa15-c1996454d71a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "aa3b83e2-1f2f-474a-b74a-a1d44f1138e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "25658ccd-f87c-47f3-803e-22e8214b879b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "c4e3c9fb-5f48-46a9-aaa9-25804aa5739b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "e88e5834-d914-48f1-8642-67612368efbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "425e8816-8f5d-4468-b981-5a02c827a620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "c2d82e1a-d06c-4044-af41-f3c03a6060ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "3143b9ba-852e-48fd-b195-097914ca9f39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "7374f5ca-9321-4253-8ed9-3153daaca601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "38a68bb4-05bf-4fb4-aba2-9c232605a56b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "51bfb318-464c-47e8-a1dd-328f4cf14503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "bea39aeb-3736-4780-808b-ab8c24e8bb24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "08639511-323e-4684-a01b-f68c1e121c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "a8f59557-79c1-4b27-bbaa-05be3cb358d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "34139a60-931b-46b0-b60d-bb6eeb3b2032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7925
        },
        {
            "id": "8534a3e0-2dc6-4dd1-9177-8e15ce5f4da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7927
        },
        {
            "id": "7e35d025-96fd-4191-9d6e-31545eb60b8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7929
        },
        {
            "id": "3396d90d-b03e-4f69-a7bd-c52403d29504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "5b606ac7-640f-4d83-a22f-3cf3895f417e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "138ca6c7-22d5-4d61-afbe-0a5db25cb77c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        },
        {
            "id": "9f82f6ed-c70b-481d-97df-1e433c828a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "9c2592be-8860-42b4-bc12-6423c2d3f53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8319
        },
        {
            "id": "42ac4444-b334-40b1-b8ac-d09f449c565c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "d202a7ac-5227-4e72-8ff1-84b3d708c261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "481a746e-7ab1-442a-942a-3139df537a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "899f598e-28db-4721-9da6-68a7076e8f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "02487ef3-c487-4c7a-8ebe-90a86f3e7c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 192
        },
        {
            "id": "5ce838c6-bd0b-43d7-9a45-614b25ca7667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "59f27394-41ae-45a9-8304-b14ac6735bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "6fea7dc6-8e59-4719-aa9b-8a7c5a3b4708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "16db956b-7b57-426e-b0d9-0464556191f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "00c325a7-13a2-4c2f-9118-de5199166879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "1c1bbb89-bac9-491b-b777-f63bf9037b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 198
        },
        {
            "id": "a8691aa3-2165-46ae-a5b3-f83fb28572a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 256
        },
        {
            "id": "f97c4faa-322b-42d3-88e6-e6bf093723fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 258
        },
        {
            "id": "34d4c111-eeae-4ec9-be54-791bbd365157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 260
        },
        {
            "id": "0021b9a1-cae2-4cba-b0f7-92438831b659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 308
        },
        {
            "id": "d0d1a20b-3202-43b4-b99f-6460867cfc06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 506
        },
        {
            "id": "69b1d79e-2a93-4765-ba54-0c7dcc22b612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 508
        },
        {
            "id": "92500009-c6bb-4c09-b0bd-6714e62818ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7840
        },
        {
            "id": "141e6bde-7739-4aaf-bfee-ea5d93811529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7842
        },
        {
            "id": "19b1c07e-5fb4-4923-b324-fc371cb58e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7844
        },
        {
            "id": "e6f10b55-252f-4136-b75e-3c5ce6a4d35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7846
        },
        {
            "id": "486e785c-101a-47be-b8ea-5ef6b80521ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7848
        },
        {
            "id": "1525e213-5e37-496e-9c41-5b247775a828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7850
        },
        {
            "id": "881cefc5-a010-4552-a78d-ba051ab1d454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7852
        },
        {
            "id": "819e15ef-329b-42d4-b7f5-0813b4984ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7854
        },
        {
            "id": "c3edac65-de07-4458-b998-2d72e8396e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7856
        },
        {
            "id": "47babd3d-990f-4682-a477-6f023ae84b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7858
        },
        {
            "id": "e10fb867-2f1e-4d42-aeb6-e78fc04b857b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7860
        },
        {
            "id": "3f2141fa-8679-4bc9-b997-f336594c18cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7862
        },
        {
            "id": "e22a4c33-96e5-4f5c-8d31-3d7d33b0224f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8230
        },
        {
            "id": "3027e6f7-597e-4b77-b159-f0daa375a95a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "48f3fb37-81ab-4766-87d4-2f79f2c44265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "b55e7632-844f-4364-81c5-930f3698ac43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "c86247b6-e878-4a9b-b348-a76c005cafa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "ca184251-8be0-42e0-b0f9-0fc05fc70cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "a6687279-8760-4a0f-bada-7da14c5060c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "457c5281-5507-45fd-9b9f-d9d06720cab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "20aa7807-7594-4e34-9a11-d2c642823084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "2b22a14c-55d5-4457-b604-5308e377f2d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "5e9f8737-fea3-477f-8bec-f0335c13788b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "7f295c30-d7c8-4adf-af82-f7334556eee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "32c9f161-ce3e-40ef-883b-7b38614d207b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "4d351a8d-d0d2-4348-bad0-9e1a6e13f61f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "ebf12f85-db66-42fd-98d4-a46d02086d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "bbbcbe60-e510-40e6-8cf6-36d1a260428e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "6f21c0b2-7719-49c8-9922-e25f07ca0bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "f51277fa-33a6-41bf-a4fb-af258ab5aa6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "fd7dac0b-6359-48b3-906f-1cf05cea53f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "838d1c91-aacb-46a0-879b-9ec30f13c072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "1328e59b-3386-4bc2-96c8-1350ce11bc28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "98a89737-8341-4678-bf5c-5d1ebbfdced6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "383f963d-1630-4415-bbb7-5b17b10fe0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "404b74c2-6805-41fa-8b19-b6d3fbc75be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "a091149b-3d31-4d43-b200-5dd59c975e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "352aae66-9eeb-4613-a54f-c0e3a9cad96f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "0512c635-0cd1-426e-bd80-42d08959e9b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "9847365c-c6c2-4ec5-ba07-d4a9c0c51e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 187
        },
        {
            "id": "687359c9-27cc-4af7-94e6-eb3edea2d792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "be67ea1e-9957-4025-a82f-0d30180ba3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "b8733b1b-25d2-4567-b523-e90d09c66911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "f597ac6b-a85c-42a3-9a02-b9649f498eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "5b23aa0b-c982-4afb-a281-d7844caedb23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "b8446848-9d7f-46b3-b467-74ac22d0f833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "523eb3eb-5331-48d4-86bd-5c34704ae49b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "a4f8c33a-38f1-49b6-931e-6486bf60a55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "2b9b0b14-5369-4789-86cf-e7944e1203a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 225
        },
        {
            "id": "6a5fbe83-329a-46ba-ad61-28dd13ab30b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "164d170d-e8cf-4063-8222-c5e84e6c49b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "627f98bd-feff-4416-ad83-5f26d6fbc80d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "a7be0662-c0d6-4db8-bffe-b7b6237b4c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "a1363635-86f0-479f-8e83-9c01de5f94fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 230
        },
        {
            "id": "ea07e75d-fdf7-495e-80df-90b28753a3c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 231
        },
        {
            "id": "cf118d07-2ebb-4106-968f-977cd845ecfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 232
        },
        {
            "id": "1a584877-2ba0-45cd-b839-587d9fe94a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 233
        },
        {
            "id": "572df4da-a2f8-481e-be90-863c4346ecbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 234
        },
        {
            "id": "500a2eac-ae42-4178-bf85-51cd9ba90671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 235
        },
        {
            "id": "6aa572a6-d9af-49bc-9718-82f726f89bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 239
        },
        {
            "id": "b0ac14a7-8392-424c-bbc1-a74a73f2f95a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 241
        },
        {
            "id": "84debbb2-c054-454d-9646-22ab3d1f270f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 242
        },
        {
            "id": "371285f4-b251-4718-97e5-1368d46e9080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 243
        },
        {
            "id": "6d7066b9-8a69-443d-8111-119ef86e921a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 244
        },
        {
            "id": "8868414f-d9e7-4fda-8ebe-86782f174884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 245
        },
        {
            "id": "9037df84-19b7-4044-a60f-faef20512cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 246
        },
        {
            "id": "ad41bd58-6485-4d44-a8d0-b37648ed9960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "47c82a4a-d2aa-427e-af13-4e417097ec38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 249
        },
        {
            "id": "ad64afed-516f-448b-b139-786362527c8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 250
        },
        {
            "id": "74ca1004-35b9-40b2-8d8a-78c6d31ed4ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 251
        },
        {
            "id": "8ab33d5a-4b1c-4e04-bfd7-726ce7d7d37e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "fa463423-6567-445b-8ff4-fd4ffea4ed67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "5bf0bd38-00a6-4362-86d5-511ce52ae5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "874a0fa1-f0a3-48d7-bf8e-87c0bc2af47f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "7bacd4d1-314c-4035-9ce6-054bb6f3f342",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "86e9ab6a-307f-467c-8767-c8cdf8aa61d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "0ac0dab4-41cd-4f4a-8f74-2b5041501189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "34f30396-4069-48eb-8134-bbcd4230c245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "b23a9010-a31b-4f09-8d6a-01ee4f4966c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 261
        },
        {
            "id": "e52b7ad0-e596-40c1-bf7c-df786b50f2dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 263
        },
        {
            "id": "e990d72a-903e-4b33-9eea-c2210b06451c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 265
        },
        {
            "id": "f1c749f1-d41c-41d8-947f-2cbc4fae28ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 267
        },
        {
            "id": "42858ea9-6455-4725-b466-71be9620db1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 269
        },
        {
            "id": "3749941e-e52a-44ff-bf53-8e874e635363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 275
        },
        {
            "id": "95097007-8dd5-4030-8995-ab4dc3c23cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 277
        },
        {
            "id": "f2fa9f67-79a0-4bcc-b406-675993166e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 279
        },
        {
            "id": "2f230563-0c1c-4939-a2d4-0e96f10651e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 281
        },
        {
            "id": "7ace5fe9-befa-4e9b-be7b-2f5df3f79e89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 283
        },
        {
            "id": "d08c7cbe-66d4-4548-b4fc-1eb83dc663b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 285
        },
        {
            "id": "b658109c-df4c-4006-b404-868e4a680fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 287
        },
        {
            "id": "c019cf53-a4ce-4e47-9c8e-5f3adfe64d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 289
        },
        {
            "id": "cf3e9be0-2874-4d2f-8e3a-0816b7cd61b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 291
        },
        {
            "id": "c1f102a4-2030-4f93-a6a5-e0665fbadfc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 297
        },
        {
            "id": "62fa0616-34cb-48f2-a5a0-a0a1815205be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 299
        },
        {
            "id": "c7536671-0d53-4d3c-815d-c27ba748c5bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 301
        },
        {
            "id": "bb97491e-7142-4f61-a8b3-4ffe2e27e8f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 305
        },
        {
            "id": "e64b20a4-ea1c-47a0-a853-2283e9e4e7e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 324
        },
        {
            "id": "471f6f29-6771-4d9a-a15e-357340f62839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 326
        },
        {
            "id": "5016d260-608d-4200-a0a1-922ad2ed2799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 328
        },
        {
            "id": "73bd51d4-4046-4210-936e-5866c8c1846e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 331
        },
        {
            "id": "3c50e499-47e4-4f19-a19d-189ee8af7e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 333
        },
        {
            "id": "a3dc729e-8a91-4152-99c4-2237321ffdb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 335
        },
        {
            "id": "880ecd55-1636-40f7-98d0-286928b11551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 337
        },
        {
            "id": "6a73107c-5ea6-42f7-a589-ed0effcd7f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "e9225ce0-8628-4cdd-bd73-c899eacdbd21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "53e9a4b0-9ef1-4e4c-9167-d242ac08ed5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "f9634e67-4c44-4328-bd0a-9dc8712b95b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 345
        },
        {
            "id": "e95c84c8-1cde-4410-ad4a-cc4cb1601c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 347
        },
        {
            "id": "e6198466-dab0-4b8b-80f0-939a60c6bbc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 349
        },
        {
            "id": "65c008df-7d73-4a97-bf6b-cb63cd44041b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 351
        },
        {
            "id": "c2f554f8-1bdb-4f36-8efd-6ee6e18989f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 353
        },
        {
            "id": "d120cb13-bbc6-4573-9a01-890f8104e0d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "9b686453-bdfd-410e-a9bf-08ca5a4f4f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "0e938204-bd97-45b9-b8ad-9008806566dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 365
        },
        {
            "id": "3b5bd15f-6bc2-4855-9c5d-9b6610fc8190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 367
        },
        {
            "id": "7ad18c04-42bc-4983-8d9d-d32055c4652c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 369
        },
        {
            "id": "ece3195c-0f61-4137-ac60-5f2560a600d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 371
        },
        {
            "id": "39ce9c42-3375-4731-9cd8-2ce3152f3653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "1fc231e7-ee79-491f-8ffd-18d45abe6d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 375
        },
        {
            "id": "8f3f4a1c-4f53-4bc6-b66d-aef255eddbcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 378
        },
        {
            "id": "0510f51b-3875-40da-a1cb-9e890890d2b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 380
        },
        {
            "id": "bdc2a006-0e69-49c8-af34-1e40b040f85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 382
        },
        {
            "id": "cf72ed73-a37f-495c-b467-9b448cf40914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 506
        },
        {
            "id": "5267c7f5-afd8-436f-b93d-713e443f08e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 507
        },
        {
            "id": "ea0a6541-5550-4b73-afa7-7da4bb91b626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 508
        },
        {
            "id": "f52a01cf-7d33-40c8-9e61-bc767343ad17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 509
        },
        {
            "id": "6d618dfb-3589-4ee0-8ff9-07b44d780cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 511
        },
        {
            "id": "83f1cdc0-d638-4904-b04e-819c03e5b58b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 537
        },
        {
            "id": "4fd24401-f500-4ffd-b8f1-3ba747b868b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "edf1add3-ae23-451d-a76f-402272af957f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "0dfbf8f0-77d0-4bec-aeab-af8b80176046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7813
        },
        {
            "id": "3e3166a9-054b-4921-a8f4-b054e887a7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7840
        },
        {
            "id": "f5ccc778-f718-49da-9cf2-4b13ce555611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7841
        },
        {
            "id": "bf2390f7-57da-417b-86cd-f6a1b9c7353a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7842
        },
        {
            "id": "e32706a3-f276-44e8-9c0e-5cb20f275912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7843
        },
        {
            "id": "361b922f-f6c6-40d0-ada9-35d84633dd66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7844
        },
        {
            "id": "2fb75954-5fd9-4d22-9a5e-e13cdc24067e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7845
        },
        {
            "id": "0d37e0ee-1b5d-4968-a11e-81d49f7169e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7846
        },
        {
            "id": "4561e71e-1353-4249-b4dd-8806c73cfe3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7847
        },
        {
            "id": "1d06d92d-e025-4bd9-9864-08911de9d6e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7848
        },
        {
            "id": "4ca8faca-0687-4140-bcda-d923f89d108f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7849
        },
        {
            "id": "a56cfb46-d094-4237-85c0-14a0f8b25c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7850
        },
        {
            "id": "0134f0ac-c830-43d9-bd02-24f269f3ac0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7851
        },
        {
            "id": "52f6b52f-95ad-475a-96a4-0b918fce721f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7852
        },
        {
            "id": "54018cfa-e1e0-46c5-851c-98428bf64842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7853
        },
        {
            "id": "0466dff5-87c3-4bd7-9a56-a6ccd262ae1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7854
        },
        {
            "id": "1a91eab2-3b04-4813-bcc7-940016d35a85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7855
        },
        {
            "id": "e4138fee-81a4-46f9-af32-d1a6e53e8b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7856
        },
        {
            "id": "b6d6b3a3-ea83-45ce-8738-d6922b2a5827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7857
        },
        {
            "id": "eba356f0-867d-4b02-ab62-53134044ecb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7858
        },
        {
            "id": "23ff8cf4-0b4e-4078-8744-59e90237e8df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7859
        },
        {
            "id": "488d24a5-cbe2-4c0e-908c-42f661c98cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7860
        },
        {
            "id": "aca002d3-2e5a-444b-bf97-9af6c214f6b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7861
        },
        {
            "id": "c88f62dd-11f5-408d-b7e0-b147f212377d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7862
        },
        {
            "id": "1f6048ef-369b-4a13-ae2c-6298d9ce1f62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7863
        },
        {
            "id": "f9647179-6578-4be2-840c-248612364677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7865
        },
        {
            "id": "0807b3cf-d51b-4a13-b6bb-c3fbaad57d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7867
        },
        {
            "id": "9c02102c-4e94-4b34-bae5-f9a18cdc7f73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7869
        },
        {
            "id": "9e3283f8-c3c4-4652-bc8e-7e57ae3842ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7871
        },
        {
            "id": "12c21c6a-ff2b-4dd3-9faa-bd2a22cadad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7873
        },
        {
            "id": "53d34a81-92f1-460e-bae5-eaa3d833b1e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7875
        },
        {
            "id": "4f427aba-f39e-4548-8865-4e5bcf6b8a72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7877
        },
        {
            "id": "f017168c-2c53-4145-9487-b7ac1bc3f144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7879
        },
        {
            "id": "a966e170-8d11-4725-8fb5-5e8481d5efb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7923
        },
        {
            "id": "fcaa692c-730a-4f09-b01f-cd89bacb4153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7925
        },
        {
            "id": "fb76dfc5-533c-4882-9578-637e82fbceee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7927
        },
        {
            "id": "1ed80964-9167-4600-9fcc-df36698bc37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7929
        },
        {
            "id": "0e43a37a-3b01-4fc1-a61a-4f5d3921286e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8208
        },
        {
            "id": "32347710-64f2-43de-badf-11562a871577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8211
        },
        {
            "id": "6d743098-0d5d-4b48-88d1-dd9521a414c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8212
        },
        {
            "id": "75058101-4c26-43f7-9ac1-486ba677c64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8213
        },
        {
            "id": "9255d995-a323-49e8-b161-a79ea7230579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8230
        },
        {
            "id": "730002e7-2dc2-43eb-9b65-4f6607315d0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8249
        },
        {
            "id": "79e0325b-41d1-4b18-b1f3-86f216532606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8250
        },
        {
            "id": "7a496ce2-f706-4412-a875-5c119cd64219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "735b0802-7eb8-4ccf-8dc0-d12207e76bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "cbc95d04-81fa-43fa-8502-16b789b8b82c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "7c8103a5-4cdf-42dc-8573-358fd7eada4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "338db896-96c7-4865-9ddf-620869682eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 171
        },
        {
            "id": "500e39c4-7167-4d74-a784-9af8e278ed4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 192
        },
        {
            "id": "c570ef7e-2a0c-44c5-8620-74ab66484226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 193
        },
        {
            "id": "a5c9c0e1-7c4a-4896-8309-40af29da79c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 194
        },
        {
            "id": "ba7b2b87-8907-4461-8790-f651ede21851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 195
        },
        {
            "id": "cfb42b56-64b2-4c8c-9345-8ebe074e2c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "3dd479f1-6d94-41a1-b4da-7d3bf4f998c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "914dbaa3-784c-45e6-9254-504c7f8c18e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 198
        },
        {
            "id": "45a94b93-440f-4521-9b1c-2e6865d05b99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 256
        },
        {
            "id": "59506fc0-40ab-40d1-8f23-23321c253f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 258
        },
        {
            "id": "939dbc0c-fdda-43fe-92a9-32fadf9b1adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 260
        },
        {
            "id": "f3632d31-dae0-4bc9-8686-dcb553c3a3be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 308
        },
        {
            "id": "b599c6e7-90b8-401e-9406-8a6c9c3a0925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 506
        },
        {
            "id": "5921bf6f-4893-4257-be23-4163666b48bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 508
        },
        {
            "id": "c769008a-ffb5-41bd-9d65-80e79a59725d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7840
        },
        {
            "id": "0b1caba9-fa46-4e8d-a9f7-6afc1b2f6f7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7842
        },
        {
            "id": "6cc68d86-8d02-4c62-9084-d323cec7538e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7844
        },
        {
            "id": "c3303fac-d22a-429a-bc5d-67446fd167fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7846
        },
        {
            "id": "47ddd862-efbd-4c82-ab4c-8e95216b89ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7848
        },
        {
            "id": "2a64a622-5191-4c8f-820e-8babee096c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7850
        },
        {
            "id": "4ff19182-bb5b-4fcd-ab41-8d25f3ca7904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7852
        },
        {
            "id": "d25ad148-4616-4ef1-9b5e-a276b63ae1d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7854
        },
        {
            "id": "9af60919-1896-4bb9-87f7-a4d3328ca4ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7856
        },
        {
            "id": "54c193ec-cb9e-43d1-b87a-9670df623973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7858
        },
        {
            "id": "95576d8d-2d13-41b8-82ce-cf1b83c151c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7860
        },
        {
            "id": "7cf84f60-a2da-413c-82f5-723a92209e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7862
        },
        {
            "id": "cfaf0c65-f9cd-4ea9-bcae-faa98b73e496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8230
        },
        {
            "id": "2c19d8da-d356-4fee-9826-a1638bfa4eb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8249
        },
        {
            "id": "9a05da89-20ff-4ea0-9f60-2a8fdf5d1439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "10c1cf13-c81e-48c2-b7b2-18d2d2a7c12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "4666f47e-f865-4a0c-a76e-ac8aa5643361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "5f345296-9daa-4cd8-ae3d-f60081ed6ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "5fe2fef6-cc71-40e3-87dc-e7a5d482359d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "ab5dd588-405d-46c5-8919-1a48b8a4b45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "452ee247-e407-4807-9a6d-f7cd7e2f5262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "092ee1ee-0c20-4aef-8e49-6f6f174630bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "8b3d9292-493f-4554-b7f3-c0ee66d3605c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "59abd70b-9ab1-4e2c-934e-0856e7ead2cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "78c8b32e-5acc-488b-a62f-6cb477c7ab7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "b15ac842-c288-4c6a-a69b-400cb9a0289d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "dba6ec38-8fd0-4c24-959a-cf1791d53852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "773e181b-95d1-4d5d-a2a4-1c4d5db62287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "13efcd32-192d-4c24-8dea-f0f7ce5d0125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "29e97990-71e9-44e4-ab3d-f57a866c0e7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "7d71b1c4-f526-4439-8538-c4473a3f679b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "0d978329-8088-4751-b4bd-2c219e8b91ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "c9513997-568d-4a77-b305-ee3cf024ff1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "73cc4cef-0620-4582-913c-7351feaa9c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "cb3fbc22-e337-4a3a-bd81-c30faf83c804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 171
        },
        {
            "id": "8c2699da-e330-4b80-b551-b8643c768876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "3c273459-0e96-4ec4-8df7-ee8d65478f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 187
        },
        {
            "id": "becb723d-6c02-42ea-accc-7212577a242b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 192
        },
        {
            "id": "6c29cb41-824e-4b77-9954-0bb968072c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 193
        },
        {
            "id": "6a4f7893-4609-4cfa-9ac9-64eb6dffe6f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 194
        },
        {
            "id": "3b44c203-32a7-4881-82cd-4b05f729a865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 195
        },
        {
            "id": "6c5ea92c-b242-46e0-bbd8-34c823d32f8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "d3c07af1-1b49-4fbe-bbb3-adeffcdd65fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "291865a3-7061-4d4e-a64e-a2c693eadd4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 198
        },
        {
            "id": "8e4b34a5-ef77-4031-8cec-da1ca4fee591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 224
        },
        {
            "id": "b4eb1b0e-213b-4144-8fdc-08da2029365f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 225
        },
        {
            "id": "31ea0c26-5f33-437f-9271-cf67e1f03d4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 226
        },
        {
            "id": "aa37b0e5-6d96-494f-988e-d3a244292f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 227
        },
        {
            "id": "edbae8b6-45a7-4107-96ec-f71daeb5412b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 228
        },
        {
            "id": "be49f5b5-1d79-4bf6-8541-4bd482abb551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "7ddc98af-6860-49e9-9c58-66460cbc8445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "964adbc8-4a86-4565-b1f0-0b6d17359d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 231
        },
        {
            "id": "1548b167-f715-43e9-9745-95fa5fdcfa0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "c2d18733-3f63-4a3a-9ad7-2ffb2f15e67f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 233
        },
        {
            "id": "b693dd0e-822b-42ca-9bc4-9530880baa6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 234
        },
        {
            "id": "66ec47fb-c22f-4244-bff2-c38f2153f879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "318cbeab-eae6-4a8f-9dbb-f342ddc35cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 241
        },
        {
            "id": "65f1e400-393b-4128-a44e-122ce6b60806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 242
        },
        {
            "id": "90c4b851-ea82-4bc9-a9cf-6479de029c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 243
        },
        {
            "id": "086b51f1-8b6b-4fb0-92de-6aa160e0995e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 244
        },
        {
            "id": "46151486-71b3-4343-9a3d-5953dfe9c10d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "972f22c3-dfc0-4f3d-90e8-28864724b608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "64e72f77-360c-499b-a74d-29f7cabf5fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "354622e0-a15a-448c-a918-1f6a05ee89c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "8321386b-7f3b-4ffb-b607-57c0eb8a24f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "b7c7d34c-7c31-4f9a-9814-0b00b672e806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 251
        },
        {
            "id": "d6722175-e9c3-44bd-b73d-76586f109eee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "481892ff-2567-4555-a74e-e838a841960f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 256
        },
        {
            "id": "7217ca9b-d743-40d0-84ef-3a50aa750ea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 257
        },
        {
            "id": "5b6792be-54e2-483a-9d95-46d82a05976d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 258
        },
        {
            "id": "4fb220ea-4794-4f7e-9df2-6962053ed98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 259
        },
        {
            "id": "b1434398-5b1a-40b6-ba1d-c44b89c9bc7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 260
        },
        {
            "id": "14263593-f996-4d21-8bb8-cf2b681b7dc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 261
        },
        {
            "id": "fa99cee2-1bb5-41bf-a33a-3650f4123577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 263
        },
        {
            "id": "84f9a239-2d42-4fd5-8a21-d607a94d5ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 265
        },
        {
            "id": "d9e18d40-78e0-4d17-bfbe-b0bd2fd285dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 267
        },
        {
            "id": "cff79818-cc1e-41c6-b803-8ad5d0468d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 269
        },
        {
            "id": "92d17995-a797-488a-bb03-823f3c8e33e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 275
        },
        {
            "id": "4a43f2f0-282b-4068-afb2-33a74700fa0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 277
        },
        {
            "id": "8e867435-bba1-4434-a764-e127e164d84a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 279
        },
        {
            "id": "8f8ff6a9-db75-4d38-90e4-f5dca1717ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 281
        },
        {
            "id": "b4608e4e-e41a-45ca-8cc7-335a57f20bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 283
        },
        {
            "id": "d749695e-fcc4-4db1-b514-31acdda27149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 285
        },
        {
            "id": "49c35b38-b739-4d86-8064-daf714d1155d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 287
        },
        {
            "id": "449184ba-06a9-42a6-a0b4-f0ca6e28f098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 289
        },
        {
            "id": "1db8703d-21b0-472f-b78c-287a86ca051c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 291
        },
        {
            "id": "1bc31fb0-0440-403d-a892-7c38013649e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 301
        },
        {
            "id": "90310dca-b512-4188-84ec-3d605e3e2a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 308
        },
        {
            "id": "56fb1ca0-b063-48a8-b47b-4b0d39784abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 324
        },
        {
            "id": "a6254044-5d71-4cc4-b03c-b8f489b7e026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 326
        },
        {
            "id": "b3a7c0e6-c9aa-4a87-bf70-19dc67d3c2df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 328
        },
        {
            "id": "44f4e185-a4cf-417e-b64a-09b82018c909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 331
        },
        {
            "id": "2b852154-c008-40fd-80d8-21f937eeedc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 333
        },
        {
            "id": "9e796b8d-3e13-419e-ade9-5b7140715e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 335
        },
        {
            "id": "78dafcaa-16dd-499d-afb7-41908d23e8c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 337
        },
        {
            "id": "a236837c-f0f8-42c7-b996-1a75746dfa10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "dc62db05-65e3-489b-9559-fc0fb0d17221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 341
        },
        {
            "id": "983f10b2-71f2-4bb3-b44c-3e9f4ca67c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 343
        },
        {
            "id": "4eb2817c-4d93-432e-9bf8-ad0adeffe800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 345
        },
        {
            "id": "5206fb6f-ae02-4581-b756-bfb69a173050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 347
        },
        {
            "id": "5b5b8229-b6f5-495e-9aa5-0ab715d632b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 349
        },
        {
            "id": "f3c6c601-bb2e-4cca-b6bd-6ae0614f0eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 351
        },
        {
            "id": "5ea1f66c-7773-4cb2-aa6f-980a7d953022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 353
        },
        {
            "id": "70a8aaab-498e-4d16-b3ff-42a74ff540ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 361
        },
        {
            "id": "c4fce1b9-41bd-4308-aa77-87c4a0e08a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 363
        },
        {
            "id": "e8516002-f031-4de1-a08d-083d96308a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 365
        },
        {
            "id": "2dad73e2-55e8-48ff-8d76-99ead43e5689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 367
        },
        {
            "id": "4afa7a9a-50a3-4c3c-a7a0-35bf91ead034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 369
        },
        {
            "id": "868fd0bf-1709-46d1-895a-bdbca2b1abdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 371
        },
        {
            "id": "45151d80-5e55-472b-8980-b49ef40f5278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 506
        },
        {
            "id": "e3a7edb5-c71c-4fcd-9c65-b95bbf990238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 507
        },
        {
            "id": "fde6f2cb-3ae3-4738-b738-eea6acfde204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 508
        },
        {
            "id": "034003d1-9c14-4943-afc2-37f7a73aa08c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 509
        },
        {
            "id": "d75d0f75-f19b-4923-9567-554f2de80cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 511
        },
        {
            "id": "662c0765-d3de-433f-9074-01a7e36c35c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 537
        },
        {
            "id": "9dd222d8-9c96-4c70-baea-504e41a77c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7840
        },
        {
            "id": "adf27830-2baf-4579-8b39-b78a6a826433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7841
        },
        {
            "id": "d3c97a8c-d4d8-42b5-a25a-08493f745e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7842
        },
        {
            "id": "7b1302ce-2153-4d2e-904e-042e47c8ef39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7843
        },
        {
            "id": "11bf5405-5333-42d6-8a4e-18876f417888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7844
        },
        {
            "id": "6a099b07-e7b8-4811-a010-09b96821a644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7845
        },
        {
            "id": "a949addd-0d38-44f1-b1e8-8f3886c3e273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7846
        },
        {
            "id": "9a80f238-669b-4d77-9d6a-368948767c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7847
        },
        {
            "id": "a0870ab1-067d-4f29-8393-38c636722a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7848
        },
        {
            "id": "bab92916-be39-4d32-a53d-29afd5dbb47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7849
        },
        {
            "id": "8a2ac60c-3904-4054-828a-577b99e40bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7850
        },
        {
            "id": "e2eee1f7-0ba6-4a89-8c9f-fcc06385cabd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7851
        },
        {
            "id": "68c7bcd7-270c-4da0-8c15-822cbd59c087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7852
        },
        {
            "id": "8dc759aa-122b-4da9-a7ae-c8734f5c1e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7853
        },
        {
            "id": "178d6296-9a7c-47fc-bb0d-4d91d7a83763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7854
        },
        {
            "id": "7cbb5119-9ffd-45a1-b471-9c538ee0c986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7855
        },
        {
            "id": "3459fef9-0762-4655-9b87-d818e3c49062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7856
        },
        {
            "id": "8dd5989e-b087-4ce1-b994-6adfbdec00e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7857
        },
        {
            "id": "c2078f68-2113-4205-aea9-8fb349cd9704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7858
        },
        {
            "id": "bb6aa526-0b04-48fc-bd42-0ae52f3d5573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7859
        },
        {
            "id": "6b488d8c-2e0f-483f-90a2-4b35665e9f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7860
        },
        {
            "id": "1e6797b7-a534-43e5-be2f-97b86c1c3ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7861
        },
        {
            "id": "7cbd1e80-69d0-4516-b5e5-dad8a2b04f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7862
        },
        {
            "id": "470642bf-aaa3-489d-9f10-567eb5622c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7863
        },
        {
            "id": "8131c719-64b2-4c8f-9ae1-67a6edef3f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7865
        },
        {
            "id": "e438adbe-1a3f-4ed9-a142-b1adfa3038c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7867
        },
        {
            "id": "464d43c8-4c1f-4e95-8c5b-d310985b87f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7869
        },
        {
            "id": "75d43f69-1976-4688-8550-2e82a39aedd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7871
        },
        {
            "id": "d5970da4-5ce8-4cea-9325-84fba92a5aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7873
        },
        {
            "id": "e56ce250-db3e-4a7d-a139-3cd737f071c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7875
        },
        {
            "id": "6cbb6d97-c6a3-4fc8-b267-9becc5388d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7877
        },
        {
            "id": "9ad63eaf-5466-4c17-993a-872b24517c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7879
        },
        {
            "id": "4ceb3f4a-31d2-4f1f-b157-19013e023856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8208
        },
        {
            "id": "3d05f472-1d75-4e73-bf64-bd9b9ac07c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8211
        },
        {
            "id": "1102baa1-86d2-4d63-b7bd-51bc893e65fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8212
        },
        {
            "id": "36759909-8082-489f-ae21-babcd0a40937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8213
        },
        {
            "id": "5581746c-699b-4022-9ab8-bf3d0feaaeed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8230
        },
        {
            "id": "7eb0d3e1-da40-42a3-9b8e-647dfd09d801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8249
        },
        {
            "id": "299a2a4f-9475-4383-9c5c-2c0059bd6f24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8250
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
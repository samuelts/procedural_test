{
    "id": "9e35be9a-c8a8-42b9-af08-c96577d05e07",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Corbel",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4097b0ca-235f-4b3a-b96f-8e3202ee008a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 29,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 157,
                "y": 126
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "596fed35-5d7d-40e3-9b6d-4a98d36ff8fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 150,
                "y": 126
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f867adbe-c8e4-468d-9c46-d53af9770ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 44,
                "y": 126
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "661cbff3-516f-45cf-ae1b-c0d663d95ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 67,
                "y": 33
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "274e5122-495c-42c4-8a99-346f72f43090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 93,
                "y": 95
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5044c1b8-8a7a-418a-8aeb-46e502b2eb74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 29,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e23192bf-296d-4d7b-a942-5a12909ba168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 29,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "330b631f-a947-4039-a858-b06ba34626a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 184,
                "y": 126
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c7ba0853-5638-44ca-8d09-f2dccfd38704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 126
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "435384bf-3cd4-44db-96c1-a83966f2b750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 63,
                "y": 126
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e0d9d807-f496-4dfb-b698-b2c1c81a1185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 54,
                "y": 95
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7660e017-0dbc-4721-b999-522ccd6e2716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 201,
                "y": 64
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d10fd1f8-b56d-4ebf-8472-55c700f2689a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 126,
                "y": 126
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8ef69e51-ed1e-4244-bc16-3274aabe71e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 142,
                "y": 126
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "eb3c0ac4-3466-4d84-a6b9-67132562160d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 29,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 189,
                "y": 126
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f8c74cc9-db54-46d9-a203-821b2ed9993a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 117,
                "y": 126
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a0ae2298-8beb-4c32-8e1b-dc41fe9f06f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 95
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4fa54377-ab33-4234-8f54-1ce1ba8dfc02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 214,
                "y": 64
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d822330e-c033-47be-9d04-244d48299929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 95
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "cbfa964f-1ae1-47d4-870c-4483ed0abb01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 145,
                "y": 95
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a1b21eed-83f0-41b9-9d80-7944009a0968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 128,
                "y": 33
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f2e96a88-d57d-4f18-ad5d-bbb2f9057895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 95
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "52dc94ad-095b-4821-afd5-ffeba77a466d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 227,
                "y": 64
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "926d8063-6a00-4fd6-8fab-aca87f2693fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 240,
                "y": 64
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f5b4d238-d29c-4862-886d-3f8b76e34283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 95
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "076f335f-3cab-4208-bc65-e2bd0d1217ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 95
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f3d18b57-4866-4ddc-a5ba-177e4a321033",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 29,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 194,
                "y": 126
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a19525d1-8be8-438b-bd1d-94c2dbbe8fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 134,
                "y": 126
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bbe416f6-fb73-4594-945b-8d54fc21ecd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 132,
                "y": 95
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3d558c63-0f74-4af0-b894-219ac89f17c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 67,
                "y": 95
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "af511e88-2f23-4de0-8033-fb6a6e256c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 80,
                "y": 95
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f9d9caf4-15d2-4998-bc11-c56a083d4437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 126
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a337a9f6-38f2-4a2d-893b-aa27cf947408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 29,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6d2abe66-1ae5-4dfb-b6ce-2b1812655489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 29,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9ba11d77-5445-4fbd-a977-439215b1ccb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 29,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 217,
                "y": 33
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "adfd7fde-71ec-4b05-a542-2f8819c006c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 203,
                "y": 33
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "199a611b-45d2-41c6-923e-f0c9bdafb34a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 29,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 158,
                "y": 33
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9c82831b-667e-4e9c-b50f-ec4b7bb2284a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 29,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 106,
                "y": 95
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "311fe520-e755-481f-b051-0bd18e036ba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 29,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 119,
                "y": 95
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "64bc9a4f-2410-461b-b0b2-55a619f7a814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 19,
                "y": 33
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6acc8f7e-294b-4ac3-b26f-6dcfbbf870eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 29,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 98,
                "y": 33
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8c9b315f-6249-44b3-a650-75df1bf742bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 29,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 204,
                "y": 126
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6d7b051e-f3a6-43d9-a1d2-a5bbae806a64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 126
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c1215d66-f472-4544-8a22-d94f370b6a99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 51,
                "y": 33
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2ce67fe8-b24b-43d4-8134-ca2f198e2d67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 29,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 149,
                "y": 64
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9c65a4a6-96be-4018-aea0-6d624f947c9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 29,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d393ff27-3af0-40e8-8a53-6af3eba58032",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 29,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 35,
                "y": 33
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "427cdbe5-bfeb-46e0-a64f-872bee0d6aca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "09c0d1dc-4c7e-4ca8-b550-84a23181d85c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 29,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 71,
                "y": 64
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6c9f8d16-650e-4b88-a37d-2a3664fff2e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "752a0dc3-4257-4ffd-9ffc-400503c6c218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 29,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "53001c04-b7bc-4ebb-bbc6-22fabc60333b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 64
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d1a19b8a-f71d-400e-bc48-8c87c5fe0c1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 29,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9730aade-aed0-4e8a-b67d-73e8d635b322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 29,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 173,
                "y": 33
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ba061c19-bdbc-4854-9ae5-1d9ffe869efe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "68300d08-8db8-4f02-b6b0-5fb7e045a1d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 29,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4a637ee7-6538-4992-9b99-7af48bd9c880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "415e6bd5-39e6-46b5-96da-662b6955878f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 33
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "808972a6-9209-450b-aa3f-caf5571a114f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 83,
                "y": 33
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fae1a012-b8d9-47bd-a29d-cb1638bf693b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 29,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 171,
                "y": 126
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ee32729f-7985-4ffc-8803-2cb7dfca48dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 29,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 24,
                "y": 126
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6ef18d31-cb3c-4cdd-ac7f-a7d76783f5d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 164,
                "y": 126
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6ff31817-1b0d-4432-a305-29cb4609222d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 113,
                "y": 33
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "6a5cf880-cd87-450d-9eb8-f8b08092a249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 30,
                "y": 64
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9236cee4-4d69-474d-b375-1142f7bac5d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 72,
                "y": 126
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5f1d4af8-0d5c-4619-a769-79790351e553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 95
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7643f531-ee2b-481b-bd27-c00092c64831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 64
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7d513548-0583-4bcd-a753-f7ada8eef8dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 241,
                "y": 95
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "63785dec-d0c9-4c84-bbb7-1ddfe6864631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 175,
                "y": 64
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "11999e73-1bf4-4b0d-90f1-33dd3f16d94f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 136,
                "y": 64
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3df72b8f-f57e-4fe2-85c1-3a3508800c47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8013f10a-c873-4c08-a917-a128bf6d60e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 123,
                "y": 64
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7170aa3b-7e0c-4ce7-9fd0-2f6994358cdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 29,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 158,
                "y": 95
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1e17f8b9-dd8d-4fe1-ae4f-3ce0877c52fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 178,
                "y": 126
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b46427b9-e0e6-4053-9bb1-7c0b3c6a1504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 29,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 81,
                "y": 126
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5822713e-857c-4517-be14-8eb7ca61f8d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 29,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 110,
                "y": 64
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bd4a0551-7b92-42cd-916f-82b395ede676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 29,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 199,
                "y": 126
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "750abc53-3146-4754-8d5a-bdf4b6bbbbb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 29,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "4e2c0b32-d3e6-40cb-8adc-7d12f13e2eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 29,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 194,
                "y": 95
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "825845a0-6bae-4f32-978f-f74509f3bb19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 97,
                "y": 64
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f3eadf53-abcf-443f-95fc-40025cdba7c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 29,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 218,
                "y": 95
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ab7a3cdc-d41b-4cec-bb5b-0b09502e9e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 84,
                "y": 64
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "279dcd35-4354-4a31-92b3-539ab09dce5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 29,
                "offset": 2,
                "shift": 8,
                "w": 7,
                "x": 54,
                "y": 126
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e7edef7e-95ae-49f2-8d4b-f1954737bb11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 95
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1714146c-e77e-4988-91f2-e0cad0b49196",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 182,
                "y": 95
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0b6c339c-1720-4cc0-90c1-47c8f6535ac2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 58,
                "y": 64
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2995e101-ab56-499f-bae0-20a48d552142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 231,
                "y": 33
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "281a00e9-0e77-40b8-8e89-3030e7a781e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 29,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d337d998-d5c3-45e4-aff2-dd5765b6a048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 188,
                "y": 33
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fd453a53-7abb-4ce2-9c4d-99b48c9a98d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 143,
                "y": 33
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0031a380-75fb-436f-9629-512f2126c14d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 162,
                "y": 64
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d598b931-6ec1-4c62-a3ad-d26c2ac56747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 99,
                "y": 126
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "cc4dfc6a-0ccd-4632-be34-8d6ecd127632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 29,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 209,
                "y": 126
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "17761d6d-d090-4382-8d10-ad95baa5a36a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 108,
                "y": 126
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d65d387f-8e75-43f9-a094-7c5437459a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 188,
                "y": 64
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "22b1b427-00e7-4060-b9e0-8282a352424f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "2fa4b1ff-a846-4f61-9d74-ff45dec3d588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 67
        },
        {
            "id": "96428678-abe7-4b38-aa01-aaa86d7ce446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 71
        },
        {
            "id": "fcc4c0d0-501d-4ffa-ab7d-93abd7f6d121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 79
        },
        {
            "id": "d66dd5c3-7668-4d09-b244-9cd11fc82720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 81
        },
        {
            "id": "b2a76f37-c9f2-498a-a058-68f093d41583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 83
        },
        {
            "id": "4c3decac-3927-4987-ada8-e314ab4dfaa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 84
        },
        {
            "id": "6e10078f-a70c-44bb-8145-972033e900ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 85
        },
        {
            "id": "f2d13b49-1d5a-4012-a242-f34086d0b8e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 86
        },
        {
            "id": "571d012e-7fac-4a85-85da-21b8d08b1161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 87
        },
        {
            "id": "6989280c-746e-422e-8b3b-64a1d798cf28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 88
        },
        {
            "id": "744abba2-b592-4fba-a558-2478e9465fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 89
        },
        {
            "id": "6030c4d4-8e9f-4e28-8eb0-5ce7d7f67628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 90
        },
        {
            "id": "a1c3f18d-f69d-4c11-a2c8-a0390c08e676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 192
        },
        {
            "id": "5dc3474c-cfe4-4f93-9c24-54452e2ebdb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 193
        },
        {
            "id": "c72646b0-1cef-45ab-9de2-2094f81729b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 194
        },
        {
            "id": "b8b48639-aee1-44e9-a4ab-8a4c36b933ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 195
        },
        {
            "id": "0a2f444e-e8c1-43a9-a9d9-a2a71675eb60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 196
        },
        {
            "id": "66e39a0d-c4b5-40b8-b8f6-e2eecb75c76c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 197
        },
        {
            "id": "230fccea-f47f-4e72-8172-70ecd0e80f7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 198
        },
        {
            "id": "12c985e4-e400-4756-8428-1d555e7faf9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 199
        },
        {
            "id": "17202a5a-7f36-4d62-929e-cca3e4047993",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 210
        },
        {
            "id": "aa23987c-f53b-46cc-b99a-2f01f8e39704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 211
        },
        {
            "id": "0eaa077c-6438-4d5a-a4aa-a55d69649267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 212
        },
        {
            "id": "00562ec0-5b90-45d9-9658-d091e2c86682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 213
        },
        {
            "id": "09fba8b1-adfa-430c-96a3-3ca6d7c6c5de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 214
        },
        {
            "id": "d1ec2c3c-f95c-404a-9333-5744c07fe7d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 216
        },
        {
            "id": "70d14b14-84a7-43fd-bf08-76af58627fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 217
        },
        {
            "id": "84e4cc1f-84ae-4708-b878-99b9b749453c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 218
        },
        {
            "id": "dc5ea0e7-f36f-48d0-a300-c6f119f6adfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 219
        },
        {
            "id": "6309e33c-81d8-49fd-a8aa-e5a87f889742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 220
        },
        {
            "id": "f5e13c3b-1311-406d-ad24-7c7d7a3763ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 221
        },
        {
            "id": "c9820692-1037-4909-bd1b-5b1e9f070769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 256
        },
        {
            "id": "ac387239-9ba5-4577-a769-cf83429b9f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 258
        },
        {
            "id": "1265b773-0b09-4c52-af0c-2a790cef05b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 260
        },
        {
            "id": "ec58ac8a-cb3e-4a07-bc9f-ac7b3c8ca24e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 262
        },
        {
            "id": "46f95480-92ef-49b7-ab08-0ff5a9299347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 264
        },
        {
            "id": "3fe79c18-89b7-4f88-ae47-45a6984f870b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 266
        },
        {
            "id": "1d25dce4-a9e6-403b-bbf3-fe63780a9f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 268
        },
        {
            "id": "36605497-8e1f-455f-ad80-a9b093c6ec96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 284
        },
        {
            "id": "5b13de32-4d5d-48b5-a8ce-67c5d861d6dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 286
        },
        {
            "id": "112a3633-156d-48cd-9156-eea4630e63dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 288
        },
        {
            "id": "4a4c1084-2eac-4964-bf88-2161378e6c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 290
        },
        {
            "id": "b6a25a39-d3eb-42b8-87e4-a911a90e199d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 332
        },
        {
            "id": "8629b34f-e90f-464a-92c2-0a74accb1649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 334
        },
        {
            "id": "142f8192-abe8-48e0-a5ae-259b264abfca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 336
        },
        {
            "id": "2b728c18-7014-4d1a-ade7-70245ad6a5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 338
        },
        {
            "id": "2ab9af4e-e6d8-41f1-aadf-8b1fe43b51c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 346
        },
        {
            "id": "f4d38266-000c-444f-a68d-2d46ffdcccb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 348
        },
        {
            "id": "11dc89ea-2104-48a5-b6a7-6f9e3a4a300b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 350
        },
        {
            "id": "603ce5a7-7ccc-4b0f-994c-6dd443dbd315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 352
        },
        {
            "id": "8f1d8bcf-67db-4108-bd9a-7c3c6fed597e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 354
        },
        {
            "id": "b13062ea-a03c-4eb1-8036-c9845e1dd59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 356
        },
        {
            "id": "f1648c69-17ea-4c20-b039-666d5898890e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 358
        },
        {
            "id": "07558d32-f6db-4932-a477-53f7e2bb488d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 360
        },
        {
            "id": "8838ec90-9716-448f-8fdf-4681aa19381c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 362
        },
        {
            "id": "188d57a6-c748-4b5e-960f-8616f4dd7801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 364
        },
        {
            "id": "b8f22b49-a879-4030-adbc-87a754dc0d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 366
        },
        {
            "id": "309a8838-3c47-4c6e-bf66-8dab4c82d9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 368
        },
        {
            "id": "b4f93c07-2a01-422a-8bda-b785c4ec5fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 370
        },
        {
            "id": "facf8265-31fc-4b6e-ab10-c2dc997c7788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 372
        },
        {
            "id": "ed8fafd5-df0e-403b-be61-b99b3313c775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 374
        },
        {
            "id": "1d6eb96b-23c6-4b88-aeb5-eb2b4ebfb690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 376
        },
        {
            "id": "64597cb8-b8f5-4f89-8d04-13a3718611c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 377
        },
        {
            "id": "f89a5aa5-8641-43bc-aaec-01634c2d3262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 379
        },
        {
            "id": "29b28757-3297-4782-ad89-5ae16fdfde31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 381
        },
        {
            "id": "963d3de2-a2f0-4545-b132-9abbb9b1cdb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 416
        },
        {
            "id": "81577a36-93f4-455f-9593-5ada59bcd043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 431
        },
        {
            "id": "745b094c-1437-47cd-8771-ccf72a8086ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 506
        },
        {
            "id": "f75d5c44-fcd7-43cf-8422-21f4f6aacc7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 508
        },
        {
            "id": "99a00493-f9d4-4e26-9a31-bc093116da16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 510
        },
        {
            "id": "7af878e6-b353-41f3-be8c-ea791cd5df19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 536
        },
        {
            "id": "2d5d7493-273c-4bd4-bce5-195cbc5827db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 538
        },
        {
            "id": "788ccd15-dacc-4558-878a-36e8db1a430b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 904
        },
        {
            "id": "f2d48828-b3c6-4027-8be1-7621b55bc8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 905
        },
        {
            "id": "ee8b7365-e6ad-421c-acb9-2eabde20205e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 906
        },
        {
            "id": "d909cef0-d830-4189-90ae-d37272906830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 908
        },
        {
            "id": "444a6abc-9115-47d8-8e83-b3f28dc1ba21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 910
        },
        {
            "id": "b8019164-9245-47ab-a3fc-8ba89b108fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 911
        },
        {
            "id": "aac28ad9-5815-4293-9938-2d372abe3b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "2b5028e2-9435-4f4a-ac36-e50d81bd6313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "3bab6caf-cc49-4bc9-b6fc-e682f3648361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 918
        },
        {
            "id": "649870fd-ac23-483b-8cc9-a7a94c833167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 920
        },
        {
            "id": "6afd6e83-6592-4ab7-8584-d4d3ee136c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "9bc1b274-5279-4430-b65e-82042b8ad714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 927
        },
        {
            "id": "62cbe3a5-c657-4881-8f36-349ddc62f2e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 931
        },
        {
            "id": "5c79e3b4-ebf7-4c1a-8849-999a57615a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 932
        },
        {
            "id": "e823ca9e-214f-480f-884a-2225bcf19b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 933
        },
        {
            "id": "b91dc2b9-fd37-47a2-a303-13bcec5f0287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 934
        },
        {
            "id": "2f2dc764-7bba-4ea0-8131-05a71a6efe54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 935
        },
        {
            "id": "e253fa4a-5633-4b12-8afb-fccab053f6b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 936
        },
        {
            "id": "894f58de-816e-4326-9479-10c819824fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 937
        },
        {
            "id": "f0b2e61f-42fa-49e4-b3f4-38d4780ddff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 939
        },
        {
            "id": "c9cb2661-66ea-442e-a09d-8a342a337bde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 1026
        },
        {
            "id": "186a11cf-48c2-492f-9b5f-a1921c98ec30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1028
        },
        {
            "id": "2ad6d521-3681-42ad-a441-c7bd3e98ecb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1033
        },
        {
            "id": "f679f89f-09a4-46f0-94ad-d13bbc69e8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 1035
        },
        {
            "id": "81487a3e-3799-4147-a532-ec93d27a5fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1038
        },
        {
            "id": "3815a30d-d306-49aa-be6b-6f9dcbf396e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1040
        },
        {
            "id": "63d16fcb-c27a-490e-b558-4872c33430a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1044
        },
        {
            "id": "00746a58-d04c-4221-93e3-6621e7c6c7fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1046
        },
        {
            "id": "67909704-389d-412f-a9d1-e2e882473f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1051
        },
        {
            "id": "334dbd3a-442d-4aaa-9221-85098dd63044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1054
        },
        {
            "id": "4cab72e3-542b-4c87-a04a-c713b40924c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1057
        },
        {
            "id": "092d9f0d-d508-4f4c-b8f3-f158d76ed96e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 1058
        },
        {
            "id": "059e3085-2b86-4961-82db-62f3d849e22c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1059
        },
        {
            "id": "9ed557b6-fee5-4d78-8b52-9f6529163916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1060
        },
        {
            "id": "f073cf58-0661-4346-b1b4-6d1dfe2ea804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1061
        },
        {
            "id": "7d2daba0-4e70-46f9-995b-3c5c81c776c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 1063
        },
        {
            "id": "a0f31f05-6762-4b85-a018-78681f67e51f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1066
        },
        {
            "id": "71e66092-8990-44ac-8ff0-78f508cb84a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1071
        },
        {
            "id": "7b04f781-fd32-4d91-9f30-ce4d01c73c10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1122
        },
        {
            "id": "394ba7e9-36f5-4f3f-9382-ff21677f0978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 1140
        },
        {
            "id": "eb0b4403-7417-4f6a-a03e-0d18240e5bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7808
        },
        {
            "id": "22af26be-ce23-4e91-818e-11623c01e9e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7810
        },
        {
            "id": "1698cdf3-ae27-4634-8b3c-dcd6653344a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7812
        },
        {
            "id": "285b9c7a-a7f8-4983-a34c-0b104cce9ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7840
        },
        {
            "id": "32b1ae6b-c782-4f21-a67e-1be34bbc069d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7842
        },
        {
            "id": "2b89c599-e331-4db7-94ca-bf38aacde387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7844
        },
        {
            "id": "77198d62-cebd-44d0-987a-5e9fd42c74ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7846
        },
        {
            "id": "24db89e0-4870-4d30-a993-fc5e13ffa20a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7848
        },
        {
            "id": "25d1e11d-b841-4ce3-9d03-4e0b3c65497c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7850
        },
        {
            "id": "c41c3f83-69de-4bc7-ad8e-e4a0c7569530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7852
        },
        {
            "id": "22591448-7984-4fa9-bc7d-4b58c77e1d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7854
        },
        {
            "id": "2359cc4a-2933-42c6-91eb-a9d17ed2be61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7856
        },
        {
            "id": "79de0639-21af-48b0-9a4f-cddb906bbd12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7858
        },
        {
            "id": "45dd662c-c0cd-4959-8478-16d5caabfa58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7860
        },
        {
            "id": "5854d488-97bc-4da2-bceb-58ea3032a80f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7862
        },
        {
            "id": "d3bf1746-03df-4a74-b372-9205a8ab110e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7884
        },
        {
            "id": "11fce712-7917-435a-a11a-1530cce6ee1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7886
        },
        {
            "id": "1c39958d-2a3a-4a18-b74b-c722490a3dd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7888
        },
        {
            "id": "c17db7fa-c2c1-40b5-89a7-82bc2d07d2fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7890
        },
        {
            "id": "2922e4d8-6cdf-45b6-ae91-4441ea8bca37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7892
        },
        {
            "id": "1c7321d1-ea37-400e-a897-92ef6e431b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7894
        },
        {
            "id": "1b64cc3d-3f02-4614-bee8-dd29a22d95cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7896
        },
        {
            "id": "ba7f9469-d025-43f2-a33a-84cbd1ad31d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7898
        },
        {
            "id": "6b707f72-cbc2-45ea-a4c0-a983fd4a41ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7900
        },
        {
            "id": "e04355fc-b967-4dff-ac4f-3ca2aae62947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7902
        },
        {
            "id": "66cd5f45-13d8-4b34-b048-cbafd51dfcde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7904
        },
        {
            "id": "bdc01332-adce-42ea-ac7b-909f202922dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7906
        },
        {
            "id": "65d6ebd8-2182-4d55-982b-728bc55cd4cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7908
        },
        {
            "id": "53e88170-304b-449f-b5ae-c5ad804ce29a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7910
        },
        {
            "id": "7b649a5f-41f1-4c4a-aa54-133e729ccd08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7912
        },
        {
            "id": "ceb52277-cae9-49ff-8a2d-05aa44d24cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7914
        },
        {
            "id": "476c64b5-5881-46e2-be39-ac0feb3cecd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7916
        },
        {
            "id": "8695415d-0bdd-4fc6-9efa-86a4af11a123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7918
        },
        {
            "id": "189b34ee-be40-4eba-bde0-b3bfe82c8aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 7920
        },
        {
            "id": "450904ea-31ec-43b0-bffa-f93330910a36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 7922
        },
        {
            "id": "a42744f4-1fed-4065-bdde-304091057df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 7924
        },
        {
            "id": "e1dd0183-abbd-4822-a0dc-663379436bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 7926
        },
        {
            "id": "a4f22e5c-dbd5-4605-9be2-02bccff37f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 7928
        },
        {
            "id": "f4f1deb5-809f-4045-8910-76328e73b49b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 34
        },
        {
            "id": "dcd4e564-c27f-4f8d-9cca-3f04efb7538f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 39
        },
        {
            "id": "ca21add2-c5a0-4736-9d7a-f7264c0fc819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "3ab182c1-f4b4-4436-816b-eb15a86275e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "e98c2b0e-73ec-44e4-875e-845feb520779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "29768c81-28ae-42b1-9a4f-b6f7cc4aa611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "b929f22d-de81-4222-aebe-3eb10d68120e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "607a3373-f7a5-4d3e-8139-7cad69cd33a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 171
        },
        {
            "id": "a2d71b66-e26d-453b-b0f5-5b05e871abf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "7e8631d4-f1d3-4e8b-a4f3-94539bf66de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 253
        },
        {
            "id": "8ae44912-21d3-4f80-a57e-524b8108aab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "4b232783-451c-4099-b818-bcafece4d2ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 354
        },
        {
            "id": "8938dec6-f262-4528-af88-fa41df674b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "ef29e061-6e85-4737-a9e5-88fecfac20a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 358
        },
        {
            "id": "1204e399-c866-4cd6-8383-93590d275f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "7e50b406-b113-451f-b56a-8c4b7203d254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "c1f296d0-c7a0-446c-a3c3-9ab669f6511e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 375
        },
        {
            "id": "f06d0208-878d-4674-b2b6-92d4d72eab4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "d76a3740-3277-4ed1-b6b7-4c9f6b064179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "6d6591c0-ddbe-4348-90a2-6198b770cacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7808
        },
        {
            "id": "8d610b72-1c22-40c8-afd3-9ac5d2e09dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7810
        },
        {
            "id": "97bcd4df-fab1-4fa3-bf69-1c8eefc61c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7812
        },
        {
            "id": "418517f2-5739-4dc1-889e-6405751678f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "267f0091-da2f-44b6-b7e9-0b36a052830f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7923
        },
        {
            "id": "54ffa9b3-26c5-4f31-91ce-738b8123de80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7924
        },
        {
            "id": "5a03eec7-5c70-4a09-bd6a-9c5c462de974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7925
        },
        {
            "id": "b9d895e7-2862-44fe-89da-7c97cd37a235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7926
        },
        {
            "id": "9989e7d0-4662-4bf5-b19b-8edd268433c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7927
        },
        {
            "id": "11b5d612-02c3-4af0-b835-3d5a0c7216b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7928
        },
        {
            "id": "f343a8b6-38ed-4b9e-ab1f-5ff11570eb97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7929
        },
        {
            "id": "340745a8-aac2-457f-bbb2-f259f9c96062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8216
        },
        {
            "id": "6651d0b4-2716-4564-a5e3-6e0f3da668d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "ab0118d5-e1c6-4d9c-be90-dfc1fc57cdbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8220
        },
        {
            "id": "6a21c068-81a4-4dc8-896f-8b3428ea5259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "482fb6d5-5a13-4286-a6cc-3974bc138757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8249
        },
        {
            "id": "4ddda9a2-1eb3-42ef-849b-679373dba744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8319
        },
        {
            "id": "3fc0b34d-6e6b-48f2-8623-304138c218f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8216
        },
        {
            "id": "b0f6c01e-b14e-4f7e-bcaa-29ac9a9eff87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8220
        },
        {
            "id": "5f2baa61-3271-49ec-9cdd-b75b8ae2468f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 93
        },
        {
            "id": "1eb330e0-2805-4f38-ac8d-3dc9f3efb712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "815260dd-4905-48c5-9e79-3cbbd3108e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 119
        },
        {
            "id": "112035dd-b179-4385-b57e-7dcbb1102a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 121
        },
        {
            "id": "2408ff5d-2f12-45aa-a31f-d1df8a89f211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 125
        },
        {
            "id": "2bdd2569-2060-4f9d-9a0b-e61eea4013ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 171
        },
        {
            "id": "f535a645-9ad2-4994-a048-cc9dcf089439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 253
        },
        {
            "id": "9c121ee9-45c0-4d20-b5fb-c7ee692786c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 255
        },
        {
            "id": "8413c929-75d9-4db5-b4b6-b076cbe3e957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 373
        },
        {
            "id": "2e071711-3d3e-49c0-bdd9-458e93b2dc71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 375
        },
        {
            "id": "65941923-5410-4d2d-ae19-f5a3fdd3ad53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7809
        },
        {
            "id": "d0fee1e4-1366-45f1-a390-a10a342612bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7811
        },
        {
            "id": "ccb827a9-bfcc-483f-b73e-322f7b30b6fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7813
        },
        {
            "id": "03e16457-ac42-49fa-86f3-749194cd3be2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7923
        },
        {
            "id": "2bb40534-0220-4dcd-9f17-8b6a75a926a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7925
        },
        {
            "id": "3f486efc-8d54-4fda-8c85-f126ff930540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7927
        },
        {
            "id": "fb221ecc-0169-4dac-9c71-039b1fd28575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7929
        },
        {
            "id": "d98a5e36-65e2-40c9-97c6-e9db4f8bbce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8249
        },
        {
            "id": "e487ffd2-8070-44e2-ae91-508342b81ec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 41
        },
        {
            "id": "aad7447c-7279-4041-a205-f4627afca107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "cfd808be-e2d3-4c32-99d6-483394e80a64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "1dd20de5-154b-4590-b0fe-a72a01e3efc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "c302bd41-1cee-4065-9fcb-c2d09c12211a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "15b66e06-70ba-4ffc-a581-1a63ffb3e3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "f0d13e04-b0fd-44f7-9e41-5f01613c4708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 93
        },
        {
            "id": "acc1ba14-8896-4155-8268-4e806be4329a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 198
        },
        {
            "id": "7bd0b7ff-513f-4177-b8c2-49f6f610ee84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 308
        },
        {
            "id": "51f644cf-4343-4469-a853-0531687fa3df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 354
        },
        {
            "id": "2a53d571-da80-45d4-b4ba-a4f98a7c6392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 356
        },
        {
            "id": "333cb4eb-2115-40a6-8615-b897112d5fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 508
        },
        {
            "id": "c8a137e7-50ab-43e4-ac32-63b867ae5064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 538
        },
        {
            "id": "226d35cc-dcbe-4198-aedd-f39f779f6b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8216
        },
        {
            "id": "cdab9511-3007-4cf5-ba28-a0771c63cfd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8220
        },
        {
            "id": "5184015d-9039-4191-9677-42661e8cf14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8230
        },
        {
            "id": "cd352569-b7f2-4c4f-8c53-08f532270002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "aa5baf95-7b52-4617-9d94-36e16bb0a658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "688e4a75-b300-4315-9085-6aef6504ea99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "22c5208a-9182-4ccc-bc62-6106675266a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "5a642a23-cfd8-4d78-9eaa-32d4c8c4490a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 171
        },
        {
            "id": "8e97d1c3-a19c-42fb-a101-1987a3172738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "e3bfac5d-3f6b-4446-ad41-64bf45c4b70b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "ff4ad686-5dab-4475-a5b2-d9988174a3fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "af2bc5e6-5cf8-43bb-860c-882c257d38f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "c33f7d3d-7902-4c32-8f39-87d179b33374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "41f2db72-91ce-4fb0-a0a9-d053386d535d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "7def829a-4bbe-472f-a798-196bdd42061b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "33cbb35f-aa59-44a3-82b0-0b5353e64d88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 238
        },
        {
            "id": "06d6b491-da0d-4a8c-a6ee-e465194bd82c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 239
        },
        {
            "id": "e3ddb3cf-73be-450e-8cde-ef8b0c8a7344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "f408d733-6477-4f11-b521-eeca0c3c0190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "60e286fa-6d83-4461-ad00-72ed1b2bc001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "c8559f23-a676-4a2e-b8f3-118122c51a75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 297
        },
        {
            "id": "7377f5e5-75fc-4148-b96e-f17dc7d53c9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 299
        },
        {
            "id": "4fc5a6c3-2c8f-404c-a308-16b15bdb4ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 301
        },
        {
            "id": "c6d51c65-59fc-44ea-8c3e-53a4810c1ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "7c1c9ac5-7a55-4e01-9cb1-c788ce5f722e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "0d4b581c-3426-4038-b7c9-15e22eb35a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 508
        },
        {
            "id": "649b4a53-f5a7-4c79-a2c7-bd5161d9bac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "c209f334-529b-4254-9b9f-0633d0561172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "00aaff8d-ff75-44d0-b51b-bc344cf82245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "b2dda1fb-9986-4eee-ad58-8190a49b736b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "b655b05b-10b2-4b6b-aa53-58d14647c568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "3cb29677-0b07-48b5-be85-df3f6838423e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "d113ef6a-607d-4aee-ae9a-2d8cdc834180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "7f9e59e7-ec9b-4e61-ac52-873ae5a03e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "74e96eb1-c35c-4f9d-ad43-e2d0612576d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "5721c6e4-910f-447c-a868-735751eae1ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "d993f659-9b94-4fce-bed1-a13a33bc6756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "3cb481fd-0761-480e-ba2f-6a02a70109ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "15be8d8c-1246-4e54-bdc2-fe79bd671441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "4db6cb6a-8e6c-42b7-92f7-f6b96c41678a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8249
        },
        {
            "id": "7b36a5b0-33fe-454b-82d3-6b6fe28fa806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "c11da5ce-2c1d-4cba-b684-ba47ecaf91e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "80f5f412-7b8f-40c9-b806-4b743ea9d17a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "f4038d2f-d309-41ed-b2f1-b11263d9db61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "7258ffeb-19b9-42b0-815a-15130ee09b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "1d7e6d20-9b7d-4c41-9cea-bdfef58f1b2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "df7dad48-af14-4838-bca9-3a8108a23f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "a1406942-9280-447f-9a09-8be761aa1a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "c456b7e9-c604-4077-984e-3f1f02a7cf16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 171
        },
        {
            "id": "3b34c91f-7427-4521-884f-7a2ddbf75597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 173
        },
        {
            "id": "9e1f6491-5e00-4242-b687-3b108368bf32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "8267ca90-c1e2-42f1-b4dd-a192a9f8c420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "af592e48-6cc5-4a78-a09e-71bfeb54553d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "58ee8a12-7843-480d-8430-a3e6f573f1dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "775a8a51-3d01-4553-ac1c-bcd0217ec3e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "b0f678b0-9e18-413c-9d77-0fe20a8657bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "715552f3-add6-4676-ad3a-ba040e44613d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "d60f1323-4eef-4799-9713-80c1751b22cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 253
        },
        {
            "id": "7c4c21fc-2d4c-4523-8203-5ef1d0653033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 255
        },
        {
            "id": "aaa48b1c-ae8b-4c23-8589-7895eb1325b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "2fa67331-a0f6-423e-a892-f391366b162c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "d8ef8c90-49b5-4315-ad34-c217d0cffc07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "7887ca8d-7c0c-408b-85f3-e674edcc32ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "212c46c5-97f9-4b81-9b54-459994e9453a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "dec655f4-1e69-4f00-8860-545fc5ab5850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "6fd0898c-5b94-428e-a1f0-a66e8881a0d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "ace79a3d-2ace-4347-8d1a-663505e92875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "6e840509-4187-4883-8941-81ec8b60f68f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "9d4cd907-bdca-41cf-882e-7fddfa2aba9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "c0fa3136-0065-43bf-8660-b7d425fb0bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "01c88a75-5d4e-45c9-9cea-19560871aa8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 373
        },
        {
            "id": "851cff85-bb16-459c-b91e-792a96542007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 375
        },
        {
            "id": "9af339aa-ade5-43f0-ad99-1793e6a584a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 416
        },
        {
            "id": "efe5b3a8-c52c-4b41-9340-a41b548ea519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "2861d40b-d513-4ab5-b58c-ab38d8780a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7809
        },
        {
            "id": "486fbf0a-754c-4e40-9847-53de73b8c053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7811
        },
        {
            "id": "187e3a4a-fc85-4d48-adf2-7818ca80a971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7813
        },
        {
            "id": "dec2831c-46f0-4fc7-95a8-63c32fa028ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7884
        },
        {
            "id": "385646e0-f98e-491c-b323-c0044bcb4b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7886
        },
        {
            "id": "36485378-ff5b-447d-87eb-4059c1eb97d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7888
        },
        {
            "id": "cbe09563-8377-48a6-9326-d6a06c659319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7890
        },
        {
            "id": "02483554-538c-4ddb-9390-520c71c8f65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7892
        },
        {
            "id": "7cfd8a33-d7af-469c-822f-bf5ca8e9e401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7894
        },
        {
            "id": "3fe518ce-03e8-4fd0-b054-736afb3cc8d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7896
        },
        {
            "id": "00b9bb3d-70d6-4798-aa8c-2343fa379c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7898
        },
        {
            "id": "f3061da1-176a-43ab-89c2-f80366cdc6c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7900
        },
        {
            "id": "3903c522-9704-4831-a2f2-280ca287c499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7902
        },
        {
            "id": "b7990fd1-ddc2-4f7e-88a8-7d18b62c198d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7904
        },
        {
            "id": "623b9391-2562-4d26-935d-e5f0df4a31da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7906
        },
        {
            "id": "0cb4d1bb-1ff5-4497-bca0-93191ce6d09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7923
        },
        {
            "id": "8ef9b8e9-949a-4322-ac52-8f2315fd2622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7925
        },
        {
            "id": "53083c19-4338-4f2f-a89b-bb0645a48672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7927
        },
        {
            "id": "b1a60f87-2ed9-43f0-a1ea-88c5831bd12e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7929
        },
        {
            "id": "e625e4d5-a459-47b9-9d7d-4c7c14448a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8208
        },
        {
            "id": "a5d89d0a-d109-4928-bd6f-2a7b8ec8793f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8211
        },
        {
            "id": "c37c0491-40b6-46ca-a848-86c8fad624ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8212
        },
        {
            "id": "41203905-2eed-4626-8247-ff1b1acf7b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8213
        },
        {
            "id": "ceba6906-7e79-42fa-8958-2f2e2429c618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8217
        },
        {
            "id": "a95362d0-7f7a-4a42-bc9a-e83e95282c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8221
        },
        {
            "id": "9c5a0c1e-d175-48f3-a3a4-16eafc31d3c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8249
        },
        {
            "id": "ef9911d2-6594-44f7-8ce6-0271f573b3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 34
        },
        {
            "id": "497ce360-c945-4da9-acf1-ee327ec554d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 39
        },
        {
            "id": "591aed48-d17b-43d3-b099-ac2abe9094b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "a0f9b73c-97d2-4350-abc0-27b80f122d52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "407dd2f9-6bb5-4b8d-b325-da7ceac8a48e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "16b7283c-1acf-4789-97c6-ddecd6296758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "efe9d832-b7b9-4ce4-978b-53ae913a96b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "d2e03e4e-bd13-4386-a7e1-6c954f0f9098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "84e0a469-1dbf-40e9-8cf6-b7cc5a3c8af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "de5aaf12-5166-4d14-8b45-77f160774b0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "8163fa59-7158-45f7-8cfb-cdd5bc12ded5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "e3f40ad9-a008-48d6-b6cc-e4d9bcd3e8d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "174c7cbc-d6b2-4a02-b1a7-d4b24718f998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "2ac12817-8bf2-4005-b347-547c1fd5aa31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "21d136b8-36c5-4fff-a1c2-c4ddea102c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "a2d8dc3d-29e9-4333-ac75-b22f3baa2be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 171
        },
        {
            "id": "67dcfcd6-b9fc-446a-b477-9d15a57b0f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 173
        },
        {
            "id": "03b3e32c-40b6-43bd-8219-374073174f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "fb49cbac-27f0-4e68-ab0d-25c4582c9e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "fd9b2f75-c40d-421d-93db-83570a14468b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "8d481c09-fc5b-46ae-a923-1629a60c94e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "f504f6c1-2c84-4578-81bb-fa478917ac0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "02b62e03-0370-4c5b-97ff-e11ff9d71bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "32fc74df-4065-4ba9-bd86-15c909113e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "f4ed59c3-f8b8-422e-a171-e7f7162bb46a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 221
        },
        {
            "id": "98f46298-285c-4795-be37-0aee94cae716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "27a2961a-ccf1-4c74-9ebd-1a009a5ba8b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "e4d3e0d5-bacf-4c05-8239-5e8cd8935c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 262
        },
        {
            "id": "20e56b5e-5f8a-4ea8-8853-170d060a9bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 264
        },
        {
            "id": "e0dbe0be-52af-453c-ad95-780dbb08de1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 266
        },
        {
            "id": "c15a8288-5f01-4ae8-b16a-de664395e4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 268
        },
        {
            "id": "60b0501c-b1e0-4c31-987c-e240c8e3721e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "fc0889f3-0051-410b-8d61-eabe595ea63d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "02cbd9f2-6e9e-4a00-a691-1c9ea6c879b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "547fd651-20c4-4a3a-ab19-eb315673de1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "a8b51252-5ed6-4899-881a-00bb9c332d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "29c53aeb-cb8e-450e-a2ed-20fbb4619765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "f5d08b00-6650-4b00-af0c-6743e5cb8ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "2dd09d89-d7c2-41cd-9e1b-8eba8f0a2cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "25c2b39d-1104-46ef-a3d3-048326fe4212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 354
        },
        {
            "id": "b3d5397f-566f-4d5c-a4ce-afd676ab9d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 356
        },
        {
            "id": "7584b922-f5fe-4544-8348-fdfd6cc7e700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 358
        },
        {
            "id": "7c115e7f-1d75-4028-913a-7ee1572caeaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "4ffd28bf-1499-48d3-88c1-3edd297ecf63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "3f3f6d38-e5b6-4c47-bdf0-4aedb07424db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 374
        },
        {
            "id": "c0e294b7-ec8a-49db-b1e0-f6ac71785559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "1730b764-abbb-424e-845a-13636a02c363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 376
        },
        {
            "id": "46e25f82-00d6-4112-9a2e-559844b40bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 416
        },
        {
            "id": "58d7d1ff-0547-4b2c-badc-a010577410ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 510
        },
        {
            "id": "cef5f81e-445e-4f0f-8aed-b5d937905686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 538
        },
        {
            "id": "fbbe613f-6474-400c-995e-a47d90cccefa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "ca36e7b9-e8ff-433e-8596-33f30d271fe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "5264440d-b524-4962-ac7e-4ade3fda18a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "0d41dec1-7d07-448d-a5c5-d6520e9b4283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "aeed25ae-0cb5-41b3-aa47-9c272b9eefb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "8c68df7e-2f66-4f45-964d-dcbc7b520317",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "8d64f9fb-df5b-4540-8f1d-8c1fbbea40ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7884
        },
        {
            "id": "82684c3b-f98e-45c2-b18c-6d2183d78bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7886
        },
        {
            "id": "0fa64048-ef70-451e-a7f2-3e3b30891218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7888
        },
        {
            "id": "774d8592-9209-4d7b-b5e3-545489adf815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7890
        },
        {
            "id": "f12a6f8d-0d7a-42b5-b88f-f361f187db73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7892
        },
        {
            "id": "65a26d23-1aa3-4113-98ff-ce109dda33ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7894
        },
        {
            "id": "dc86ccc2-7270-4bab-9fa5-6539e189bb6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7896
        },
        {
            "id": "6a26718a-8dad-4bff-9ae3-928c88b853d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7898
        },
        {
            "id": "5c2d180f-8413-46d5-894d-443ff6ddf4bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7900
        },
        {
            "id": "c929c97f-901a-4780-af66-044f5e9026a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7902
        },
        {
            "id": "d1f69524-1771-4bea-9d19-919ce29cc0c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7904
        },
        {
            "id": "ac984e2c-47a7-4a5b-8fae-718f1324c627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7906
        },
        {
            "id": "00b24fd5-a5bf-47fd-944f-f5dfa7d1df7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7922
        },
        {
            "id": "32d31eb3-2430-4edc-a285-a6668cba2e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "2f03dff4-b035-4fec-8039-7329dcdfceb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7924
        },
        {
            "id": "1cbcef10-08e9-4bcd-b21a-19a709c8ab75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7925
        },
        {
            "id": "1907beda-8ca8-43ff-b220-2e39940292a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7926
        },
        {
            "id": "a207c2ac-815e-4e80-99ee-84a3e2a0d921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7927
        },
        {
            "id": "faf3daf5-9995-4378-a44c-dfac56ac8d4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7928
        },
        {
            "id": "a76eed50-21d7-43b9-a21e-8d06639e5448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7929
        },
        {
            "id": "c5dd0f07-938c-434e-ba4f-4fbfeae46467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8208
        },
        {
            "id": "28b498bc-5e7c-48f0-b8fb-a93be7d8d03a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8211
        },
        {
            "id": "cacd8225-c073-466e-a9ab-6e53035ea6b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8212
        },
        {
            "id": "b3154cbd-df06-4a63-89aa-42c0714d27e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8213
        },
        {
            "id": "7c7b0a5f-75ff-4951-bf9a-f2df8a6e2ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8216
        },
        {
            "id": "878a4c47-f495-4bb7-96cd-e64cb05c1d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "23777c5c-5baa-41cd-8f34-ca260b909111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8220
        },
        {
            "id": "606dc2bf-1821-4cfd-ae6d-be98bc60b2a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "8bca48f9-df58-4d6b-91bf-2d3ce122fd24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8249
        },
        {
            "id": "34af0dab-d978-4263-b977-38deb3ba0cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8319
        },
        {
            "id": "8224d620-2182-4a7b-b720-a3c581567e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 41
        },
        {
            "id": "037221e2-51ec-4613-9220-d77c23b679aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "db00e300-dbb6-4afa-9ef8-776e2855592b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "f53595e8-ebd1-4928-9fac-23e0360f8004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 74
        },
        {
            "id": "fa4921e8-a278-4698-aea9-6086dba83070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "93f341f0-a493-4cba-9d97-a60eba669ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "3b32aab2-9356-436d-be86-002cca56551b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 93
        },
        {
            "id": "9800d420-f182-46cf-9dc4-9603008a51e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 198
        },
        {
            "id": "cbcddbdc-f965-4158-a619-4e6797a3e1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 308
        },
        {
            "id": "023d424d-c500-4226-a56e-f5960f353a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 354
        },
        {
            "id": "c147a7b1-e069-41eb-882a-e7cb069aee6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "ac82a8c8-cdbd-487f-b4ba-5b1428ff9b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 508
        },
        {
            "id": "a578a5ca-c050-4201-9588-549f3f2d2d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 538
        },
        {
            "id": "8e5d31bc-d4b3-494c-a2db-0dbc9178a000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8216
        },
        {
            "id": "b0e4148e-d981-4ede-b976-0eb9e99072ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8220
        },
        {
            "id": "288aeff2-971a-4324-b73c-873ff59796b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "c5b5e070-d387-47f5-a3f6-382dbf868a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "2886a844-e047-42fd-88f8-f01025015920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 45
        },
        {
            "id": "41217a9a-365e-41fd-a9b3-d2f2832a5f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "0d0a718f-4bc2-4e01-be06-5826b760a79f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "198223e0-5063-4ff6-8596-22de4ab7bcb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 74
        },
        {
            "id": "13322658-5db5-400f-a2dd-77a379a06bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 90
        },
        {
            "id": "2429764a-eaae-4a2b-933e-d7d6e8587df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "1dae767f-9b2d-4e1d-80c5-831dd22fbaea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "72e1af89-5e14-49f8-8a0a-e8d1fe2ae24e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "7f2f5b04-039d-47c8-9c4a-e9c1a2d2438c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "a88bfeeb-9ed6-485d-b9e1-4425e08c6e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "28ec13eb-88e6-4ade-8c75-2bd82da3c44e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "252541f7-4b9f-48a9-8454-9df8a72d5495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 171
        },
        {
            "id": "07acca78-a4c5-4130-8f92-86c287b5fab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 173
        },
        {
            "id": "fd5fa78d-fbf3-466e-a97f-36f34d5e618c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "c234b17f-ae94-4158-85cc-fd26119b84d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "72f03f28-5e02-432e-b976-e7b7aac2c72d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "45b23a01-47d8-4b02-99e3-a6ae17c3dce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "ed01e65c-2a20-4810-aebf-40edd620d79a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "8d1c8012-4ff4-4a61-9a24-ed4f46313ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "f53fff2a-e53b-4872-8ad2-475dbb7cad3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 198
        },
        {
            "id": "8c4da3f5-ee9f-46f4-be71-9764a7860708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 231
        },
        {
            "id": "a2985a99-f117-4c89-8bf4-10e30fa96dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "1599d45a-a123-49ea-a9ea-6a98f4c8c471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "d14d85bd-ce3a-4d90-a59d-cf837c2656f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "d8c355f5-9f75-44c4-a508-a0474a1b7cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 235
        },
        {
            "id": "c0c7d946-d128-4f1f-9a30-11a4b7bcb9b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 240
        },
        {
            "id": "92ebe386-f77b-4f1e-bc05-589e40a51da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "8eb3ab47-5be5-4871-9fb3-cfa85250c239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "54c7bf44-4cc1-4a03-a6ab-4a5a672d5368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 244
        },
        {
            "id": "31dda070-3a9f-4113-b83f-a34e4eac81bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 245
        },
        {
            "id": "6178846f-79f4-4558-a45a-a369affe8b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 246
        },
        {
            "id": "61ac5c0c-df52-4311-998a-82f4f5837994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 248
        },
        {
            "id": "cb86f445-4dfe-455c-8292-4582d3044d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 256
        },
        {
            "id": "f18e9646-696e-4e0b-a587-b6fe025f4793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 258
        },
        {
            "id": "9c528513-ef1e-47f9-82d4-faec39697952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 260
        },
        {
            "id": "61918011-1122-4ebb-953c-7c1a362994fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 263
        },
        {
            "id": "e08e115c-1720-47c6-a039-00d76f8aa30e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 265
        },
        {
            "id": "0d8c8e7f-f423-4c52-9e5e-9ddc6f410938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 267
        },
        {
            "id": "4fd69485-7ccc-43be-9567-13c9c8aa8587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 269
        },
        {
            "id": "d08bfad0-7309-426d-95cc-f51c3de53ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 275
        },
        {
            "id": "558a7224-11ed-442c-96d2-59faad4a8d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 277
        },
        {
            "id": "baec76c2-7b06-446a-9d20-0cca0646d918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 279
        },
        {
            "id": "0a2da779-ed5a-470f-8c18-4e15fbc13c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 281
        },
        {
            "id": "693b33fb-7d47-4a65-9be4-c3547bb22b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 283
        },
        {
            "id": "cf42135a-e892-4fa3-a651-6e5249ce0be2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 285
        },
        {
            "id": "f7db5da3-00f2-4f4f-a12e-db4a8155dc94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 287
        },
        {
            "id": "4a2fbc46-cd03-4d5d-8545-6f702f2125e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 289
        },
        {
            "id": "290eb593-d119-49d6-83f8-38bfa599811f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 291
        },
        {
            "id": "83f4fcad-6d8a-43dd-8599-fb845d1b572f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 308
        },
        {
            "id": "5ca58079-413c-4b69-84e4-4d8c2d97d0d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 333
        },
        {
            "id": "cc29e826-5b87-4e03-a07e-f63ba83dafad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 335
        },
        {
            "id": "7a79a1e1-fe09-405f-b349-381f6b0849c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 337
        },
        {
            "id": "d891e9db-e665-4057-bb48-f8f478735632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 339
        },
        {
            "id": "f678976b-8351-4747-80f8-253a32b69657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 377
        },
        {
            "id": "e9e296db-1c8f-4477-8928-4e5ca881af12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 379
        },
        {
            "id": "b5f3e9b7-ec48-41f5-9724-b6a0a878825c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 381
        },
        {
            "id": "f12c1e16-251d-4340-9ca1-547dd7c64857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 417
        },
        {
            "id": "f62f6179-96f8-4c33-aed4-6432b0a715da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 506
        },
        {
            "id": "b1457dd8-95d8-486c-8999-bf06e6588a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 508
        },
        {
            "id": "f9927f89-ddd8-49aa-8924-509672ee2246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 511
        },
        {
            "id": "8c720753-0299-4fff-b55e-fdcd44439b6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7840
        },
        {
            "id": "a48d5ea7-b57c-4a18-ad82-6b4d2b25951f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7842
        },
        {
            "id": "f6ab2a57-a947-4d84-9f23-b8bcc9dbaa5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7844
        },
        {
            "id": "7b0c9c3c-e412-434d-99c3-a8e48cfd1429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7846
        },
        {
            "id": "b4f3d85e-3ff1-4be2-b07a-ee400be572a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7848
        },
        {
            "id": "b1fcccae-7301-4fd8-8f19-24ece4ebb84c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7850
        },
        {
            "id": "66bcb66a-c7d3-4563-b86e-4da1d12d4be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7852
        },
        {
            "id": "4d198c93-e431-4ea3-9459-8ba88ddc0104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7854
        },
        {
            "id": "f8bd8122-104e-469e-8654-06a5aa7593a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7856
        },
        {
            "id": "20a5e66d-940e-406a-ba04-9cd033536a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7858
        },
        {
            "id": "2a0ebccf-df3c-49be-8b5b-c88d55d043c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7860
        },
        {
            "id": "a501feb5-2956-478e-877e-3b6ecbf91440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7862
        },
        {
            "id": "ca36dd37-3f5d-4dbd-83a8-3280e01b309d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7865
        },
        {
            "id": "152a0bb2-1242-4cdb-974a-ea172943fc50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7867
        },
        {
            "id": "96430e38-2748-4767-8c0b-fd67fe027138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7869
        },
        {
            "id": "172d3d1a-857b-451e-ad05-aa60f6c83e97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7871
        },
        {
            "id": "cdbab20f-37c1-4c8a-b4b1-91ce000daa39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7873
        },
        {
            "id": "bcf899d4-bfaa-42a5-8c22-8ae13eaabc9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7875
        },
        {
            "id": "ef6a7144-3037-42d4-a7c3-608f2f55e73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7877
        },
        {
            "id": "08b3eb6d-edc9-4108-9635-b76fcd88445c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7879
        },
        {
            "id": "54af3565-1184-4519-ad4e-21bc2c206457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7885
        },
        {
            "id": "148021c2-7ed2-4957-a110-7c6b1c34b6bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7887
        },
        {
            "id": "8a58a70f-927e-4480-8b05-5b0145acfdb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7889
        },
        {
            "id": "7cdb9502-164f-4856-a747-adb3b65022fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7891
        },
        {
            "id": "6bdb6248-01bb-447d-858c-71cfe593f2dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7893
        },
        {
            "id": "48a80652-718f-462e-87e6-48d47b40c0a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7895
        },
        {
            "id": "7385deb5-47d1-4b63-a8f2-29b6547370c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7897
        },
        {
            "id": "ecd170dc-c702-4c56-ba86-1164a98e04e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7899
        },
        {
            "id": "352dfd87-f1d0-442e-8b4d-7232001fa65d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7901
        },
        {
            "id": "c478eb0a-cb51-48d1-b7a7-c9e43fcdd076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7903
        },
        {
            "id": "f6d24333-052d-46f9-b865-1c6514861852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7905
        },
        {
            "id": "ac8e8613-2e5c-496d-8c48-1fc7bc6b4bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7907
        },
        {
            "id": "09745367-9ab2-4a1f-8748-c0479794d1cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8208
        },
        {
            "id": "c9757367-a245-49ae-a62d-822a5b71028e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8211
        },
        {
            "id": "e700de00-63a2-4456-a145-8205237d8aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8212
        },
        {
            "id": "bc3215e4-133b-46f1-9c3b-36a31326850e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8213
        },
        {
            "id": "7247ec8d-b397-43c6-918e-dd4192b953ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 8230
        },
        {
            "id": "dd41a141-3c9d-4585-b7ef-afe42645d247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8249
        },
        {
            "id": "145c8805-62f4-44b8-b76a-09fd6db1f3f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 41
        },
        {
            "id": "20f18158-33c8-4a9f-a5d5-7625af448d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 93
        },
        {
            "id": "caa4dba9-3f2e-4eb4-bcba-ee600bfe8b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8216
        },
        {
            "id": "97b494b4-71b4-4205-91e9-586d7d9e5bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8220
        },
        {
            "id": "041102d0-754c-411a-8db5-a3eaa7fbe602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 45
        },
        {
            "id": "d27fbb87-0fbc-45d6-8e9a-d25dab2eefb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 171
        },
        {
            "id": "7360d4fa-caec-4b90-9673-bcaabf0faf46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 173
        },
        {
            "id": "a236b4a0-63fd-46b7-b333-a8b1248497eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 240
        },
        {
            "id": "c354a590-593f-4ffe-a971-ac5316c50468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8208
        },
        {
            "id": "90a3b71d-d598-418f-be50-ce8fe5a37106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8211
        },
        {
            "id": "f1213f0a-1358-4517-88a8-0a9196570e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8212
        },
        {
            "id": "82881459-8317-49f6-97e2-0f9a8726f213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8213
        },
        {
            "id": "95704d97-0aa1-435a-94f5-9c7c42c6f055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8249
        },
        {
            "id": "21c7f9c6-5cda-41fd-8a4c-b160a8ca9cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8217
        },
        {
            "id": "6c3fca1a-cc5c-4c7a-a90d-e613c72afaf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8221
        },
        {
            "id": "7d408376-0c5c-49b8-b1a5-232c7e63d0d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "ce2f19b9-fe00-4912-98ce-ec5a4d139b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "84795e0c-76ef-42bc-a328-604eeaf9e86d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "4b9e5cb1-8149-4794-bf01-9253f90b5b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "67c5816e-e07b-4a57-99ce-00810f691bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "3c945b14-c80c-4f16-9faa-135971a62609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "1b9cebe7-a64f-4b94-8900-f4bfc1eeaf34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "06207468-5e27-45e7-94f9-811625e0b471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "c957e443-d00c-43f9-af97-8a1f74d3d00d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "1dd20980-d42c-410d-8f99-fed419a863f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "98c668dd-d166-48c2-9c93-f4373c112417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "c4f04373-0c70-4eb0-8a32-6fbb28fab6b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "dd2acc5e-8190-45ed-90dc-186e75191472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "027bb85b-9629-4507-9879-dd551eacdce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "a53429a5-d9f4-4002-9b4a-493af73e1942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "6a63f665-de45-429f-acd0-eb19061f7fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "df075c81-6299-4339-80df-5fb2a638138a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "314e333d-8856-43be-8419-3e3811275d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "81ec72f4-607c-4ee8-89e5-afe3b131eb6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "ed67803a-4e26-4d6f-a859-95c8b3416db8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "89c2d22b-203d-458e-aeab-63e8999b2c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "35222ad8-503f-4543-99ab-02532364936d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "c99b1d46-a665-43e7-8f47-af4140478b2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "340d760b-23c1-4a76-9cd1-5cf3dcdbe24d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "18905ec4-30a9-4944-9a6d-2a7b9f9d1e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "7a2e54f3-2b0c-45d8-95bf-0f7bafbbb14c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "001c7725-9b46-404d-b711-ba618a2f8491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "e6d0900c-61a0-4050-8827-b3a02fc08e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "092d39db-8c87-4479-92d3-7fd90b2b7c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "cfe7904e-93c0-4240-88bb-09256c5c1449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 171
        },
        {
            "id": "f1e5e59c-a4bb-4c7f-8b32-690373745a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "113c6860-6db9-4527-a6c6-c3cada9780d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 187
        },
        {
            "id": "6cc8575d-4a13-43aa-aa3b-8a29cabc62a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "41540888-2d4f-40b8-ae96-5d8f82826bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "c6db8565-7827-4c3a-879b-27964c3d063a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "41a1e3ad-8c54-491d-b3e6-e76f2b6e12c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "7cee9c34-20ef-45cd-a477-8caa74719023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "b0eede97-3ab6-42d6-bd25-572c9e51cdd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "2ceababd-fc71-40b0-b1d9-70f54a813f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 198
        },
        {
            "id": "b1e811a4-0db3-47fa-9d0c-f7561589fa2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 199
        },
        {
            "id": "576bf7d4-4585-44d5-8e56-1159dbbee81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 210
        },
        {
            "id": "ca2adbf7-2cd5-4327-a8fa-3d04fa3d99ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 211
        },
        {
            "id": "968b7e0c-b394-4ed9-8e86-6b8efcb7432a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 212
        },
        {
            "id": "842ebd39-90e1-4c9a-afdc-5c38ce8cd6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 213
        },
        {
            "id": "ae66226a-bc86-43e4-b8b2-193bbd52153a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 214
        },
        {
            "id": "14da83eb-7e63-40bf-8fd0-68ca0d4da99b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 216
        },
        {
            "id": "0bae7ce5-da28-4380-ae0e-15bfd23c6196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 224
        },
        {
            "id": "9e71d835-697d-479d-b0e5-ec82fe6ee057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 225
        },
        {
            "id": "5e5816e2-5c9f-44bd-a620-726eeeccf688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 226
        },
        {
            "id": "0d20fd19-06c5-4187-90ff-7eda4b95e512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 227
        },
        {
            "id": "f3db4b6f-d40c-4554-b09a-c43babfbebdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 228
        },
        {
            "id": "8b403ed8-3752-456d-9fe5-643de5562229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 229
        },
        {
            "id": "c90981b1-beea-4da4-95fa-8d3749e67e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 230
        },
        {
            "id": "5be0df27-0d27-4201-82d3-7f6e99af01c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 231
        },
        {
            "id": "8ae9c2a7-2b29-4d05-aa5b-3956b73b164b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 232
        },
        {
            "id": "90b5f772-d075-46cc-8ad8-db45c67db4ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 233
        },
        {
            "id": "068f875b-519f-46b8-9f36-a528d81eaf8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 234
        },
        {
            "id": "3099aed0-d717-48c9-94f6-595e3306a92b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 235
        },
        {
            "id": "33af78cb-d01e-419f-8fa4-694563bd28d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 238
        },
        {
            "id": "4060ec80-4fbc-49e8-9b46-8ea02cff63a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 239
        },
        {
            "id": "f23451ba-47d3-4a89-b36e-a061950a7177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 240
        },
        {
            "id": "28a6042e-1054-4ffe-9a76-93966ab1dae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 241
        },
        {
            "id": "68642810-e984-4ce0-99ea-341b47e78a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 242
        },
        {
            "id": "51b5ea48-7e0a-446b-9bfc-dbadccb80645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 243
        },
        {
            "id": "43f08075-0467-4a90-bf91-ae0924289375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 244
        },
        {
            "id": "5539f092-9ab2-4fbc-959d-09a2fda6bee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 245
        },
        {
            "id": "c73519da-c6ac-412a-835c-3f2d07973d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 246
        },
        {
            "id": "7269a317-ec07-48db-a5de-04b6e8a39368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "636e201d-1a00-4dcc-9085-65514e287a16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 249
        },
        {
            "id": "3af9eb29-0e0a-47d6-83d8-eadcde1c2b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 250
        },
        {
            "id": "19c93078-7862-400f-a023-4ca45a68f12f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 251
        },
        {
            "id": "a888b1f9-3750-4f0e-aa1b-988d090aa0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "a8de409a-59ba-4b80-bb4f-30dc4052bb07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "3bc2968d-f853-4148-94e6-243f3d2f8901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "b55c39b9-622f-422d-8dde-ad9bf4712272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "b3067b57-89fd-479b-84c9-d8c960ca1cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 257
        },
        {
            "id": "27dd53c7-6791-4ee6-bbc6-9e3420901515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "12053e63-3ae7-4cff-ae67-44ab3136d6b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 259
        },
        {
            "id": "a93d2cd5-927b-40c2-b92f-0db0939dbf1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "96861b8b-38ef-4f50-ade5-13a8783ad877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 261
        },
        {
            "id": "606966e3-9d7f-45e0-8fe4-3f17db61246e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 262
        },
        {
            "id": "a494f5dc-5a5b-412d-b780-d58c5d5a0f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 263
        },
        {
            "id": "d504654e-d03c-499c-a550-de9b5bc78246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 264
        },
        {
            "id": "e82a885e-53ed-4a5d-b44e-03a2f51fbdce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 265
        },
        {
            "id": "b42732e1-3cdb-42cf-8edc-75088a4cdcfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 266
        },
        {
            "id": "6e60bd81-9185-450f-8aac-c62ced43e515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 267
        },
        {
            "id": "d81bcb35-ab25-4403-afb7-0217b47d59e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 268
        },
        {
            "id": "61636603-84aa-4956-a3d6-43cfb7c2e2d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 269
        },
        {
            "id": "aa4e2bae-cbaf-4d39-a024-275644980a36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 275
        },
        {
            "id": "c1a956fe-1edd-4260-b336-aadf31b11276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 277
        },
        {
            "id": "2eda5a70-8389-452f-86dd-5c9e8579cbf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 279
        },
        {
            "id": "c1ab1b48-87ee-4543-a6c1-e977965ff864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 281
        },
        {
            "id": "93c50611-4367-4ed2-9913-fbdcb2fbb1e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 283
        },
        {
            "id": "29784e96-88d6-4855-ac6e-ee79aa9e89f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 284
        },
        {
            "id": "b2b0fae1-82fc-4a45-92bd-fc8f4d5a06b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 285
        },
        {
            "id": "7edb6ad7-ed5b-4519-bd3d-f632a8a96b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 286
        },
        {
            "id": "7ae13d24-0524-4b97-bf40-859ded018b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 287
        },
        {
            "id": "63fbf02a-b831-4013-bf47-0ebd3fc17bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 288
        },
        {
            "id": "65381c7f-6769-41cb-8480-c0afcb35aec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 289
        },
        {
            "id": "0b7ffd7f-f0a4-4c1f-b22c-d552daf0ef74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 290
        },
        {
            "id": "fa1df310-58a0-4a9f-bf72-632506e772ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 291
        },
        {
            "id": "c4907503-0f7d-453d-adc3-c6a59666d569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 297
        },
        {
            "id": "3bdbae49-899b-472a-9f29-64cf4dd81499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 299
        },
        {
            "id": "cde87f6f-c2a0-45aa-856a-b348f5e2802a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 301
        },
        {
            "id": "d083bef0-5a79-4d46-9016-32e890484bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 308
        },
        {
            "id": "0c57b34b-677c-4f01-8e1f-156ef174dcac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 324
        },
        {
            "id": "51cdbf38-f4a5-46c3-8be0-1fc08f1bafcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 326
        },
        {
            "id": "9f526bf2-500c-4a9c-92a7-e6eaa6f06cd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 328
        },
        {
            "id": "72779020-9966-4aa5-a0bb-0993d6415352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 331
        },
        {
            "id": "ce1c95d4-76fc-490c-9b91-3e458e8a4214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 332
        },
        {
            "id": "855468a9-9d23-4232-953d-6d206888f947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 333
        },
        {
            "id": "0b495b55-742f-43a5-8ba2-0a325c6f7c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 334
        },
        {
            "id": "8327fff3-c09d-414b-92f8-7a91f0d52d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 335
        },
        {
            "id": "f906d088-c5ac-4fba-a37d-3577d78f0e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 336
        },
        {
            "id": "8625d8ac-fe78-41a0-bd68-3ae8f47ad613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 337
        },
        {
            "id": "b77bdc88-66da-41f7-b3d1-a88f06531d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "1ca063f8-c6df-4c7c-9aa3-a27892f195ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "9166d7d1-3312-478c-9a0f-6bef8317734a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "e513b9e5-d8e6-459b-a9ee-72b1bdf6a1b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 345
        },
        {
            "id": "329f1c55-b67f-4580-866f-a95e4f4bd68d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 347
        },
        {
            "id": "e03e2862-8c5e-46b3-bff1-fbaa8301733a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 349
        },
        {
            "id": "2e72e4f4-26fe-4e5a-b0bc-516f0565f823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 351
        },
        {
            "id": "52094b6b-6b3a-4096-9d53-58cf530ad6ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 353
        },
        {
            "id": "abe87061-46f7-4e50-a761-c0ba5b24a120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "8b04abda-8ccb-42bd-95c8-5c6c5e263798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "916b99cc-67da-4de0-8d4b-bfab2a1d4f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 365
        },
        {
            "id": "50bfcb1b-391b-4646-805d-456315495f48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 367
        },
        {
            "id": "042d0acb-45e4-4503-9138-a6ceafd99879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 369
        },
        {
            "id": "8f362f62-a499-4bae-a084-5637d025ff4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 371
        },
        {
            "id": "0fd45a65-44aa-465d-94ca-f367b41c6535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "a091224a-f05e-410c-ae5c-3d46290aebbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 375
        },
        {
            "id": "7249a63c-93da-4bea-9ef5-5044d3a3b5c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 378
        },
        {
            "id": "a82a21e7-e0ce-475f-b666-8d691e13e1cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 380
        },
        {
            "id": "d554a7a2-5703-4e2c-b3ff-71d0d7e55093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 382
        },
        {
            "id": "faea7b18-4284-4db3-a88e-75a8d0c08052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 416
        },
        {
            "id": "beecf25b-73b5-4c9b-8370-6bf06804832d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 417
        },
        {
            "id": "44f1d7a6-f778-4137-b34a-9925931c5c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 432
        },
        {
            "id": "ff554665-99f7-4269-9265-e8c9313629db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 506
        },
        {
            "id": "02b75ee0-5460-4fad-bfb2-d3d7e118ab0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 507
        },
        {
            "id": "3d90deef-7623-4167-ac20-0ccc69d300a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 508
        },
        {
            "id": "e0d0ba44-85fe-47a2-8ef9-bb2ddc9cc596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 509
        },
        {
            "id": "f2f75cd8-6ad4-4196-9781-5a6ebe57452c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 510
        },
        {
            "id": "505b8429-c746-4139-b18e-0147c0024789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 511
        },
        {
            "id": "d89ad7f6-a35d-4a36-b09e-9c60fbafd712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 537
        },
        {
            "id": "9968b1fc-6d2e-4c82-960f-5b232ed99ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "4184f18a-c01d-4593-902a-e7b1bc0a69b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "a23d3e84-dbd7-42ae-8fce-80235fc5aa7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7813
        },
        {
            "id": "979cd54a-2914-4311-a749-fea140a9eeb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7840
        },
        {
            "id": "30642b06-4749-4091-a556-57266083e9d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7841
        },
        {
            "id": "3d1e018f-6eb5-476f-aa68-91487b248082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7842
        },
        {
            "id": "3b94a75f-d13f-4a3f-9b07-18ff636645e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7843
        },
        {
            "id": "4c0bd791-f52f-4845-b92d-260fd9335e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7844
        },
        {
            "id": "09f9580e-2a4f-4ce9-9481-600a6d565c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7845
        },
        {
            "id": "cc87ca1c-08ae-4c43-879d-814f86f24e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7846
        },
        {
            "id": "a3d93115-ce44-4eb2-9b54-f7117c251d64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7847
        },
        {
            "id": "2de19e74-d4a2-441f-bf46-e28bd6ca850c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7848
        },
        {
            "id": "47086528-6a31-4b8f-8399-e48f169579e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7849
        },
        {
            "id": "45115241-9f17-45f3-8a0e-2f29e9f42d4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7850
        },
        {
            "id": "014e375f-2cad-4748-a270-e972d78ca810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7851
        },
        {
            "id": "e1652bcb-3312-4e0c-b377-73c5e657f3a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7852
        },
        {
            "id": "88d02f4b-beaa-4c35-b665-486c8cb3f330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7853
        },
        {
            "id": "09e59ade-3e1f-4900-9f24-cb37461d3231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7854
        },
        {
            "id": "c8333bdb-dd9d-49c6-99e9-350e9eaad7d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7855
        },
        {
            "id": "9361bc92-95c9-408a-b96a-06ba4d01914a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7856
        },
        {
            "id": "15b065e1-c109-48a8-bad3-ac2354278a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7857
        },
        {
            "id": "c0d6f0c7-5b9c-4adb-877b-ba66670dd7c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7858
        },
        {
            "id": "01d4e058-0d3d-4ba0-93db-37ff05891553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7859
        },
        {
            "id": "2e78dd44-ab50-47ca-94f1-0d2305ec60d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7860
        },
        {
            "id": "9ffb2bc5-e5ac-4baf-bdf4-3f1afb895323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7861
        },
        {
            "id": "dea2a455-ea37-4aed-add4-29bf72091324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7862
        },
        {
            "id": "db589260-fb6d-449a-8f81-50d8211c1c8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7863
        },
        {
            "id": "8cbe989b-f8bc-4d77-8780-a64097557d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7865
        },
        {
            "id": "f949ea5a-4551-4294-b027-d9eb610f6937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7867
        },
        {
            "id": "0056073d-d80b-49d5-b402-8a259426b3df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7869
        },
        {
            "id": "ac04e975-be30-47db-a375-458d44403d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7871
        },
        {
            "id": "23bf8867-44e9-458c-ac44-bd9e3a9a4c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7873
        },
        {
            "id": "fea8b84e-55a9-4f2f-a381-b5ef7e5e20e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7875
        },
        {
            "id": "b1b4e456-52cc-40ca-9967-169f18bf27b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7877
        },
        {
            "id": "4e93e182-0e7d-4c5f-855d-1a42568b1045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7879
        },
        {
            "id": "d868d455-afa3-480b-becf-062c8e840949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7884
        },
        {
            "id": "37ab76f6-9428-4a77-8fb4-ff8a8413d126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7885
        },
        {
            "id": "c4f67771-cfbd-4e3b-ba2e-d6de6d1c7e36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7886
        },
        {
            "id": "3ccecd12-2442-4f2b-b039-ef6985dc6523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7887
        },
        {
            "id": "f62b74e4-3b45-4b8d-aac9-3d46d88fecf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7888
        },
        {
            "id": "7bdc19cc-0e51-47e8-b996-76e0780606aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7889
        },
        {
            "id": "06034eb9-ac57-4394-9d95-8ccab2a50967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7890
        },
        {
            "id": "cde7c1ca-5936-4809-8952-33ce7c419cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7891
        },
        {
            "id": "875a91e3-c5c9-4826-8886-72376c4a7a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7892
        },
        {
            "id": "332e8e1a-ab84-4c0d-bf12-cf4c7b4767cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7893
        },
        {
            "id": "b13971de-9f97-4baa-8bda-8b4912b46380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7894
        },
        {
            "id": "c20deaa3-3044-40c6-9a69-2bbeb6f0e4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7895
        },
        {
            "id": "88f990bf-4eb7-4aa6-8883-1b10cc475c8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7896
        },
        {
            "id": "1f2388ba-c7bd-4132-ad4a-f802086dd179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7897
        },
        {
            "id": "2a05019b-adc1-45d9-afcf-557ca971b8d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7898
        },
        {
            "id": "b65a9f6f-d85b-4e28-80dd-05dd52aa7680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7899
        },
        {
            "id": "329eb51d-f031-4b4f-bb89-e72afad730f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7900
        },
        {
            "id": "baf635a8-c7c3-4c83-9998-4f74700ffec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7901
        },
        {
            "id": "3a2d3745-218d-48f4-9ed0-68326a350822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7902
        },
        {
            "id": "598266b5-4554-4c5b-8555-281c3dc08402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7903
        },
        {
            "id": "03a456cb-e152-4cbf-ae0f-a0a5099859b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7904
        },
        {
            "id": "acb50a9a-823c-4157-8ee1-a169cc400d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7905
        },
        {
            "id": "0da79068-f024-4c76-a4d7-5fe8803f3f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7906
        },
        {
            "id": "965c4cf4-1f86-452f-ace3-57f47cacf0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7907
        },
        {
            "id": "fe9a7560-83ae-49fa-8fc1-0946d1c95ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7909
        },
        {
            "id": "e1e9bbcf-bab4-4315-8740-0f50a757965b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7911
        },
        {
            "id": "347f772f-3b64-4ce6-b612-e8e5d47e444a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7913
        },
        {
            "id": "6e8e368e-0a27-47f4-8188-5c49b6e01824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7915
        },
        {
            "id": "49addb2f-256e-4355-8c60-ab4bc5759833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7917
        },
        {
            "id": "08ffac05-835b-48fb-8fb8-e7d31e69465a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7919
        },
        {
            "id": "758d07aa-f9f1-4138-8a32-e317644145ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7921
        },
        {
            "id": "7d8d2bc4-fb15-41ba-a231-9d08332cca72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7923
        },
        {
            "id": "8ac8ca6f-ecce-4dbd-9f71-42fab02ab49f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7925
        },
        {
            "id": "f46f7f55-84ac-4472-85a0-9fb2e1a2035e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7927
        },
        {
            "id": "7a8f4266-9a51-4217-8935-492f6c7b606d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7929
        },
        {
            "id": "b0ca2a50-79f0-45e3-b975-7454d8948e55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8208
        },
        {
            "id": "4bcbddbb-f2c8-49e3-bebf-39253358a510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8211
        },
        {
            "id": "913bd92e-a01d-4c9c-a9bc-f3e0ed7e41c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8212
        },
        {
            "id": "d864aa4e-c533-4a28-b5ba-fb31776c7867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8213
        },
        {
            "id": "df8b2abb-7620-4d7c-a968-bec43aad1b73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "54b60b79-be93-4ff8-b138-b06f3078aad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8249
        },
        {
            "id": "af4d7a6e-6fd4-4615-8bc1-5d9535c803de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8250
        },
        {
            "id": "fd2916e6-e51b-4ddb-ab0b-767a428033d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "f425a4d1-0e11-41e9-9ce8-c8d24b4323d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "68037af3-cee3-4fc9-a015-0e587c16a9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "c71b18e5-cc67-43e8-80eb-2232d6e31361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "36bf21ed-289d-41e6-ac01-74173d4879a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "2d0779d5-b9cf-46a2-a714-4163942d0199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "c4d825bc-914e-43c1-8283-ba9fa57ce22f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "c361ac8c-b27f-489e-b730-50e1c95270d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "a083bb0e-57f9-4189-91e2-b85996747148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "4ed27d37-71e1-4f1e-a674-fc7f891057ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "5815c212-da3f-41e7-ae12-b4dc0785578a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "a8fa123e-2310-4f6f-bc8d-3ec35e907e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "944bc194-904e-4fde-8106-09f29a79d62b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "8f36c36b-50bd-4856-9d15-7b127168bb98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "444b3cdb-224a-4fe2-9295-3d37e4328f85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "592b0d96-49ac-46ed-91e7-aaa1ced81776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "cfdf5393-ca44-4ebc-96b8-a79fd0cd8adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "2de440b4-08d5-42f6-a45e-738b9a74f5b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "6270a934-5229-4675-885f-3de585ca2ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "7de943d8-70a0-4361-8ca0-113046b5e7ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 171
        },
        {
            "id": "9ae7079e-e980-440c-881a-5787ad044d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "36da06c5-512e-43d1-b944-2529865244cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 187
        },
        {
            "id": "a916d900-4958-4b1e-94b4-868046a3cfac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 192
        },
        {
            "id": "4c67f6b6-0ec7-4e6b-ab30-340dce3b2723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 193
        },
        {
            "id": "38270492-6283-427d-9858-d656320a1da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 194
        },
        {
            "id": "c3ce014a-8fbd-41c6-9437-0b62bd6206ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 195
        },
        {
            "id": "ec9ffd71-f219-4d2c-af38-1eae05717e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "1a637ef1-82b9-470d-8f2f-a1affd88b475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "333910f8-3c15-451d-9cbb-464f4f896a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 198
        },
        {
            "id": "ae9abd23-c4b8-467f-baa1-b7adc8971025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "ff48865a-2dc4-45d0-8c62-9f91be123f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "f41bc361-894c-45ee-89f2-e876a3ff0ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "07c39c15-aa31-4a42-b03d-46add5b8f34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "3c1939d1-1adc-4104-a3f5-dbd87115cbcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "d1ecfb49-1b9c-4711-a18d-ed6a4e669f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 238
        },
        {
            "id": "7cb57739-fbaa-4960-b99e-7d67209224a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 239
        },
        {
            "id": "18ce5081-a95a-42f6-8a23-3673a5f74883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 240
        },
        {
            "id": "ecfc2a48-7514-4522-8eda-2aea37b38924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 241
        },
        {
            "id": "9a70a59b-aea0-4258-8522-a9dc9a73f9cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "8dd2b68d-bdfb-4a05-a63f-03508b51d52c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "50ee6abf-585a-4523-8d89-b1c41319a69a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "05c11e5f-58f0-4f80-8114-7ac5d9fd2bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "a1f32ad6-f315-418a-86c6-64126b423dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "796def78-43c1-4f50-b005-5344631c483e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "7a6ca705-a900-4cbd-b101-5b605f12f404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 256
        },
        {
            "id": "94fe3f44-b32e-43f8-9064-dfbb670c5274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 258
        },
        {
            "id": "01befe54-4673-4aff-bfd8-b6dfc8ea3062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 260
        },
        {
            "id": "c6cc27a7-c064-48c3-8e71-c1865f65e427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 263
        },
        {
            "id": "010f6689-7ece-4648-8602-1ac4a7f66299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "209e7d8e-e93a-4974-b404-e821993569ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 267
        },
        {
            "id": "7f74b2b4-be93-4847-b39f-d21010cf22fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "c46e21c0-a143-4536-a1d4-838e9798adab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "3ebec032-2ecb-4d89-8e26-0176dc78fe20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "ef2fbec2-3a67-4ddf-9903-5bc01eae3ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 279
        },
        {
            "id": "46f188f1-dd8c-439c-b191-da5176a49687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 281
        },
        {
            "id": "f3f1423d-99e9-498b-a452-121256b81134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "08b72d9d-d820-4096-8fc0-17b8386f3e94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 285
        },
        {
            "id": "2043e2f3-0be6-41c8-aa14-7058229ea662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 287
        },
        {
            "id": "1937873c-6060-4655-8f43-d58b8e24044a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 289
        },
        {
            "id": "bb0b1c93-f762-463e-a764-acac82abb860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 291
        },
        {
            "id": "256fe742-fc89-4939-a8be-c83d1c551080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 297
        },
        {
            "id": "6509e5a6-ce98-48ef-abd1-0567df23523f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 299
        },
        {
            "id": "0feccf8a-0d92-405f-841e-ef41f67e5a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 301
        },
        {
            "id": "66be6fdd-aa83-4ede-a22e-f23650d3e5a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 308
        },
        {
            "id": "59d6c4c1-2c5e-49e4-8664-694494dc2c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 324
        },
        {
            "id": "ee459ed6-31ae-4447-bea0-d3de32e07981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 326
        },
        {
            "id": "c6d8faad-3bb7-4f0e-b38f-4bd0e555d350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 328
        },
        {
            "id": "d8bfc18d-23ac-463e-b2a4-e6c97874dd3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 331
        },
        {
            "id": "4a8a580d-25e2-444b-ad49-2d0ef6fb91c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "02f6b15a-74ff-4913-8f58-0f5a37fda0ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "012019d4-57f4-40b3-a30a-44861c3f9504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 337
        },
        {
            "id": "14996bd5-2e5f-4d28-a1fb-d0103ea2eca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "d1d1cb4e-d821-4293-aa7f-7a6e0b6b16aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 341
        },
        {
            "id": "8f33d74c-0a3b-40a8-b0ea-8be0e7ff436b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 343
        },
        {
            "id": "e17306fa-66e1-400b-aa78-026e567ae46f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 345
        },
        {
            "id": "85c83796-80ec-4d24-99e6-1489a413627e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 347
        },
        {
            "id": "1c420ed8-0310-49d1-bcd2-b6f4d5b28728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 349
        },
        {
            "id": "2e3d9b8f-3c29-4edc-802a-e931b58f844e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 351
        },
        {
            "id": "289d6e1f-4d07-426b-852d-d9dfdce2eb02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 353
        },
        {
            "id": "9bb27ea3-199a-4bf3-a6f4-4cb38d1f05e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 417
        },
        {
            "id": "533b01e7-6237-45d3-9b6d-a5066cf57362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 506
        },
        {
            "id": "9b80881a-26d9-4689-b67d-865bbcfc11af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 508
        },
        {
            "id": "fa79af80-dac4-47f5-8d33-228a78aa087d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 511
        },
        {
            "id": "7def1aac-3028-4e1d-9b83-f27ba9e5b6b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 537
        },
        {
            "id": "4e922f51-ab7e-4985-804a-e7caf4589498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7840
        },
        {
            "id": "8094682d-f782-4728-b6fd-42aa9554b293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7842
        },
        {
            "id": "6d8774db-c51d-4c2a-ad87-dc37d1a9c82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7844
        },
        {
            "id": "04afdeff-a479-4805-a61d-efee5d1aba91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7846
        },
        {
            "id": "269a50ec-e2d7-4500-942a-02ce7389f6f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7848
        },
        {
            "id": "27f18358-491b-487a-9f49-a2ae9196d075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7850
        },
        {
            "id": "96eab635-ae76-43d1-98be-f16e1997329d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7852
        },
        {
            "id": "8c139198-cfec-44cb-b4cb-fb997e64f7bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7854
        },
        {
            "id": "6df2821f-cd67-4d28-a169-cb292514ddd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7856
        },
        {
            "id": "c2c1fee7-6346-47b9-a127-c1fabd46cb49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7858
        },
        {
            "id": "a90b8162-2da9-4652-a7d5-2130fdb9fe4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7860
        },
        {
            "id": "e794430d-6ca5-423c-ac4d-e21b39f0d897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7862
        },
        {
            "id": "0bca3539-7da2-41cf-b037-42bb09d4c3b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7865
        },
        {
            "id": "8cad0971-179b-4820-bb39-19e22cb86ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7867
        },
        {
            "id": "041abda8-a8ad-4bf5-b7b0-10b3580e1849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "7226b372-9425-4a34-9d72-b7245f465753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "da4978f2-0368-4cc5-a7f0-7aa9f3e47f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7873
        },
        {
            "id": "e78b67a6-e62f-4c82-8133-62df51b7ef28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "8c110d31-144e-4001-a4cc-4e332de13c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "19d896f8-0b9c-48a2-8c78-64982835ed35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "7c5fad8d-8487-4c33-bda8-c5c18997b7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7885
        },
        {
            "id": "49ec7935-f0fc-4a7d-b989-2b75da27ccf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7887
        },
        {
            "id": "a20ce847-28c4-4a1e-895c-ac4202b4cf93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7889
        },
        {
            "id": "895e1c24-e96d-4084-b8ba-6a7dbdcbb151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7891
        },
        {
            "id": "6dab867a-f2eb-4611-b0fd-21c949b6c92e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7893
        },
        {
            "id": "f5ede298-614a-4137-8d7a-c016b7bf3a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7895
        },
        {
            "id": "1744f57b-1428-4dcc-9fec-0cd1b0ac4bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7897
        },
        {
            "id": "6c8cf843-14e5-4d72-8893-16b609f6b825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7899
        },
        {
            "id": "094891ed-2ad0-40a6-a2f6-68fa34da4f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7901
        },
        {
            "id": "380a2c98-22f4-40e3-9a91-a3bc31d87877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7903
        },
        {
            "id": "e5ef1a61-439a-4548-9db8-d211bd22395f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7905
        },
        {
            "id": "080051b3-4077-4b5d-bf43-0c84318b3988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7907
        },
        {
            "id": "748fa221-dc59-44f2-aac8-438060837b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8208
        },
        {
            "id": "71b3714d-7a7a-4ed7-ad83-e331742a30e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8211
        },
        {
            "id": "c7a6b085-946d-4c00-bcd1-a9aca32d8e0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8212
        },
        {
            "id": "68bf1c92-1be7-4e71-8c24-6f7daccd8ebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8213
        },
        {
            "id": "d26971a4-7da3-4d1e-99de-47df843b95fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8230
        },
        {
            "id": "c81a4916-7b70-4b08-a524-c6111b0f1fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8249
        },
        {
            "id": "78312217-30e9-4832-bbe3-eb6653e05deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8250
        },
        {
            "id": "3af9b950-0066-470c-a66f-c89b85cdd0f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "41711942-1f6b-4da9-8667-f361b25e2ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "b5689616-9713-47a9-9196-46c3cf2b7bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "65765c9f-a36b-4f4b-a7f8-db69f4db0e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "0d5d6086-cdd5-49ec-ace4-9616b51b140f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "86ae1cd7-578d-4e92-bac8-431464c79d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "1a3ac8f0-9821-4579-b7c0-a87c74ad28ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "a0cd6d51-0450-45bd-839b-cfd56164635c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "d2333ba5-6db9-475d-8240-418d82db3a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "40e52452-8294-4e78-a86c-cfad0c1a6a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "3e3781f7-9769-4d5e-b576-fa4b7014aeae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 115
        },
        {
            "id": "1e1aedf3-6977-49df-b1e4-b6263d5d57e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 171
        },
        {
            "id": "5e9a592b-7c5f-49cb-b45e-4cefd4e50530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "e46b9383-afef-4d42-bf7b-5a13cff061ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "19bd4b66-b07e-4407-883c-887849b7c444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "f7fae4cc-7d8b-4f44-bf11-52a9946d543e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "1afbf87a-e4a2-4bac-ab65-2ebcbbaa990c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "6722a92e-2c2e-4907-8ed5-14013a701511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "4c99ccc5-f9ee-4654-ad6d-dd2d8b4f5936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 198
        },
        {
            "id": "8d3dcf55-9064-4e2b-95d1-9964aaa6fff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "3ac05226-e929-486f-a4b0-54ca15841a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "874cb225-9302-4c19-868a-5fc70c17481b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "bd5e1d6e-b95a-4da4-95fa-502fb49db607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "826f8eda-ac0b-442e-87cb-8c6af6307d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "837d075b-318b-49d5-ae4d-7a333549f673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 238
        },
        {
            "id": "95cab7b8-3d4c-4489-a873-38a00f2c88a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 239
        },
        {
            "id": "e32a52da-a758-4166-bb57-4d0967b98ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 240
        },
        {
            "id": "27184ba1-bb33-4e9c-a92e-24d44b84b18c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "09401e65-d9fa-4a6a-a71c-5f2ebb0437c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "bf63039d-792d-4fb3-b9ae-384db73eb312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "bc9f0c23-92ad-479a-8407-715e64f1f7ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "332c5e1f-cf1a-416c-947e-5dff0fc0f8a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "48f0fb7d-93c9-4d4f-9350-f0d4f8b6a215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "e1c460a3-1fcc-420f-ac7d-b1460bb3fadd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 256
        },
        {
            "id": "5f2e524d-f1a9-4cf0-b492-19e220bcf2b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 258
        },
        {
            "id": "a5718429-9f36-4e2a-9e62-0746c4da4225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 260
        },
        {
            "id": "c5eb3920-d321-40ff-92cd-c43723781ada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "0bd9116e-1b6d-490f-b6a9-c7fbab9b5a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "36603ea8-8310-43ba-9a45-1346032cfe9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 267
        },
        {
            "id": "4b3c395f-2740-4820-9dd3-3121de79fbe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "ca690646-9e0c-43ea-a7e4-485497a8818f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "ef872604-470c-4561-8355-6ca93b08bc3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "1370b2fa-200e-4027-ac8b-a2a431bc3a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "cbf5d037-cbef-4fa3-a7ec-60d3867d7a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "4a383c5f-9611-4c82-b52b-a8f961b313ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "7db24424-f832-41e8-b594-752e460d557c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 285
        },
        {
            "id": "4d3a1c4e-da40-42b1-983b-064ea5ffc107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 287
        },
        {
            "id": "171f4357-c59c-423d-865b-1d007ee66901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 289
        },
        {
            "id": "5aab6b8f-eb77-43c3-87c2-34ddf1f2863e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 291
        },
        {
            "id": "dea8cf77-765a-43c4-91f1-941bb2e62a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 297
        },
        {
            "id": "7c6b1949-fa4b-486b-b086-271369fc6bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 299
        },
        {
            "id": "57f31759-50a8-47ea-964f-1eddebfb2fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 301
        },
        {
            "id": "506d0d17-b891-4b6c-ba49-f6fd0dea8d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 308
        },
        {
            "id": "72c538bd-545d-4c52-b549-7e2697b103c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "1cfdf474-2c58-4b6d-9769-bd97556b742b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "dfa51912-7602-4293-b47d-3ebef6057a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "f4a8c9b6-2afe-47bd-a2a9-0bfb4a5dec56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "f6499cdc-db8d-4688-8b8b-62c4f74c3547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 347
        },
        {
            "id": "f61de089-7906-4708-999e-0a4b73cea386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 349
        },
        {
            "id": "54e58711-8456-4e81-966d-0285b8e36064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 351
        },
        {
            "id": "c19b779d-c59a-428f-9067-455dc5aa962f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 353
        },
        {
            "id": "fbe27139-b21b-4f4a-965e-bec7503efc20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 417
        },
        {
            "id": "5251ce98-ba5e-46fe-b2be-e9940f82da7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 506
        },
        {
            "id": "520ddf05-b0d5-400f-a3ee-0cbcbdf36449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 508
        },
        {
            "id": "6334ebb8-b739-44fa-bb99-9338d9e5756d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "af903f3a-797c-4f3e-afb5-516d2b7826ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 537
        },
        {
            "id": "259b4916-7733-4618-82b2-996b6ed96213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7840
        },
        {
            "id": "7f60506f-31c2-4fd2-8f51-fea3b0837450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7842
        },
        {
            "id": "56a198b2-439e-4b72-bcce-4838fa8f0a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7844
        },
        {
            "id": "a5a90d1a-88dd-45f4-8678-12b07c5fe4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7846
        },
        {
            "id": "da1009b1-fad0-4cac-9c1c-36095bcf2126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7848
        },
        {
            "id": "0745ec49-cb39-46d9-a4d8-0def9d474d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7850
        },
        {
            "id": "80894d53-1643-4dd5-ba15-afe0b6cf0d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7852
        },
        {
            "id": "0d42a03c-4a50-4113-9078-206067276116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7854
        },
        {
            "id": "373ccf2a-11b6-4803-9562-2354d624298b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7856
        },
        {
            "id": "6a4adf89-dbdc-4b99-a1b5-060504c75a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7858
        },
        {
            "id": "1833f42d-68c9-4e55-a414-233264dcbeda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7860
        },
        {
            "id": "ecbf6e3f-8e96-4cc4-8f1e-31b5623f0555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7862
        },
        {
            "id": "e7692863-b403-484e-86cb-58eb527b0fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7865
        },
        {
            "id": "a90824a7-6853-4a90-83ea-e4111c22d3f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7867
        },
        {
            "id": "e7b1bb2b-3ba3-4eb5-af5a-832334079a1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "5f1c05b5-221c-4723-97c2-56b81b8c9858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "37bb101e-99ee-4494-9d32-c771615647b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7873
        },
        {
            "id": "8d0a7076-6ab7-44f5-bad6-bb7c7d6f2b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "7545e98a-e967-482b-9e0d-e60c1d50dc9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "59a991b8-582f-43a7-9c1f-4b32f1da1403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "35394495-41aa-4fed-b1d0-006da1b941d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7885
        },
        {
            "id": "7367ef22-dc10-4cef-b6b4-875861f49356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7887
        },
        {
            "id": "a1b17abf-c5d8-4cd7-bea8-0536535e86a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7889
        },
        {
            "id": "7718f1f4-121e-4653-bef4-7a2d3735f633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7891
        },
        {
            "id": "e3017a3e-7a93-4b79-93ee-20c73568b7fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7893
        },
        {
            "id": "2b646ae0-4728-46da-b6ef-3b033f743160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7895
        },
        {
            "id": "729e7faf-addc-48fc-838b-c1b0d11087bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7897
        },
        {
            "id": "b75c171d-bfb9-4f0b-a732-0eb39234b9f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7899
        },
        {
            "id": "afe4c811-85d4-456f-9271-2a6613e08366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7901
        },
        {
            "id": "d166e55f-84ad-4da7-b589-355c5ded6619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7903
        },
        {
            "id": "bc5f78be-ec8b-452b-89af-d4879ad4e60e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7905
        },
        {
            "id": "007823a7-d61f-4a6b-af2b-3e2257237e88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7907
        },
        {
            "id": "c5ffa2f0-4c20-4999-87db-06c611ea80d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "781d5f12-86e6-4495-ae43-c35d7600889a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8249
        },
        {
            "id": "2ea1cfd4-7ade-4a5f-9414-3e13b6eafc11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 45
        },
        {
            "id": "da863998-e917-4a58-b910-c37eb8243152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "54f2b2b0-7102-45f6-a1b1-cdcf55752cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "e0b27bef-b217-4e3f-bf1c-f20a6b3d43dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "ef768c3d-7eba-4bfb-b5fd-d461ebd3bce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "5b0e1b3c-25a9-4838-b3c9-40a3af094f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 171
        },
        {
            "id": "db22e399-a2ce-4768-af7b-43752a485da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 173
        },
        {
            "id": "b0e2430e-7bc3-41b3-9bf0-5d3da547b9ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 199
        },
        {
            "id": "441ffef6-00a2-4f8e-8f89-aa4021765080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 210
        },
        {
            "id": "36af0443-8d89-46cb-a753-f3a69118718c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 211
        },
        {
            "id": "45c98d56-3625-4585-b39c-9d33b26058ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 212
        },
        {
            "id": "e5e8d68f-0cab-4ed4-86a2-04aeced2c611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "4a27aa08-68c8-4ea4-a7c0-1fd2c0b5c92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 216
        },
        {
            "id": "322e6e9d-c486-4d7f-bb70-a99460177aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 262
        },
        {
            "id": "e784bfaa-545b-4945-9810-99f9d92f800a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 264
        },
        {
            "id": "7d1dc60b-42f7-4d20-bc45-f96938ef594f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 266
        },
        {
            "id": "f77ba4a3-56d6-4e62-9b82-13934a702b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 268
        },
        {
            "id": "1c46c5f7-2957-4c96-89de-4953424d7af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 284
        },
        {
            "id": "033a1175-b94a-49d7-8697-209e9494e5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 286
        },
        {
            "id": "81551839-44f0-4546-9ebe-85c74212f605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 288
        },
        {
            "id": "d406cc7c-a604-4d26-82fe-5c16227092b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 290
        },
        {
            "id": "77b4a75f-9b7b-4ada-9303-eb86a939faf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 332
        },
        {
            "id": "94718b1f-b6be-4303-a838-45677d652aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 334
        },
        {
            "id": "9069f2f5-30c2-4659-93d3-d3d69395627c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 336
        },
        {
            "id": "dc1ec125-99a7-4ffb-9af7-912e0abb554c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 338
        },
        {
            "id": "7f7a8ae8-02ae-4cbe-bf8d-ab5d4e020094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 416
        },
        {
            "id": "46c4df1f-9f39-4db1-ade9-33191d25fb73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 510
        },
        {
            "id": "96c8ddc1-e569-4691-9216-a48ab88a1095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7884
        },
        {
            "id": "48c786a5-1b58-48ab-8027-ae3694db030c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7886
        },
        {
            "id": "f319916a-e6b5-461a-aa3b-afe5d41361c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7888
        },
        {
            "id": "a3d5e0bb-c385-4059-91c7-1e3213645095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7890
        },
        {
            "id": "ac6a06e4-a56a-4a44-9d0e-a8e9a9567402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7892
        },
        {
            "id": "e7a67eec-2a15-42f3-9146-2561e53ba59a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7894
        },
        {
            "id": "67fdcdb1-31f9-4f43-aea1-2b08b08e9ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7896
        },
        {
            "id": "ce1e9987-42cc-476f-b976-cb3cc3f5b530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7898
        },
        {
            "id": "51ae935c-0dae-496c-8494-b18aa8d5c4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7900
        },
        {
            "id": "b95d9a3d-dfec-4f64-a184-28ce05f5c73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7902
        },
        {
            "id": "73d4040e-a747-4812-a51d-6097fe593cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7904
        },
        {
            "id": "a6a086a1-8581-4b35-84cd-85e9d7fa3c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7906
        },
        {
            "id": "99f5ac26-221e-48b2-828b-0ac192c457da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8208
        },
        {
            "id": "6e74562a-931f-419e-a7a0-262d1fc36ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8211
        },
        {
            "id": "834cd62a-a090-4d1e-9b0b-8e164ca5a4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8212
        },
        {
            "id": "7504a5a4-d8c9-4c9b-948a-034137d17abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8213
        },
        {
            "id": "a051045a-63ff-412f-97d6-6d37e2d8d32b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8249
        },
        {
            "id": "ee2b7d36-5b0f-4d79-9899-fe2336404e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "87d0be76-2c30-4fa3-a860-09a77ad5e716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "6e0460cc-42f9-4448-8cb5-157299600d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "ea79fafe-8b38-49ba-92e8-766db5ca4c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "fb1a80fa-9f60-4ad7-8f8c-aae889f03e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "3691f248-500d-4285-bc93-8ae7f9a754cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "cff353bd-923b-4f28-b049-d12dec113e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "14295843-2548-4574-819a-938eac8e35e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "75a3de90-1d7f-4e75-90f9-092b65064773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "3c217308-7648-4def-9457-d4487624cb16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 100
        },
        {
            "id": "4b899599-8c53-4521-9a89-6de17a72e80b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "fc3482f7-d6b3-4077-b36f-29ba62faa395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 103
        },
        {
            "id": "b6d25856-e39f-4881-ad36-931f449c7153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "ad786cab-bfc5-411f-ac42-b3a39473cfd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "d3877545-2198-4066-8f88-67a50ff20f23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "98ecd410-8f2a-49a2-9424-50ed4392b144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "65ba2ef1-8ae1-4579-b587-24368fb486ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "77db751d-d10e-46c3-9597-fc59be9d33fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "bc3b7ab2-7581-4fdb-86d1-b310e77076a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "0fa66111-c111-4c14-8348-723a4e65351d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "85b656dd-9efe-4558-a969-f25df0c1a6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "0e00cc8a-3a74-4939-9925-2dfda9e87bde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 119
        },
        {
            "id": "4e3c7941-a90b-4ec4-8df6-1955ff048ae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 120
        },
        {
            "id": "65164321-43b1-4343-beff-62cecdda5207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 121
        },
        {
            "id": "5fa189cf-612e-4fa1-a3a1-871c13e795a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "42d64ffa-3cfa-41bb-b437-e8a3e2e4ba75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 171
        },
        {
            "id": "bd3ed92b-857c-48f2-a2e2-8f78379f2e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "fd2fa4fa-abf5-4bbe-9052-548dfe918d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 187
        },
        {
            "id": "f0805bc1-4682-4991-ba8d-4499bb58502b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 192
        },
        {
            "id": "578d365f-f4bc-459a-88dc-b3fb1ed8e83d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 193
        },
        {
            "id": "4c3212b1-60b8-4291-902d-1e8267ea554c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 194
        },
        {
            "id": "3e866481-5a83-48d7-a84c-7a5f220bc525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 195
        },
        {
            "id": "507bf3c3-6eb5-42af-8c28-28cdbeee61cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "3a78a0fd-5cf8-43f9-a4a5-31f6b02ea901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "361a3859-90a8-40fc-a7e0-62d21a55d986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 198
        },
        {
            "id": "9cb73c07-6939-4324-8c0d-eb8960c1db02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 224
        },
        {
            "id": "560055c5-9e46-4c47-9e55-0637de37e57d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "87b4eb2c-e1bf-4066-af48-5c8c06c9a10f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 226
        },
        {
            "id": "e395874b-9d04-4b5f-87b7-7e37d3b1d073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 227
        },
        {
            "id": "668488e6-8a68-4c47-8ad0-5561e6ffb081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 228
        },
        {
            "id": "f19b8e08-c6fc-42a9-bad9-bc5be098b7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 229
        },
        {
            "id": "e7673f09-42a7-4694-a5bd-3911fa52c0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 230
        },
        {
            "id": "c96543c4-0c7d-4ef8-a4cf-9bb5965b9670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 231
        },
        {
            "id": "27baec80-a82c-48ea-be5c-940d4584d2cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 232
        },
        {
            "id": "5c9c4b1c-ee58-4a63-b78f-3184bce0dc8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 233
        },
        {
            "id": "bdf43935-b6ce-460d-b640-1aa6e0d4478f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 234
        },
        {
            "id": "1cae1944-b1c5-4b76-b624-a93d1ecd78f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 235
        },
        {
            "id": "08bd24b1-5a40-4d5a-800a-454e34ec1dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 238
        },
        {
            "id": "5ca8bcd3-c065-491f-aa4b-9123e10ea74e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 239
        },
        {
            "id": "9f076d65-1af0-4c21-b12b-aae3cee1e7ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 240
        },
        {
            "id": "ecb2c424-c980-4de3-9b1f-96d86b8120c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 241
        },
        {
            "id": "c798b984-90a3-4699-b626-8361ebe7a818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 242
        },
        {
            "id": "3b71fbe6-13f7-412a-95ae-d0979dec4fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 243
        },
        {
            "id": "41e13226-6636-4640-948d-d73dc966f031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 244
        },
        {
            "id": "24ce4d69-e990-4d11-9428-7af6a59a3054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 245
        },
        {
            "id": "5698fdec-4be0-48af-8245-35bd5562b16d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 246
        },
        {
            "id": "809268d7-6f2b-4599-8c39-a7d5c9be3793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 248
        },
        {
            "id": "8212a943-788a-4ec6-abde-c337dfe97aac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "35973f91-a419-4917-a781-b40467381478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "a03e104d-82c3-437a-95f3-8218543c026d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 251
        },
        {
            "id": "097e1e8b-4cf8-41f7-8daf-8d44503fcc2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "9ccfeff5-9bfc-4270-ac93-918950521f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 253
        },
        {
            "id": "79761ef3-a844-4cb2-911b-8cac736bf593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 255
        },
        {
            "id": "f70430ff-6f69-4119-9ee2-d8a2df3e1ad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 256
        },
        {
            "id": "ec76b292-fad8-4643-8713-999377f571b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 257
        },
        {
            "id": "ae003ebf-3084-4036-8619-b5c7dd6155f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 258
        },
        {
            "id": "4be0069e-3eea-4dd6-9831-f8ed93133250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 259
        },
        {
            "id": "00619598-a5ea-4680-ac41-a39f81a4226d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 260
        },
        {
            "id": "03a757c1-c995-4abf-b5b6-224ada511bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 261
        },
        {
            "id": "b24e7f4e-c04f-48eb-952f-81c503ebd048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 263
        },
        {
            "id": "6b275d0f-b04f-473c-8bae-42963439decf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 265
        },
        {
            "id": "6fc23efa-2b24-46aa-aeee-2054f92b6e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 267
        },
        {
            "id": "7124d642-ea6f-4c23-a0a5-664983dd7dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 269
        },
        {
            "id": "b43252db-ef47-4762-b854-73d1a6df5865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 275
        },
        {
            "id": "c5503c1b-5e0d-4dcc-aac3-75cd25b4eff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 277
        },
        {
            "id": "52abc76e-db3e-4526-86f6-b3c7b94e87e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 279
        },
        {
            "id": "d3898510-1642-4cec-b763-09322e921ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 281
        },
        {
            "id": "af570c68-fa68-42f6-86bc-03959e392f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 283
        },
        {
            "id": "a6adb366-3c3d-4a5c-82eb-08f0d626e970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 285
        },
        {
            "id": "d04d0833-fe5a-4c58-a7fc-feac67b76aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 287
        },
        {
            "id": "3c01e414-4c3c-44dc-940c-a60f6b4da43b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 289
        },
        {
            "id": "a459e553-1bb3-4b35-9514-5d5bd6ce64f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 291
        },
        {
            "id": "0b09cb44-304e-4eb8-a614-394af1950ecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 297
        },
        {
            "id": "9d7a6ab5-4743-4a1c-9a4f-ad6a5c96e025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 299
        },
        {
            "id": "eccb6aff-1e74-4d40-a895-428a05d3a214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 301
        },
        {
            "id": "5bee1727-8bd7-495a-943a-c709cc475913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 308
        },
        {
            "id": "1e907643-07c0-4769-bfbf-2e9c7fca480a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 324
        },
        {
            "id": "20187b38-f904-4276-8130-e701841855b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 326
        },
        {
            "id": "88e0d845-a1cd-4db8-ba87-d8b3731a3e0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 328
        },
        {
            "id": "92693dbf-b102-4206-ba1a-ab547a1a255c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 331
        },
        {
            "id": "c9de93c8-5f86-465a-b981-e71a3041861e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 333
        },
        {
            "id": "4310ed2d-771f-4a80-8310-75f03d72b800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 335
        },
        {
            "id": "c342229a-3a42-4048-8ed6-a08e90d36a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 337
        },
        {
            "id": "451c4011-da81-4756-9ed1-d90a050a9539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 339
        },
        {
            "id": "ad705efb-878d-4a5b-822f-a3bd117fd4a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 341
        },
        {
            "id": "2752e488-7048-40ed-b84b-c1d7eec0c115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 343
        },
        {
            "id": "793491e0-ef7e-4302-826c-e032f4cf6ece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 345
        },
        {
            "id": "33e95ed9-2741-43d1-bf4c-86bdc38dbca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 347
        },
        {
            "id": "cde26a1b-2061-476e-a067-c26e71e4f0a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 349
        },
        {
            "id": "7e52e78e-d97c-4321-b0d2-7b2cd96bd531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 351
        },
        {
            "id": "5c00274d-4e39-4c0e-a3fe-344e2811610e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 353
        },
        {
            "id": "9faf835a-e73c-4c7a-a006-b10d9214be4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 361
        },
        {
            "id": "d68230d2-1aee-43ea-8099-ecacd8aafa74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 363
        },
        {
            "id": "7bd689a3-d299-4d25-b2cd-a682f09cc7ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 365
        },
        {
            "id": "50d69693-0262-4ca9-bb21-de4523d95be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 367
        },
        {
            "id": "71c5c868-c265-4998-9e63-615dc91c4b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 369
        },
        {
            "id": "65b6f486-d67a-4843-a6be-c55d5ea79b4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 371
        },
        {
            "id": "66a2885d-ede8-406c-a211-06ad0df329de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 373
        },
        {
            "id": "6b156453-50c6-4f56-a2c1-637a32f9465d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 375
        },
        {
            "id": "f4d9a74e-a48f-4c67-8054-700e06169ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 378
        },
        {
            "id": "0ce79276-90fb-4b8a-8bf1-ea8b1b3824cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 380
        },
        {
            "id": "c7fd84f8-8b05-4a2d-a239-2d66a00aa353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 382
        },
        {
            "id": "fb6b2281-4533-4243-9dde-2f960713adb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 417
        },
        {
            "id": "392214db-e45a-4e66-98d5-d3132b6fe23d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 432
        },
        {
            "id": "b0a5e5dc-bef6-45b2-9c50-51d0c2f8f391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 506
        },
        {
            "id": "9172dd77-ed3e-4fbf-8936-76c027a2e190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 507
        },
        {
            "id": "70e1766d-e3cc-4f1c-aed1-72130f2f8eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 508
        },
        {
            "id": "47c48cb6-b5c0-49e2-a252-5bd689615bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 509
        },
        {
            "id": "2c99b208-60db-4e38-a4d8-af4840a8c337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 511
        },
        {
            "id": "c8c51123-82ae-494b-9421-5c754fda6137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 537
        },
        {
            "id": "2e14d275-be97-45bb-aa36-ea46e35817f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7809
        },
        {
            "id": "5dad9168-8967-4a06-b683-cdf0fb5e9aab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7811
        },
        {
            "id": "3cbe397b-761c-45a0-a855-9dbce9ecbf2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7813
        },
        {
            "id": "cc8437b7-bc7e-40cf-becd-9a598e97677c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7840
        },
        {
            "id": "7cb81000-c640-411c-a27e-ea310a2795d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7841
        },
        {
            "id": "a01f494a-947a-40a6-b617-1668102f7a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7842
        },
        {
            "id": "b2fcd9b2-1bd4-4911-a89a-b155eaadcc8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7843
        },
        {
            "id": "8fd18848-9b93-48f0-87fc-6d55ad3ebe90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7844
        },
        {
            "id": "50304390-03d4-49d0-9f97-aa77fc1dbdf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7845
        },
        {
            "id": "276657de-e9aa-419d-a86f-1e04a647a1ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7846
        },
        {
            "id": "9d2eb3d2-d6ec-4a68-a793-2fa1f254f302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7847
        },
        {
            "id": "47330b7a-0002-41a8-8161-327b00584a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7848
        },
        {
            "id": "327b790e-7a0d-47f8-b03b-5e343945a5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7849
        },
        {
            "id": "eb1f4469-965d-4299-a0fa-1e0728581a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7850
        },
        {
            "id": "b2c9b03a-d0c9-4b40-9284-d51d424a31bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7851
        },
        {
            "id": "ad142a2d-57af-4e24-8bfd-80c502cb3953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7852
        },
        {
            "id": "3159b2c0-c61a-4a2a-ac3f-83d82097a67c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7853
        },
        {
            "id": "98a1d971-6b45-46c8-8c6d-cfeb91d8150a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7854
        },
        {
            "id": "6db641c9-59f2-4c8f-85a3-c192c2812f72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7855
        },
        {
            "id": "3528a0e6-d7f1-4a17-a44c-6a12566940df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7856
        },
        {
            "id": "cddf3ae4-738f-4494-9f2e-3c03123270ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7857
        },
        {
            "id": "ed35f9be-8e3a-4d41-a15c-2890322b56d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7858
        },
        {
            "id": "a8cd4be9-2bed-44ac-b443-0fa1cd659800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7859
        },
        {
            "id": "e3cccf13-2634-4b19-8b5c-5a9dedb103bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7860
        },
        {
            "id": "38e1cd40-8cac-4156-9ab5-bff6eaaa2f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7861
        },
        {
            "id": "0ab3aa8c-9c35-46aa-ba67-989e624cc9a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7862
        },
        {
            "id": "12969372-2456-487b-9282-84ce69e842b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7863
        },
        {
            "id": "82707b4d-a2d7-4bcb-abd7-5f782b3046b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7865
        },
        {
            "id": "04206ad8-a9eb-4504-bdf6-097d933b1165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7867
        },
        {
            "id": "1752fa67-8001-4ffb-a7a4-4d7a14dab446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7869
        },
        {
            "id": "6bdade7d-afb7-4dcf-bf8b-4cc766348f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7871
        },
        {
            "id": "fe3a0910-6172-425b-96b2-5c601c57164b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7873
        },
        {
            "id": "bf4fe016-5b58-426e-b148-9721db1f9264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7875
        },
        {
            "id": "93c7535c-efd3-4ced-a9c2-a832000c62e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7877
        },
        {
            "id": "10b71b4b-1905-4d10-ae0c-85f6ffe02850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7879
        },
        {
            "id": "75d94413-81a6-4fb8-83ac-9f0b60f363d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7885
        },
        {
            "id": "e64f6ba8-edc1-4ecf-ac41-6eb6380cc274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7887
        },
        {
            "id": "5e218205-716c-4dcc-a2dc-261877b0cdd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7889
        },
        {
            "id": "c3c64ac6-ce30-4e34-9a68-0429879cd43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7891
        },
        {
            "id": "604630a4-d954-4e28-9a0f-ff43dfb08744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7893
        },
        {
            "id": "707f99a6-1f22-4909-9b33-aedae1260012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7895
        },
        {
            "id": "f8de1f51-c160-4d92-aa11-30149f4dfdf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7897
        },
        {
            "id": "cf7edddc-8895-46c7-b2f9-a95efae164da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7899
        },
        {
            "id": "84f3bc73-c2e8-4511-bcf5-51514e9c46f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7901
        },
        {
            "id": "9f32d281-8f0a-4335-b254-9bcf56315dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7903
        },
        {
            "id": "575fac6c-ce6f-4031-85d7-118e1bb14f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7905
        },
        {
            "id": "964c39e2-ecb4-471b-860c-ce7683b72510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7907
        },
        {
            "id": "7f5fd34b-cab9-4f57-aea8-56d6020a1d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7909
        },
        {
            "id": "9384d121-5a80-415f-a623-83d4d581665d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7911
        },
        {
            "id": "b273c0a4-4844-48e4-8866-840f8c77af13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7913
        },
        {
            "id": "b4824257-9ada-4ec5-a19e-e7d74c814c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7915
        },
        {
            "id": "6b2d92f7-5372-494d-9046-19662ab5a199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7917
        },
        {
            "id": "c6235d2c-1844-4b84-89ae-8aba41544628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7919
        },
        {
            "id": "28f0fa5b-7edd-4823-938c-bf04a3e64e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7921
        },
        {
            "id": "ef8321c6-6696-4d06-9ef6-46e58e666474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7923
        },
        {
            "id": "7fa66ab5-786d-49ba-b1c0-41be8c244be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7925
        },
        {
            "id": "40a4cf22-e10b-4f9b-bc83-a07f00ad2d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7927
        },
        {
            "id": "27aa5792-168a-4d40-b892-e1dae90c1ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7929
        },
        {
            "id": "d05ee630-bc57-4bdf-a4f0-74bb6188d190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8208
        },
        {
            "id": "1bbcc45e-0a03-4906-a9aa-4d822dfeb4ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8211
        },
        {
            "id": "d39b8edc-4478-4b35-a407-6cbf69c03a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8212
        },
        {
            "id": "bf9cca50-51a5-4155-af53-dfa1cfe216b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8213
        },
        {
            "id": "004276eb-01cc-4680-9d69-603511052422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "8e14dfb5-643d-4ac9-bd21-06c0742a5b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8249
        },
        {
            "id": "0d5b3c94-4195-46ff-b67b-bcd18695185b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8250
        },
        {
            "id": "fbe4eb4d-1ea8-4a1f-821d-ab207844916f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "43885b6c-c91b-4ecc-9305-cb816256541f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 171
        },
        {
            "id": "a8930986-c62e-46da-8f89-e66fae5cc230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 173
        },
        {
            "id": "9eb7af94-d3fd-4f95-a083-a55c7860a990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8208
        },
        {
            "id": "2bc5c75d-7c85-4e2b-b850-070ca0bf9939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8211
        },
        {
            "id": "fb2fc53e-e72c-40b5-9ee8-db7e20d6e3d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8212
        },
        {
            "id": "eead1ed8-8bda-409e-9cc2-26104147e7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8213
        },
        {
            "id": "3f426f91-f916-47f6-9c3b-ea82c60ac003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8249
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
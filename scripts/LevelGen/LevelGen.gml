#macro CELL_WIDTH 32
#macro CELL_HEIGHT 32
#macro FLOOR -5
#macro VOID -7

#macro NORTH4B 1
#macro WEST4B 2
#macro EAST4B 4
#macro SOUTH4B 8

#macro NORTHWEST 1
#macro NORTH 2
#macro NORTHEAST 4
#macro WEST 8
#macro EAST 16
#macro SOUTHWEST 32
#macro SOUTH 64
#macro SOUTHEAST 128

randomize();

var _wall_map_id = layer_tilemap_get_id("WallTiles");
var _floor_map_id = layer_tilemap_get_id("FloorTiles");

width_ = room_width div CELL_WIDTH;
height_ = room_height div CELL_HEIGHT;
grid_ = ds_grid_create(width_, height_);
ds_grid_set_region(grid_, 0, 0, width_, height_, VOID);

// Create pathfinding grid
grid_path = mp_grid_create(0, 0, width_, height_, CELL_WIDTH, CELL_HEIGHT);

//Create controller
var _controller_x = width_ div 2;
var _controller_y = height_ div 2;
var _controller_direction = irandom(3);
var _steps = 400;
var _curr_step = 1;

//adjust this value to change layout style (0.0-1)
var _direction_change_odds = 1;

//===================================
//Player Creation Code
//===================================
var _player_start_x = _controller_x * CELL_WIDTH + CELL_WIDTH/2;
var _player_start_y = _controller_y * CELL_HEIGHT + CELL_HEIGHT/2;
instance_create_layer(_player_start_x, _player_start_y, "Player", oPlayer);
//===================================


//===================================
//Primary Generation Loop
//===================================
repeat (_steps) {
	grid_[# _controller_x, _controller_y] = FLOOR;
	
	//Randomize the direction
	if (irandom(_direction_change_odds) == _direction_change_odds) {
		_controller_direction = irandom(3);
	}
	
	//===================================
	//Chance Object Creation Code
	//===================================
	var _object_spawn_chance = 4; //percent chance
	if (_object_spawn_chance >= random(100)) {
		var _mob_choice = choose(oOgre,oSkeleton,oBat,oSlime);
		var _mob_start_x = _controller_x * CELL_WIDTH + CELL_WIDTH/2;
		var _mob_start_y = _controller_y * CELL_HEIGHT + CELL_HEIGHT/2;
		instance_create_layer(_mob_start_x, _mob_start_y, "Player", _mob_choice);
	}
	//===================================
	
	//Move controller
	var _x_direction = lengthdir_x(1, _controller_direction * 90);
	var _y_direction = lengthdir_y(1, _controller_direction * 90);
	_controller_x += _x_direction;
	_controller_y += _y_direction;
	
	// Make sure we dont leave grid
	if (_controller_x < 2 || _controller_x >= width_ - 2) {
		_controller_x += -_x_direction * 2;
	}
	if (_controller_y < 2 || _controller_y >= height_ - 2) {
		_controller_y += -_y_direction * 2;
	}
	
	_curr_step++;
	
	//===================================
	//Place object at last tile location
	//===================================
	if (_curr_step == 400) {
		var _floor_end_x = _controller_x * CELL_WIDTH + CELL_WIDTH/2;
		var _floor_end_y = _controller_y * CELL_HEIGHT + CELL_HEIGHT/2;
		instance_create_layer(_floor_end_x, _floor_end_y, "Instances", oFloorEnd);
	}
	//===================================
}

//===================================
//Remove single wall tiles
//===================================
for (var _y = 1; _y < height_ - 1; _y++) {
	for (var _x = 1; _x < width_ - 1; _x++) {
		if (grid_[# _x, _y] != FLOOR) {
			mp_grid_add_cell(grid_path, _x, _y);
			
			var _north_tile = grid_[# _x, _y-1] == VOID;
			var _west_tile = grid_[# _x-1, _y] == VOID;
			var _east_tile = grid_[# _x+1, _y] == VOID;
			var _south_tile = grid_[# _x, _y+1] == VOID;
			
			var _tile_index = NORTH*_north_tile + WEST*_west_tile + EAST*_east_tile + SOUTH*_south_tile+1;
			if (_tile_index == 1) {
				grid_[# _x, _y] = FLOOR;
			}
		}
	}
}

//===================================
//Bitmask tilemapping 
//47 tile tilesets (8 direction checking)
//===================================
for (var _y = 1; _y < height_ - 1; _y++) {
	for (var _x = 1; _x < width_ - 1; _x++) {
		if (grid_[# _x, _y] != FLOOR) {
			var _north_tile = grid_[# _x, _y-1] == VOID;
			var _west_tile = grid_[# _x-1, _y] == VOID;
			var _east_tile = grid_[# _x+1, _y] == VOID;
			var _south_tile = grid_[# _x, _y+1] == VOID;
			
			var _northwest_tile = grid_[# _x-1, _y-1] == VOID && (_north_tile && _west_tile);
			var _southwest_tile = grid_[# _x-1, _y+1] == VOID && (_south_tile && _west_tile);
			var _northeast_tile = grid_[# _x+1, _y-1] == VOID && (_north_tile && _east_tile);
			var _southeast_tile = grid_[# _x+1, _y+1] == VOID && (_south_tile && _east_tile);
			
			var _tile_index = NORTH*_north_tile + WEST*_west_tile + EAST*_east_tile + SOUTH*_south_tile + NORTHWEST*_northwest_tile + SOUTHWEST*_southwest_tile + NORTHEAST*_northeast_tile + SOUTHEAST*_southeast_tile;
			bitmask_list_ = ds_list_create();
			ds_list_add(bitmask_list_, 2, 8, 10, 11, 16, 18, 22, 24, 26, 27, 30, 31, 64, 66, 72, 74, 75, 80, 82, 86, 88, 90, 91, 94, 95, 104, 106, 107, 120, 122, 123, 126, 127, 208, 210, 214, 216, 218, 219, 222, 223, 248, 250, 251, 254, 255, 0);
			var _bitmask_index = ds_list_find_index(bitmask_list_, _tile_index)+1;
			tilemap_set(_wall_map_id,_bitmask_index,_x,_y);
		} else {
			var _north_tile = grid_[# _x, _y-1] == VOID;
			var _west_tile = grid_[# _x-1, _y] == VOID;
			var _east_tile = grid_[# _x+1, _y] == VOID;
			var _south_tile = grid_[# _x, _y+1] == VOID;
			
			var _tile_index = NORTH4B*_north_tile + WEST4B*_west_tile + EAST4B*_east_tile + SOUTH4B*_south_tile + 1;
			tilemap_set(_floor_map_id,_tile_index,_x,_y);
		}
	}
}
randomize();

#macro TILE_SIZE 16

var dir = 0;

repeat (1000) {
	if (Chance(80)) {
		dir = choose(0, 1, 2, 3) * 90;
	}
	
	instance_create_layer(x,y,"Instances", oFloor);
	
	if (Chance(2)) {
		instance_create_layer(x,y,"GreenCloaks", oOgre);	
	}
	
	if (!player_made) {
		instance_create_layer(x,y,"Player",oPlayer);
		player_made = true;
	}
	
	x += lengthdir_x(TILE_SIZE, dir);
	y += lengthdir_y(TILE_SIZE, dir);
	
	x = clamp(x, 0 + TILE_SIZE, room_width-TILE_SIZE*2);
	y = clamp(y, 0 + TILE_SIZE, room_height-TILE_SIZE*2);
	
}

xx = 0;
yy = 0;
#macro makeWall instance_create_layer(xx,yy,"Instances",oWall)
#macro right (instance_place(xx+1,yy,oFloor)) || (instance_place(xx+1,yy,oLevelGen))
#macro left (instance_place(xx-1,yy,oFloor)) || (instance_place(xx+1,yy,oLevelGen))
#macro up (instance_place(xx,yy-1,oFloor)) || (instance_place(xx+1,yy,oLevelGen))
#macro down (instance_place(xx,yy+1,oFloor)) || (instance_place(xx+1,yy,oLevelGen))



for (var i = 0; i < room_width div TILE_SIZE; i++) {
	for (var j = 0; j < room_height div TILE_SIZE; j++) {
		xx = i * TILE_SIZE;
		yy = j * TILE_SIZE;
		
		if (!instance_place(xx,yy,oFloor)) && (!instance_place(xx,yy,oWall)) {
			if (up) {
				makeWall;
			} else if (down) {
				makeWall;
			} else if (left) {
				makeWall;
			} else if (right) {
				makeWall;
			}
		}
	}
}
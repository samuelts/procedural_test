///@arg x coordinate
///@arg y coordinate

var xx = (argument0 div CELL_WIDTH)*CELL_WIDTH+CELL_WIDTH/2;
var yy = (argument1 div CELL_HEIGHT)*CELL_HEIGHT+CELL_HEIGHT/2;

if (mp_grid_path(oLevelGen.grid_path, path, x, y, xx, yy, false)) {
	path_start(path, maxspd, path_action_stop, false);
}


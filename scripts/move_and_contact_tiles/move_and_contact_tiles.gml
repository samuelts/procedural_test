///@param _tile_map_id
///@param _tile_size
///@param _velocity_array

var _tile_map_id = argument0;
var _tile_size = argument1;
var _velocity = argument2;

/// For the _velocity array
var _vector2x = 0;
var _vector2y = 1;

// Move horizontally
x += _velocity[_vector2x];

// Horizontal collision
if (_velocity[_vector2x] > 0) {
	var tile_right = tile_collide_at_points(_tile_map_id, [bbox_right-1, bbox_top], [bbox_right-1, bbox_bottom-1]);
	if (tile_right) {
		x = bbox_right & ~(_tile_size - 1);
		x -= bbox_right-x;
		_velocity[@ _vector2x] = 0;
	}
} else {
	var tile_left = tile_collide_at_points(_tile_map_id, [bbox_left, bbox_top], [bbox_left, bbox_bottom-1]);
	if (tile_left) {
		x = bbox_left & ~(_tile_size - 1);
		x += _tile_size + x - bbox_left;
		_velocity[@ _vector2x] = 0;
	}
}

// Move vertically
y += _velocity[_vector2y];

// Vertical collision
if (_velocity[_vector2y] > 0) {
	var tile_bottom = tile_collide_at_points(_tile_map_id, [bbox_left, bbox_bottom-1], [bbox_right-1, bbox_bottom-1]);
	if (tile_bottom) {
		y = bbox_bottom & ~(_tile_size - 1);
		y -= bbox_bottom-y;
		_velocity[@ _vector2y] = 0;
	}
} else {
	var tile_top = tile_collide_at_points(_tile_map_id, [bbox_left, bbox_top], [bbox_right-1, bbox_top]);
	if (tile_top) {
		y = bbox_top & ~(_tile_size - 1);
		y += _tile_size + y - bbox_top;
		_velocity[@ _vector2y] = 0;
	}
}

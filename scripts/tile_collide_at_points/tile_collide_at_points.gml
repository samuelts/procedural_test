///@param tile_map_id
///@param point_arrays...

var _tile_map_id = argument[0];

// Found variable
var _found = false;

// for the point arrays
var _vector2x = 0;
var _vector2y = 1;

// Loop through the points and check for a tile
for (var i = 1; i < argument_count; i++) {
	var _point = argument[i];
	_found = _found || tilemap_get_at_pixel(_tile_map_id, _point[_vector2x], _point[_vector2y]);
}

// Return found
return _found;

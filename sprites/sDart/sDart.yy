{
    "id": "f2be4f1d-d0b0-4f86-b9b1-8e51f94b16d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4377314-7c40-4e3d-982e-bc718a1549e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2be4f1d-d0b0-4f86-b9b1-8e51f94b16d1",
            "compositeImage": {
                "id": "4c2940c4-c0a3-4cb4-aa08-1f49f54e7e43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4377314-7c40-4e3d-982e-bc718a1549e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f5d2772-4a16-4459-af88-ae7e846c99b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4377314-7c40-4e3d-982e-bc718a1549e4",
                    "LayerId": "596355d7-742a-48ae-ab99-7045aba28f3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "596355d7-742a-48ae-ab99-7045aba28f3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2be4f1d-d0b0-4f86-b9b1-8e51f94b16d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
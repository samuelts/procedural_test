{
    "id": "35b9192c-9b2a-4813-9df2-b834235ae147",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeavyJumpCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 80,
    "bbox_right": 138,
    "bbox_top": 71,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f24b4b4f-965a-4a66-bc85-7c0b2348b09d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35b9192c-9b2a-4813-9df2-b834235ae147",
            "compositeImage": {
                "id": "85d9a72c-6bf2-4a15-89b3-0bfdbdae5989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f24b4b4f-965a-4a66-bc85-7c0b2348b09d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a768a76f-83b9-4bd3-aca3-1df871b32137",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f24b4b4f-965a-4a66-bc85-7c0b2348b09d",
                    "LayerId": "775f93f9-625c-40ff-985a-ac65bd367d71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "775f93f9-625c-40ff-985a-ac65bd367d71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35b9192c-9b2a-4813-9df2-b834235ae147",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 72,
    "yorig": 80
}
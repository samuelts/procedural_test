{
    "id": "7d6f1213-3091-452e-ac5b-50f1f7b067be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f6a29db-2521-4e60-899f-1795dfa4906e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d6f1213-3091-452e-ac5b-50f1f7b067be",
            "compositeImage": {
                "id": "740682e9-202a-46b0-84c1-5c55148dd998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f6a29db-2521-4e60-899f-1795dfa4906e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545c5b15-8065-4ef4-b669-9ddec447ec4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f6a29db-2521-4e60-899f-1795dfa4906e",
                    "LayerId": "c09694a6-9928-43dc-8c0c-09b66884f232"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "c09694a6-9928-43dc-8c0c-09b66884f232",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d6f1213-3091-452e-ac5b-50f1f7b067be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 96
}
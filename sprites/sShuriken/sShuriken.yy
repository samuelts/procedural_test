{
    "id": "e7df765e-63ff-41a0-ace6-48989971724b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShuriken",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "017c339d-4960-4fc6-8c2f-3872acba80eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7df765e-63ff-41a0-ace6-48989971724b",
            "compositeImage": {
                "id": "95c541c8-95c1-40b1-8f76-b24c38ba69d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "017c339d-4960-4fc6-8c2f-3872acba80eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f296df6c-0bdb-4bd8-807e-8f405837ea00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "017c339d-4960-4fc6-8c2f-3872acba80eb",
                    "LayerId": "dc9c3ce6-3edf-4803-a525-938eb4655b4e"
                }
            ]
        },
        {
            "id": "22d2edc1-e429-46b0-8ec9-088bdf1955cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7df765e-63ff-41a0-ace6-48989971724b",
            "compositeImage": {
                "id": "0e2c347f-6ced-47e7-bdbe-0aed9c0348c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d2edc1-e429-46b0-8ec9-088bdf1955cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10f5f956-2b4e-499b-8808-1a2803f68f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d2edc1-e429-46b0-8ec9-088bdf1955cc",
                    "LayerId": "dc9c3ce6-3edf-4803-a525-938eb4655b4e"
                }
            ]
        },
        {
            "id": "da757b10-4031-4c1d-a17e-26c11a45f274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7df765e-63ff-41a0-ace6-48989971724b",
            "compositeImage": {
                "id": "f095dffc-ae97-4244-992d-cadd46fac6a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da757b10-4031-4c1d-a17e-26c11a45f274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8511b8a5-517f-4ea7-8429-3804fbd50259",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da757b10-4031-4c1d-a17e-26c11a45f274",
                    "LayerId": "dc9c3ce6-3edf-4803-a525-938eb4655b4e"
                }
            ]
        },
        {
            "id": "3d1f6943-1557-4d91-bbc3-243cce1af738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7df765e-63ff-41a0-ace6-48989971724b",
            "compositeImage": {
                "id": "d7b81a11-1225-46b3-a1c7-f1a429b03320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d1f6943-1557-4d91-bbc3-243cce1af738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "570c4c61-20fa-4930-8913-2349308fcf6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d1f6943-1557-4d91-bbc3-243cce1af738",
                    "LayerId": "dc9c3ce6-3edf-4803-a525-938eb4655b4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dc9c3ce6-3edf-4803-a525-938eb4655b4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7df765e-63ff-41a0-ace6-48989971724b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
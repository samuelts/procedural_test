{
    "id": "8aceee6d-1327-49d4-8848-0355af030d53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSkeletonShuffle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6961bd3a-ed81-48de-a0c6-62c51b07ac04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8aceee6d-1327-49d4-8848-0355af030d53",
            "compositeImage": {
                "id": "6118a912-1efc-45c8-9c9e-e77d98c458a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6961bd3a-ed81-48de-a0c6-62c51b07ac04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5734a84-8378-4d1b-9a0a-e9f7f2946483",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6961bd3a-ed81-48de-a0c6-62c51b07ac04",
                    "LayerId": "ed62d2a1-88b0-4bc8-b06e-a2ee5ef2c214"
                }
            ]
        },
        {
            "id": "d2f49bf3-b1a8-4680-aea6-e6d2a1db19c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8aceee6d-1327-49d4-8848-0355af030d53",
            "compositeImage": {
                "id": "a9eef61d-8894-417f-b351-ab9fb5980cec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2f49bf3-b1a8-4680-aea6-e6d2a1db19c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f69abd97-fb46-4f63-93ac-8187be7c64e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2f49bf3-b1a8-4680-aea6-e6d2a1db19c8",
                    "LayerId": "ed62d2a1-88b0-4bc8-b06e-a2ee5ef2c214"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ed62d2a1-88b0-4bc8-b06e-a2ee5ef2c214",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8aceee6d-1327-49d4-8848-0355af030d53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
{
    "id": "d392d39b-724a-4b33-9dd0-eb83b6d78631",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1feb461d-0307-4ecc-a85e-11ecdeb34343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d392d39b-724a-4b33-9dd0-eb83b6d78631",
            "compositeImage": {
                "id": "cdb887ea-e613-4f75-b16a-3a4421301d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1feb461d-0307-4ecc-a85e-11ecdeb34343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0437c021-5e9a-46e5-ad5c-d3890432e761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1feb461d-0307-4ecc-a85e-11ecdeb34343",
                    "LayerId": "cf8d16a2-69d8-408e-b35b-9361d72c7877"
                }
            ]
        },
        {
            "id": "91d9867d-8aed-479e-8bb1-63bd3bdebd36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d392d39b-724a-4b33-9dd0-eb83b6d78631",
            "compositeImage": {
                "id": "0e527c3b-12bd-4086-96a7-13ea14b60c97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91d9867d-8aed-479e-8bb1-63bd3bdebd36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1d2313-d4fa-4acf-817e-0f2c18625488",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d9867d-8aed-479e-8bb1-63bd3bdebd36",
                    "LayerId": "cf8d16a2-69d8-408e-b35b-9361d72c7877"
                }
            ]
        },
        {
            "id": "64ad0210-f86e-4b95-9c3f-30d2fbac53fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d392d39b-724a-4b33-9dd0-eb83b6d78631",
            "compositeImage": {
                "id": "d05e6951-c13c-43e2-8856-b8175046255f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64ad0210-f86e-4b95-9c3f-30d2fbac53fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21464cf9-003d-46c4-a127-ca690ae8a0d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64ad0210-f86e-4b95-9c3f-30d2fbac53fb",
                    "LayerId": "cf8d16a2-69d8-408e-b35b-9361d72c7877"
                }
            ]
        },
        {
            "id": "fd266a5f-5052-4165-8b25-18ad954bc182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d392d39b-724a-4b33-9dd0-eb83b6d78631",
            "compositeImage": {
                "id": "e6f1238c-14ab-404a-9d96-4732be804c26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd266a5f-5052-4165-8b25-18ad954bc182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e0b196a-d011-443b-869f-406cf8582bad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd266a5f-5052-4165-8b25-18ad954bc182",
                    "LayerId": "cf8d16a2-69d8-408e-b35b-9361d72c7877"
                }
            ]
        },
        {
            "id": "0e7ab487-b5d8-4963-bfdb-8b97c47540fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d392d39b-724a-4b33-9dd0-eb83b6d78631",
            "compositeImage": {
                "id": "d4096568-eaef-46b8-9b56-36a02642b9a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e7ab487-b5d8-4963-bfdb-8b97c47540fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aae8fbe-b077-488a-8f53-e4563fcbfe3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e7ab487-b5d8-4963-bfdb-8b97c47540fc",
                    "LayerId": "cf8d16a2-69d8-408e-b35b-9361d72c7877"
                }
            ]
        },
        {
            "id": "0f473d9b-e6a7-438d-a6f3-d4f72943bb3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d392d39b-724a-4b33-9dd0-eb83b6d78631",
            "compositeImage": {
                "id": "93e8f4af-7e75-4595-b959-cf41e792de13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f473d9b-e6a7-438d-a6f3-d4f72943bb3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24bdcf5c-1dbe-4977-bb04-887ad61cf1b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f473d9b-e6a7-438d-a6f3-d4f72943bb3c",
                    "LayerId": "cf8d16a2-69d8-408e-b35b-9361d72c7877"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cf8d16a2-69d8-408e-b35b-9361d72c7877",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d392d39b-724a-4b33-9dd0-eb83b6d78631",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 8
}
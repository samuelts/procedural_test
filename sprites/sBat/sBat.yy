{
    "id": "a3af73d2-92d8-43c9-a547-fad40b40b227",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59cdf549-2027-4b41-b320-d946c060a393",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3af73d2-92d8-43c9-a547-fad40b40b227",
            "compositeImage": {
                "id": "bdf527da-8059-4242-bfc5-1c78b8a2369c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59cdf549-2027-4b41-b320-d946c060a393",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cc488dd-79fa-4d36-9b17-55f8878a5207",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59cdf549-2027-4b41-b320-d946c060a393",
                    "LayerId": "ddc874a2-04ea-4622-81bf-0e6447564397"
                }
            ]
        },
        {
            "id": "d2af5fc9-b803-482d-8d07-184be933286e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3af73d2-92d8-43c9-a547-fad40b40b227",
            "compositeImage": {
                "id": "8f7976fd-6e75-4c41-8f02-06b50614aa53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2af5fc9-b803-482d-8d07-184be933286e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c279a07-7838-432c-ba72-b4f17f772d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2af5fc9-b803-482d-8d07-184be933286e",
                    "LayerId": "ddc874a2-04ea-4622-81bf-0e6447564397"
                }
            ]
        },
        {
            "id": "3c2a636a-3b49-47e6-9a99-c13652e2bfce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3af73d2-92d8-43c9-a547-fad40b40b227",
            "compositeImage": {
                "id": "073bf651-8d63-40fd-b5fb-655c412cba19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c2a636a-3b49-47e6-9a99-c13652e2bfce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0167978-73f2-427d-83a2-4f847ee9e0a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c2a636a-3b49-47e6-9a99-c13652e2bfce",
                    "LayerId": "ddc874a2-04ea-4622-81bf-0e6447564397"
                }
            ]
        },
        {
            "id": "6d43bccf-f37a-4564-9598-c3260c7ce6b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3af73d2-92d8-43c9-a547-fad40b40b227",
            "compositeImage": {
                "id": "44907492-19c7-4beb-bf43-14b35059376d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d43bccf-f37a-4564-9598-c3260c7ce6b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "281431f2-bf67-4765-859c-951ba1a275ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d43bccf-f37a-4564-9598-c3260c7ce6b9",
                    "LayerId": "ddc874a2-04ea-4622-81bf-0e6447564397"
                }
            ]
        },
        {
            "id": "8bb8f18c-d398-4fee-9337-12ac20d40350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3af73d2-92d8-43c9-a547-fad40b40b227",
            "compositeImage": {
                "id": "8fa3b625-274e-492a-949b-e5bb93ab229d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb8f18c-d398-4fee-9337-12ac20d40350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2dfc632-73e5-4dde-bcfc-9782f66e4555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb8f18c-d398-4fee-9337-12ac20d40350",
                    "LayerId": "ddc874a2-04ea-4622-81bf-0e6447564397"
                }
            ]
        },
        {
            "id": "19c76d38-f95c-403f-870c-29ff97f52643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3af73d2-92d8-43c9-a547-fad40b40b227",
            "compositeImage": {
                "id": "20898cb0-7bd6-47c6-9c91-2e674c30fb9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19c76d38-f95c-403f-870c-29ff97f52643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f720503-2799-4293-ac5a-3cf338a4c4a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19c76d38-f95c-403f-870c-29ff97f52643",
                    "LayerId": "ddc874a2-04ea-4622-81bf-0e6447564397"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ddc874a2-04ea-4622-81bf-0e6447564397",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3af73d2-92d8-43c9-a547-fad40b40b227",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
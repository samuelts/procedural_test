{
    "id": "9817a441-b974-459f-9505-8737e0c1c1b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFrontSwing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 5,
    "bbox_right": 57,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87eae9ae-240c-4e9f-a49e-841ac9e00fad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9817a441-b974-459f-9505-8737e0c1c1b2",
            "compositeImage": {
                "id": "9260aec9-5cd0-4c99-8740-7781fecf5463",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87eae9ae-240c-4e9f-a49e-841ac9e00fad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3f4c2f7-5edf-418a-af79-3d8dade03800",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87eae9ae-240c-4e9f-a49e-841ac9e00fad",
                    "LayerId": "01165fb5-9418-4744-979e-e4a7928e3992"
                }
            ]
        },
        {
            "id": "7538588a-14e4-4c2a-bf6b-6e39fc444cf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9817a441-b974-459f-9505-8737e0c1c1b2",
            "compositeImage": {
                "id": "4131a7eb-2652-416d-80d6-956edae3349a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7538588a-14e4-4c2a-bf6b-6e39fc444cf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6250ec29-ebfa-4148-849d-d68864e2b85c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7538588a-14e4-4c2a-bf6b-6e39fc444cf6",
                    "LayerId": "01165fb5-9418-4744-979e-e4a7928e3992"
                }
            ]
        },
        {
            "id": "8de7755b-2228-454a-ad03-cbacabefe67f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9817a441-b974-459f-9505-8737e0c1c1b2",
            "compositeImage": {
                "id": "9d174791-79c5-4405-9a08-5be06432ce29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8de7755b-2228-454a-ad03-cbacabefe67f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ea6c96-b26a-473d-bf47-b3676a6c1b34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de7755b-2228-454a-ad03-cbacabefe67f",
                    "LayerId": "01165fb5-9418-4744-979e-e4a7928e3992"
                }
            ]
        },
        {
            "id": "1ef9721d-258e-4f06-8356-bd394f7995f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9817a441-b974-459f-9505-8737e0c1c1b2",
            "compositeImage": {
                "id": "b6034f38-153d-4826-b4dd-3cb95b919b11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ef9721d-258e-4f06-8356-bd394f7995f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "865ba956-c24d-4cd4-afbe-632fc07b469d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ef9721d-258e-4f06-8356-bd394f7995f7",
                    "LayerId": "01165fb5-9418-4744-979e-e4a7928e3992"
                }
            ]
        },
        {
            "id": "51164005-0214-4538-842e-203378033e20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9817a441-b974-459f-9505-8737e0c1c1b2",
            "compositeImage": {
                "id": "ec47f031-a357-4615-a4b5-546610dbaec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51164005-0214-4538-842e-203378033e20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "930264b8-ef80-4a50-a673-70c40f77696f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51164005-0214-4538-842e-203378033e20",
                    "LayerId": "01165fb5-9418-4744-979e-e4a7928e3992"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "01165fb5-9418-4744-979e-e4a7928e3992",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9817a441-b974-459f-9505-8737e0c1c1b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 40,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
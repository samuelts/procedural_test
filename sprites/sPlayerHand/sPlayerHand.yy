{
    "id": "c13ed4e2-9b76-439c-b807-361e12a0dfe3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerHand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5ea5b20-6311-4b29-ac12-302277b90b8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c13ed4e2-9b76-439c-b807-361e12a0dfe3",
            "compositeImage": {
                "id": "9e076f31-0c78-40ab-81a3-8886b51947ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ea5b20-6311-4b29-ac12-302277b90b8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1792d4c2-34b1-4ad7-a72f-232a9d15907e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ea5b20-6311-4b29-ac12-302277b90b8b",
                    "LayerId": "3f4bebf2-16ac-4518-9180-59f3808547ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "3f4bebf2-16ac-4518-9180-59f3808547ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c13ed4e2-9b76-439c-b807-361e12a0dfe3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}
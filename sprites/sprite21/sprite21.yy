{
    "id": "c491ff75-136c-4f2f-a71e-8df2372c0618",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b90cf06-c801-41ee-b22b-48b474602cc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c491ff75-136c-4f2f-a71e-8df2372c0618",
            "compositeImage": {
                "id": "cc194b9a-405d-487e-a3e7-b4864fe1e755",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b90cf06-c801-41ee-b22b-48b474602cc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be3a62a5-ed3b-4bbe-94dd-ac9e9e539f6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b90cf06-c801-41ee-b22b-48b474602cc8",
                    "LayerId": "1f3dea51-7998-4d6c-92ef-05636e093c92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1f3dea51-7998-4d6c-92ef-05636e093c92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c491ff75-136c-4f2f-a71e-8df2372c0618",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
{
    "id": "4f7d4ded-993c-4a6a-a23b-e831fe8fbcd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOgreWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3765c397-86c1-4c18-a771-9a2631d39b40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7d4ded-993c-4a6a-a23b-e831fe8fbcd5",
            "compositeImage": {
                "id": "4a82c563-6427-4a1b-8733-9e42be402bf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3765c397-86c1-4c18-a771-9a2631d39b40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2438ec46-3e75-459e-bbdd-d1d2373a1182",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3765c397-86c1-4c18-a771-9a2631d39b40",
                    "LayerId": "6ba2a20d-b891-44ac-bf4b-932b27d17050"
                }
            ]
        },
        {
            "id": "543185c2-fb4c-40ef-853e-07e6bb9ec609",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7d4ded-993c-4a6a-a23b-e831fe8fbcd5",
            "compositeImage": {
                "id": "be53939f-eb03-4bee-9802-3d0d380def23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "543185c2-fb4c-40ef-853e-07e6bb9ec609",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f653067-18cf-4edf-9775-a4e5ff8d7863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "543185c2-fb4c-40ef-853e-07e6bb9ec609",
                    "LayerId": "6ba2a20d-b891-44ac-bf4b-932b27d17050"
                }
            ]
        },
        {
            "id": "d6b89260-5d36-4fa1-aff1-6353da1f51f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7d4ded-993c-4a6a-a23b-e831fe8fbcd5",
            "compositeImage": {
                "id": "dda1ca2f-cd71-47c0-959c-9844e6d5d95b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6b89260-5d36-4fa1-aff1-6353da1f51f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd2f36c2-a1f4-4185-9791-072b8d5cf5c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6b89260-5d36-4fa1-aff1-6353da1f51f9",
                    "LayerId": "6ba2a20d-b891-44ac-bf4b-932b27d17050"
                }
            ]
        },
        {
            "id": "dddd9136-0f5b-4b6c-879b-9bf7f413e4e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7d4ded-993c-4a6a-a23b-e831fe8fbcd5",
            "compositeImage": {
                "id": "2e3f6783-d92c-40bb-a3b7-df1ccac3c94e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dddd9136-0f5b-4b6c-879b-9bf7f413e4e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4dc54dd-7e1f-4e57-a9dc-343859ed1f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dddd9136-0f5b-4b6c-879b-9bf7f413e4e2",
                    "LayerId": "6ba2a20d-b891-44ac-bf4b-932b27d17050"
                }
            ]
        },
        {
            "id": "9d84fd5b-0a4f-401f-b27d-17fbfe07aa2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7d4ded-993c-4a6a-a23b-e831fe8fbcd5",
            "compositeImage": {
                "id": "cf412ace-0349-4ae2-8829-1b2163e7f6af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d84fd5b-0a4f-401f-b27d-17fbfe07aa2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "820f7f49-e9bf-4ab6-870a-a0ba6e95d128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d84fd5b-0a4f-401f-b27d-17fbfe07aa2d",
                    "LayerId": "6ba2a20d-b891-44ac-bf4b-932b27d17050"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6ba2a20d-b891-44ac-bf4b-932b27d17050",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f7d4ded-993c-4a6a-a23b-e831fe8fbcd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
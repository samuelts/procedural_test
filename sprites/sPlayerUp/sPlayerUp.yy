{
    "id": "39723e30-06ea-487d-a9e8-2c49176ada31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3435841-4f4f-4a71-8ad5-ccc5683812d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39723e30-06ea-487d-a9e8-2c49176ada31",
            "compositeImage": {
                "id": "5f7b0635-3671-4dae-9bbc-69d64bb7ba62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3435841-4f4f-4a71-8ad5-ccc5683812d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d389d611-66c4-40f5-9841-7c67ec7aff7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3435841-4f4f-4a71-8ad5-ccc5683812d8",
                    "LayerId": "f0a6be27-8402-4e94-9ac2-64a284f06773"
                }
            ]
        },
        {
            "id": "76065a0f-880d-43a4-ad90-6e7c73f1dcda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39723e30-06ea-487d-a9e8-2c49176ada31",
            "compositeImage": {
                "id": "febec1f8-469d-469e-b8d7-f165849244ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76065a0f-880d-43a4-ad90-6e7c73f1dcda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47d52e5c-9ed3-43fa-92f3-a7b96c420178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76065a0f-880d-43a4-ad90-6e7c73f1dcda",
                    "LayerId": "f0a6be27-8402-4e94-9ac2-64a284f06773"
                }
            ]
        },
        {
            "id": "e25bacc4-f57e-4b9a-8d60-08069696e319",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39723e30-06ea-487d-a9e8-2c49176ada31",
            "compositeImage": {
                "id": "d2861359-1d42-4679-a1f3-0780ca7b3218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e25bacc4-f57e-4b9a-8d60-08069696e319",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78f8705b-b65c-4b6d-8a9f-866c9f76e599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e25bacc4-f57e-4b9a-8d60-08069696e319",
                    "LayerId": "f0a6be27-8402-4e94-9ac2-64a284f06773"
                }
            ]
        },
        {
            "id": "a22609f2-4f1e-4bc7-b6ee-020b135a7ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39723e30-06ea-487d-a9e8-2c49176ada31",
            "compositeImage": {
                "id": "02969107-ded8-41e5-99f8-13abed08194e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a22609f2-4f1e-4bc7-b6ee-020b135a7ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dd361be-bc95-4209-8287-95f28c11cfe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a22609f2-4f1e-4bc7-b6ee-020b135a7ecf",
                    "LayerId": "f0a6be27-8402-4e94-9ac2-64a284f06773"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f0a6be27-8402-4e94-9ac2-64a284f06773",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39723e30-06ea-487d-a9e8-2c49176ada31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 8
}
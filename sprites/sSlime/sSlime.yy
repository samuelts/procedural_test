{
    "id": "e1276d53-3c4a-4ba9-ba57-0d0e4fc7d8fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlime",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8deb348a-b5a8-4f90-a1e9-712b26fa1894",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1276d53-3c4a-4ba9-ba57-0d0e4fc7d8fa",
            "compositeImage": {
                "id": "4c404766-8bea-4f3f-803b-1babfa715bf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8deb348a-b5a8-4f90-a1e9-712b26fa1894",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "191e4ba2-8c14-4f21-af19-3e118000484a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8deb348a-b5a8-4f90-a1e9-712b26fa1894",
                    "LayerId": "2d1f84d6-145b-40f6-af29-7b654757ee8b"
                }
            ]
        },
        {
            "id": "4baf05b3-543e-4dd3-9bd0-76c13dd00a5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1276d53-3c4a-4ba9-ba57-0d0e4fc7d8fa",
            "compositeImage": {
                "id": "17501832-3006-4591-accf-29232f5cdc48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4baf05b3-543e-4dd3-9bd0-76c13dd00a5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51564ee7-8995-4afb-acb1-4c08075c84d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4baf05b3-543e-4dd3-9bd0-76c13dd00a5e",
                    "LayerId": "2d1f84d6-145b-40f6-af29-7b654757ee8b"
                }
            ]
        },
        {
            "id": "e4a24cf7-6a08-4d1a-bbbf-6eaeceae15d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1276d53-3c4a-4ba9-ba57-0d0e4fc7d8fa",
            "compositeImage": {
                "id": "611fb17d-b5e1-4d32-90b4-0157b37ab507",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a24cf7-6a08-4d1a-bbbf-6eaeceae15d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4611e7d7-2794-4e0a-b900-6471b5b34759",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a24cf7-6a08-4d1a-bbbf-6eaeceae15d3",
                    "LayerId": "2d1f84d6-145b-40f6-af29-7b654757ee8b"
                }
            ]
        },
        {
            "id": "7a138ec1-6ddc-4224-86b5-8cab1db6416b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1276d53-3c4a-4ba9-ba57-0d0e4fc7d8fa",
            "compositeImage": {
                "id": "8398b897-9720-401d-8b02-b62461f6e5c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a138ec1-6ddc-4224-86b5-8cab1db6416b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a50320f-0309-4492-942e-40146a646f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a138ec1-6ddc-4224-86b5-8cab1db6416b",
                    "LayerId": "2d1f84d6-145b-40f6-af29-7b654757ee8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2d1f84d6-145b-40f6-af29-7b654757ee8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1276d53-3c4a-4ba9-ba57-0d0e4fc7d8fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
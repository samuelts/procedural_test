{
    "id": "b3db1d21-5480-4d86-bae5-f496b2585f65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sOgreCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b60245e8-44e1-48e5-9e58-5ca170be32c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3db1d21-5480-4d86-bae5-f496b2585f65",
            "compositeImage": {
                "id": "ab8d555b-7529-4e55-a527-36039bbb49f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b60245e8-44e1-48e5-9e58-5ca170be32c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "486b1517-bfd8-47c8-a6e1-07e98adb7505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b60245e8-44e1-48e5-9e58-5ca170be32c6",
                    "LayerId": "8cc8594a-01ac-48da-9121-1394b5562bda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8cc8594a-01ac-48da-9121-1394b5562bda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3db1d21-5480-4d86-bae5-f496b2585f65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
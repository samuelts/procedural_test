{
    "id": "684a11bf-1d97-403d-a28e-33bb4c7c431f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeavyJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09551dd0-97e2-464d-b0f6-b992b82759e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "684a11bf-1d97-403d-a28e-33bb4c7c431f",
            "compositeImage": {
                "id": "cf28acc3-7d24-464d-adcd-d222a18a24fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09551dd0-97e2-464d-b0f6-b992b82759e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7e98b98-13ce-4f3a-8843-7a6c391f947c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09551dd0-97e2-464d-b0f6-b992b82759e6",
                    "LayerId": "72eba3cc-e92b-4af3-a41f-23e2bec8200c"
                }
            ]
        },
        {
            "id": "0de632a7-162e-457a-a4e3-60c518638e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "684a11bf-1d97-403d-a28e-33bb4c7c431f",
            "compositeImage": {
                "id": "cf04313d-a726-4f73-bfa5-4a53fff04920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0de632a7-162e-457a-a4e3-60c518638e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50fc5627-8ef7-49d3-9de4-98a12f8d05b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de632a7-162e-457a-a4e3-60c518638e1e",
                    "LayerId": "72eba3cc-e92b-4af3-a41f-23e2bec8200c"
                }
            ]
        },
        {
            "id": "a54ad1ad-7640-45b4-8b1c-523583102e52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "684a11bf-1d97-403d-a28e-33bb4c7c431f",
            "compositeImage": {
                "id": "f899a4d3-a84f-4efb-9883-0732e0ca3bf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a54ad1ad-7640-45b4-8b1c-523583102e52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f064484d-6046-49ad-8fe0-f44c9fafe9c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a54ad1ad-7640-45b4-8b1c-523583102e52",
                    "LayerId": "72eba3cc-e92b-4af3-a41f-23e2bec8200c"
                }
            ]
        },
        {
            "id": "44054a6b-e9ec-4b5a-a511-3c6386df90e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "684a11bf-1d97-403d-a28e-33bb4c7c431f",
            "compositeImage": {
                "id": "203bd090-1c4e-4d79-a4a8-ecb02f850f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44054a6b-e9ec-4b5a-a511-3c6386df90e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "731d0143-d24e-441b-883d-d1382bd4e0ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44054a6b-e9ec-4b5a-a511-3c6386df90e7",
                    "LayerId": "72eba3cc-e92b-4af3-a41f-23e2bec8200c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "72eba3cc-e92b-4af3-a41f-23e2bec8200c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "684a11bf-1d97-403d-a28e-33bb4c7c431f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 63
}
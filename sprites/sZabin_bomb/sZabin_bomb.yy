{
    "id": "432f8d03-68dd-4c8d-a75d-fc4150655562",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sZabin_bomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 413,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98ae0c4c-a99b-481e-9125-8fe397a0daec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "432f8d03-68dd-4c8d-a75d-fc4150655562",
            "compositeImage": {
                "id": "8ccc6a00-cb06-4d50-a8a0-0f8ed2ef7cb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ae0c4c-a99b-481e-9125-8fe397a0daec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc2fae4-0b95-4ed3-b216-95f6bc32f7af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ae0c4c-a99b-481e-9125-8fe397a0daec",
                    "LayerId": "9f30c977-d09a-4303-ac2f-c48bbcfd4eb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9f30c977-d09a-4303-ac2f-c48bbcfd4eb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "432f8d03-68dd-4c8d-a75d-fc4150655562",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 416,
    "xorig": 82,
    "yorig": 103
}
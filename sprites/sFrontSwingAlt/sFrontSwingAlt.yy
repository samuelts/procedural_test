{
    "id": "e2335faf-9deb-4f12-b123-aa7ec14c0f9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFrontSwingAlt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 5,
    "bbox_right": 57,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b448c77d-8a22-4bab-b499-3fb6369a83e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2335faf-9deb-4f12-b123-aa7ec14c0f9f",
            "compositeImage": {
                "id": "3164cacd-aa8b-49f1-be0e-186c29bae4a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b448c77d-8a22-4bab-b499-3fb6369a83e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89b29b74-d349-4c33-b738-793ceb9da05a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b448c77d-8a22-4bab-b499-3fb6369a83e3",
                    "LayerId": "7785b996-f94a-4ddc-88ed-22b95e421809"
                }
            ]
        },
        {
            "id": "7e70b2ed-09f7-4d67-91ad-79307f511574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2335faf-9deb-4f12-b123-aa7ec14c0f9f",
            "compositeImage": {
                "id": "f654250c-0342-4d12-9a1a-047327e93a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e70b2ed-09f7-4d67-91ad-79307f511574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e45f831-88ea-4422-a1b0-6e29205f2cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e70b2ed-09f7-4d67-91ad-79307f511574",
                    "LayerId": "7785b996-f94a-4ddc-88ed-22b95e421809"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7785b996-f94a-4ddc-88ed-22b95e421809",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2335faf-9deb-4f12-b123-aa7ec14c0f9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
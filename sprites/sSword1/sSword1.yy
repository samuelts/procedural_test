{
    "id": "7193e23f-33c0-44de-be1b-bc838cc541db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSword1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4525428b-ba70-449c-934d-c2d1c7aa7f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7193e23f-33c0-44de-be1b-bc838cc541db",
            "compositeImage": {
                "id": "a224072f-e1cd-445a-a80c-56f0f8f0d7f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4525428b-ba70-449c-934d-c2d1c7aa7f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0735eba-a78a-4d19-b1e6-2c3b0aa15311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4525428b-ba70-449c-934d-c2d1c7aa7f8d",
                    "LayerId": "506d3ea9-dee2-433a-aaef-39d11b98d924"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "506d3ea9-dee2-433a-aaef-39d11b98d924",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7193e23f-33c0-44de-be1b-bc838cc541db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 3,
    "yorig": 8
}
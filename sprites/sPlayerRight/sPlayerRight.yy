{
    "id": "5bb788e9-1cfc-40ad-9867-9c6f38e786fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "653c9ced-0e71-48c1-b5dd-268b449b71a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb788e9-1cfc-40ad-9867-9c6f38e786fa",
            "compositeImage": {
                "id": "7a7b15e8-071a-410a-81f6-a97eb23dbadb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "653c9ced-0e71-48c1-b5dd-268b449b71a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c56dd88-e603-47cd-ad80-d8c7fdc5fb32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "653c9ced-0e71-48c1-b5dd-268b449b71a6",
                    "LayerId": "48e862be-93a6-4c6e-969d-8391d20def10"
                }
            ]
        },
        {
            "id": "edebc09e-596d-4fe4-aca5-d84f8f68c226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb788e9-1cfc-40ad-9867-9c6f38e786fa",
            "compositeImage": {
                "id": "fa484b14-3a36-4f06-ac51-f5528d28990e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edebc09e-596d-4fe4-aca5-d84f8f68c226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10f01fc8-bcc2-4519-965f-01348659ae78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edebc09e-596d-4fe4-aca5-d84f8f68c226",
                    "LayerId": "48e862be-93a6-4c6e-969d-8391d20def10"
                }
            ]
        },
        {
            "id": "88649f94-1c65-42a4-a2de-067a9cce56c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb788e9-1cfc-40ad-9867-9c6f38e786fa",
            "compositeImage": {
                "id": "20eee3cd-d5c6-4ed4-9530-5f198e01f632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88649f94-1c65-42a4-a2de-067a9cce56c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd56bad3-9cd8-4f5a-bcbd-d40f3eb3e77b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88649f94-1c65-42a4-a2de-067a9cce56c8",
                    "LayerId": "48e862be-93a6-4c6e-969d-8391d20def10"
                }
            ]
        },
        {
            "id": "5e57269c-b934-4a0d-b2df-2b5f9dc85ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb788e9-1cfc-40ad-9867-9c6f38e786fa",
            "compositeImage": {
                "id": "9c427f5d-8e86-470c-8c04-127dd4115e42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e57269c-b934-4a0d-b2df-2b5f9dc85ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80739580-cd44-426c-aacf-7e027cf9e7af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e57269c-b934-4a0d-b2df-2b5f9dc85ea9",
                    "LayerId": "48e862be-93a6-4c6e-969d-8391d20def10"
                }
            ]
        },
        {
            "id": "fe8ea9bf-ec5a-45fd-9a03-36463da1e4b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb788e9-1cfc-40ad-9867-9c6f38e786fa",
            "compositeImage": {
                "id": "6d33a961-63d2-4e86-be87-4bb70a695d45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe8ea9bf-ec5a-45fd-9a03-36463da1e4b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03b99deb-d0fa-4360-9ba5-a0ace9274e8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe8ea9bf-ec5a-45fd-9a03-36463da1e4b4",
                    "LayerId": "48e862be-93a6-4c6e-969d-8391d20def10"
                }
            ]
        },
        {
            "id": "9b48dae0-0026-4979-838a-b58aab022924",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb788e9-1cfc-40ad-9867-9c6f38e786fa",
            "compositeImage": {
                "id": "32b63c18-a72a-41e7-aaee-9a71ae0b6278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b48dae0-0026-4979-838a-b58aab022924",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9974e46d-bbf6-49e8-8320-48a1c40c4a23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b48dae0-0026-4979-838a-b58aab022924",
                    "LayerId": "48e862be-93a6-4c6e-969d-8391d20def10"
                }
            ]
        },
        {
            "id": "0119d32c-92a5-4177-8804-78a54ab62db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb788e9-1cfc-40ad-9867-9c6f38e786fa",
            "compositeImage": {
                "id": "840af017-33d1-4474-839b-405e73d74261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0119d32c-92a5-4177-8804-78a54ab62db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af1e0d04-7c96-4269-a1d4-b6d066d451cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0119d32c-92a5-4177-8804-78a54ab62db5",
                    "LayerId": "48e862be-93a6-4c6e-969d-8391d20def10"
                }
            ]
        },
        {
            "id": "92fcaff3-407a-4b09-9651-9440595a20a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb788e9-1cfc-40ad-9867-9c6f38e786fa",
            "compositeImage": {
                "id": "5eccd2c4-2b2e-4c2c-8763-125bcabbaaaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92fcaff3-407a-4b09-9651-9440595a20a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9dfd19b-e256-4daf-b2a6-cfd412c52a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92fcaff3-407a-4b09-9651-9440595a20a1",
                    "LayerId": "48e862be-93a6-4c6e-969d-8391d20def10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "48e862be-93a6-4c6e-969d-8391d20def10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bb788e9-1cfc-40ad-9867-9c6f38e786fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 8
}
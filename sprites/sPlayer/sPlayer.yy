{
    "id": "77e46f03-7f38-4177-a330-240eb9061572",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a77dd8b-6d60-4a57-8c08-e053fbd20bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e46f03-7f38-4177-a330-240eb9061572",
            "compositeImage": {
                "id": "1c91aa85-651a-401b-a5fe-e09d7d7ca58e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a77dd8b-6d60-4a57-8c08-e053fbd20bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fda3581-611d-4fe0-b948-1ace500b94b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a77dd8b-6d60-4a57-8c08-e053fbd20bed",
                    "LayerId": "ff7089fb-2d3b-43e0-9767-cfff541cf956"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ff7089fb-2d3b-43e0-9767-cfff541cf956",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77e46f03-7f38-4177-a330-240eb9061572",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
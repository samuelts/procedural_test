{
    "id": "02692e2c-fc0f-4104-8301-b7fe9ed4b429",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ec483e5-53a5-41c2-b399-e53126ace365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02692e2c-fc0f-4104-8301-b7fe9ed4b429",
            "compositeImage": {
                "id": "ce7cb48e-51a2-4d26-af4e-4c861a6b537b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec483e5-53a5-41c2-b399-e53126ace365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7a779db-ba2e-424f-b453-fd08b6a578ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec483e5-53a5-41c2-b399-e53126ace365",
                    "LayerId": "2cd3e678-d7cb-413e-b4e4-091cf189aa39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "2cd3e678-d7cb-413e-b4e4-091cf189aa39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02692e2c-fc0f-4104-8301-b7fe9ed4b429",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 1,
    "yorig": 1
}
{
    "id": "8dbc3729-948c-4a00-9d8f-bc5e594a262c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01607348-de17-4b03-afae-92abcad0a89b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dbc3729-948c-4a00-9d8f-bc5e594a262c",
            "compositeImage": {
                "id": "728d082d-fad2-44da-b550-a89a55a0fdd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01607348-de17-4b03-afae-92abcad0a89b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70008612-bbca-4d3b-abd6-b2658c619ddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01607348-de17-4b03-afae-92abcad0a89b",
                    "LayerId": "cc196464-e763-4606-b752-a2f258d144b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cc196464-e763-4606-b752-a2f258d144b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dbc3729-948c-4a00-9d8f-bc5e594a262c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
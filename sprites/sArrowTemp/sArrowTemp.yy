{
    "id": "41956611-ac23-48bb-860a-90e4e942fcd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sArrowTemp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b73afaf-9041-4110-a32e-7de8a4e6ce10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41956611-ac23-48bb-860a-90e4e942fcd3",
            "compositeImage": {
                "id": "99cecfd9-5e1a-426c-ae9a-8d908f2a317e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b73afaf-9041-4110-a32e-7de8a4e6ce10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa7c128c-71bd-41f7-a379-e24ae2738374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b73afaf-9041-4110-a32e-7de8a4e6ce10",
                    "LayerId": "45ad9400-054b-4eb8-a064-4437de9779df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "45ad9400-054b-4eb8-a064-4437de9779df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41956611-ac23-48bb-860a-90e4e942fcd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 5,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 7,
    "yorig": 4
}
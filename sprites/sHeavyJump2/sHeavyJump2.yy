{
    "id": "38f9700b-0e62-4edc-9fcc-05364f673a70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeavyJump2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "470f360a-dbde-4353-8473-f7d5f59761ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38f9700b-0e62-4edc-9fcc-05364f673a70",
            "compositeImage": {
                "id": "72183c34-b91c-4ff0-9339-3587b8d9191a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "470f360a-dbde-4353-8473-f7d5f59761ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b35b745-fdad-4661-abff-a217c04c51ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "470f360a-dbde-4353-8473-f7d5f59761ba",
                    "LayerId": "1b9fdc8f-05e8-4aa4-b8be-c834ba066c04"
                }
            ]
        },
        {
            "id": "533ca154-d1fb-4855-b317-44cc070e5846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38f9700b-0e62-4edc-9fcc-05364f673a70",
            "compositeImage": {
                "id": "fad4c017-e5d9-4953-8591-49364dc31fd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "533ca154-d1fb-4855-b317-44cc070e5846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db053592-332f-4c8d-a1ed-f249bc9e0f50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "533ca154-d1fb-4855-b317-44cc070e5846",
                    "LayerId": "1b9fdc8f-05e8-4aa4-b8be-c834ba066c04"
                }
            ]
        },
        {
            "id": "2cf41c60-1709-45f6-b777-d33bc39664a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38f9700b-0e62-4edc-9fcc-05364f673a70",
            "compositeImage": {
                "id": "bdbb06f7-e760-4cc5-8159-7faf3d226666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cf41c60-1709-45f6-b777-d33bc39664a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "163fe350-a4bf-4615-9293-59b5e05dff22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cf41c60-1709-45f6-b777-d33bc39664a5",
                    "LayerId": "1b9fdc8f-05e8-4aa4-b8be-c834ba066c04"
                }
            ]
        },
        {
            "id": "bd4d1c20-45e2-43f5-a51e-dc90a9db706b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38f9700b-0e62-4edc-9fcc-05364f673a70",
            "compositeImage": {
                "id": "04e16705-4404-4cd3-b759-2facec3e4e7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd4d1c20-45e2-43f5-a51e-dc90a9db706b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aeffe70-3aaa-4dd9-87c5-c3b63fca68e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd4d1c20-45e2-43f5-a51e-dc90a9db706b",
                    "LayerId": "1b9fdc8f-05e8-4aa4-b8be-c834ba066c04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1b9fdc8f-05e8-4aa4-b8be-c834ba066c04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38f9700b-0e62-4edc-9fcc-05364f673a70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 63
}
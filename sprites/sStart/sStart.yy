{
    "id": "780d1a63-019a-4202-87c3-9e610c1f32ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ba06c99-abc8-4e82-a226-28f0798fcf7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780d1a63-019a-4202-87c3-9e610c1f32ac",
            "compositeImage": {
                "id": "e249c217-8f14-4261-8352-dec65a3ad693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ba06c99-abc8-4e82-a226-28f0798fcf7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cda52f3-df3d-49f9-92e1-b725616ff480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ba06c99-abc8-4e82-a226-28f0798fcf7c",
                    "LayerId": "6d5ba94a-b24f-4981-a9e0-55a9b5bbfb50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6d5ba94a-b24f-4981-a9e0-55a9b5bbfb50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "780d1a63-019a-4202-87c3-9e610c1f32ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
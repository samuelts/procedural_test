{
    "id": "12249b72-fac2-4489-b0de-d619b395f76d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGreatsword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e83d80e-d34c-4e20-8065-4b3d0581ed6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12249b72-fac2-4489-b0de-d619b395f76d",
            "compositeImage": {
                "id": "6b853cd5-feb4-4116-a598-d3b90b6759b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e83d80e-d34c-4e20-8065-4b3d0581ed6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9908d309-17f4-4f89-95dc-64a2eaf5986e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e83d80e-d34c-4e20-8065-4b3d0581ed6a",
                    "LayerId": "ea08ae6c-3fb3-47d9-9c59-8463b5a9b91a"
                }
            ]
        },
        {
            "id": "c6883fa5-28cb-4bf2-81c5-cdeed85a5e4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12249b72-fac2-4489-b0de-d619b395f76d",
            "compositeImage": {
                "id": "4ab1d7de-8885-422b-815e-9e425eddcafd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6883fa5-28cb-4bf2-81c5-cdeed85a5e4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e6f0171-ed01-492f-9d7d-49c3f56fab4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6883fa5-28cb-4bf2-81c5-cdeed85a5e4a",
                    "LayerId": "ea08ae6c-3fb3-47d9-9c59-8463b5a9b91a"
                }
            ]
        },
        {
            "id": "d590e92e-5ba0-4fba-806e-2b5f7f951efc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12249b72-fac2-4489-b0de-d619b395f76d",
            "compositeImage": {
                "id": "ccc7b81d-a01c-4951-ab26-abeaf853534e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d590e92e-5ba0-4fba-806e-2b5f7f951efc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5efc6a04-cb07-4a8e-ba2e-b4c12d7762ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d590e92e-5ba0-4fba-806e-2b5f7f951efc",
                    "LayerId": "ea08ae6c-3fb3-47d9-9c59-8463b5a9b91a"
                }
            ]
        },
        {
            "id": "b3d4ac0d-834b-43e9-aa68-d318dd7982b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12249b72-fac2-4489-b0de-d619b395f76d",
            "compositeImage": {
                "id": "bb301130-97d1-47f8-be8c-4102d535de0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d4ac0d-834b-43e9-aa68-d318dd7982b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "100f3e4d-8383-47e3-8ef0-55415ad685fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d4ac0d-834b-43e9-aa68-d318dd7982b8",
                    "LayerId": "ea08ae6c-3fb3-47d9-9c59-8463b5a9b91a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ea08ae6c-3fb3-47d9-9c59-8463b5a9b91a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12249b72-fac2-4489-b0de-d619b395f76d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}
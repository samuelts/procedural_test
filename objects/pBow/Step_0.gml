var _mouse_dir = point_direction(x,y,mouse_x,mouse_y)

x = oPlayerHand.x;
y = oPlayerHand.y;

visible = false;

firingdelay--;

//Shoot arrow
if (mouse_check_button_pressed(mb_right) && firingdelay < 0 && arrowCount > 0) {
	firingdelay = 25;
	with (instance_create_layer(x,y,"Weapons",oArrowDisp)) {
		speed = 15;
		direction = _mouse_dir;
		image_angle = _mouse_dir;
	}
	arrowCount--;
}

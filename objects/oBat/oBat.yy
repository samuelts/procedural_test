{
    "id": "69687a4b-c264-4c45-ae94-a57e2b4be679",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBat",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "fc237e64-425d-4375-8fc9-9681da088d05",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "b04dea41-401c-4700-894d-964898cc5ceb",
            "propertyId": "a2a53b8f-e304-40ec-9735-9178507df706",
            "value": "5"
        }
    ],
    "parentObjectId": "b04dea41-401c-4700-894d-964898cc5ceb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a3af73d2-92d8-43c9-a547-fad40b40b227",
    "visible": true
}
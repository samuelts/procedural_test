/// @desc Player GUI

font_size = font_get_size(fMenu);

w = display_get_gui_width();
h = display_get_gui_height();

DrawSetText(c_red,fMenu,fa_left,fa_top);

draw_text(25,25,"HP: " + string(hp));

DrawSetText(c_blue,fMenu,fa_left,fa_top);

draw_text(25,25 + font_size * 1.5,"MP: " + string(mana));

DrawSetText(c_white,fMenu,fa_left,fa_top);

draw_text(25,50 + font_size*1.5, "Floor: " + string(global.floorNum));

DrawSetText(c_green,fMenu,fa_left,fa_top);

draw_text(25,75 + font_size*1.5, "Arrows: " + string(pBow.arrowCount));

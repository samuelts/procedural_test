/// @desc Player object create event
image_speed = 0;

velocity = [0,0];
max_velocity = [1.7, 1.7];
max_speed = 1.7;
acceleration = 1.1;

// Get tilemap id
var _layer_id = layer_get_id("WallTiles");
collision_tile_map_id = layer_tilemap_get_id(_layer_id);


instance_create_layer(x,y,"Hand",oPlayerHand);
instance_create_layer(x,y,"Hand",oCursor);
instance_create_layer(x,y,"Weapons",pBow);
instance_create_layer(x,y,"Instances",oCamera);

hp = global.playerHP;
mana = global.playerMana;
pScore = global.playerScore;
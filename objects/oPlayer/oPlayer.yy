{
    "id": "8a393b40-7c88-4e24-9a1d-23d1d798d90c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "ea412b00-1736-4c60-b572-5b729bfdf40d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a393b40-7c88-4e24-9a1d-23d1d798d90c"
        },
        {
            "id": "cb59795a-a9d2-4d7c-aef2-7c32503e5ddf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8a393b40-7c88-4e24-9a1d-23d1d798d90c"
        },
        {
            "id": "a472ea5f-1463-48ad-a234-6a72223ae145",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "8a393b40-7c88-4e24-9a1d-23d1d798d90c"
        },
        {
            "id": "e917969e-6583-42ed-85c4-74200da557d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8a393b40-7c88-4e24-9a1d-23d1d798d90c"
        },
        {
            "id": "b5ef330f-713a-4fd0-977a-2c4d84660244",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "8a393b40-7c88-4e24-9a1d-23d1d798d90c"
        },
        {
            "id": "bad1e4ea-ab81-4f0e-93ef-a128d9e9f6fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "8a393b40-7c88-4e24-9a1d-23d1d798d90c"
        }
    ],
    "maskSpriteId": "8dbc3729-948c-4a00-9d8f-bc5e594a262c",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "f7d86864-6919-4353-a7cf-991e0ca31e49",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "3",
            "varName": "hp",
            "varType": 0
        },
        {
            "id": "1234fb14-ed7e-4d0c-9d7c-628febe24c93",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "25",
            "varName": "mana",
            "varType": 0
        },
        {
            "id": "61e24a55-0b33-46b5-850b-cdd01fa5890f",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "hascontrol",
            "varType": 3
        },
        {
            "id": "7818722a-d284-461a-b7fe-8d71194c7bfe",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "False",
            "varName": "setHeavyAttack",
            "varType": 3
        },
        {
            "id": "10cd078b-2065-44ee-896f-194cc60f974a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "flash",
            "varType": 0
        },
        {
            "id": "55e6e84f-4071-4b74-bdc9-1a8eb6aeb58d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "pScore",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "77e46f03-7f38-4177-a330-240eb9061572",
    "visible": true
}
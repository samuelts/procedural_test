/// @desc Player Object Step

var _x_input = (keyboard_check(ord("D")) - keyboard_check(ord("A"))) * acceleration;
var _y_input = (keyboard_check(ord("S")) - keyboard_check(ord("W"))) * acceleration;

// Vector variables
var _vector2x = 0;
var _vector2y = 1;

// Horizontal movement
velocity[_vector2x] = clamp(velocity[_vector2x] + _x_input, -max_velocity[_vector2x], max_velocity[_vector2x]);

// Horizontal movement
velocity[_vector2y] = clamp(velocity[_vector2y] + _y_input, -max_velocity[_vector2y], max_velocity[_vector2y]);

// Friction
if (_x_input = 0) {
	velocity[_vector2x] = lerp(velocity[_vector2x], 0, 0.2);	
}
if (_y_input = 0) {
	velocity[_vector2y] = lerp(velocity[_vector2y], 0, 0.2);	
}

//Normalize speed in 360 degrees
var _speed = point_distance(0, 0, velocity[_vector2x], velocity[_vector2y]);
var _direction = point_direction(0, 0, velocity[_vector2x], velocity[_vector2y]);
if (_speed > max_speed) {
	velocity[_vector2x] = lengthdir_x(max_speed, _direction);
	velocity[_vector2y] = lengthdir_y(max_speed, _direction);
}

// Move and contact tiles
move_and_contact_tiles(collision_tile_map_id, 32, velocity);


// Flip player image if mouse on left
if (sign(mouse_x - x) < 0) {
	rightysign = -1;
} else {
	rightysign = 1;
}

// Direction variable for external calls
if (velocity[_vector2x] > 0) {
	playerdir = 1;
} else if (velocity[_vector2x] < 0) {
	playerdir = -1
} else {
	playerdir = 0;
}

//Animation
if (_x_input = 0) && (_y_input = 0) {
	sprite_index = sPlayer;
	image_speed = 0;
	image_index = 0;
	image_xscale = rightysign;
} else if (_x_input = 0) {
	if (_y_input > 0) {
		sprite_index = sPlayerDown;
		image_speed = 1;
	} else {
		sprite_index = sPlayerUp;
		image_speed = 1;
	}
} else if (_y_input = 0) {
	if (_x_input > 0) {
		sprite_index = sPlayerRight;
		image_xscale = 1;
		image_speed = 1;
	} else {
		sprite_index = sPlayerRight;
		image_xscale = -1;
		image_speed = 1;
	}
}
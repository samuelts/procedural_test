{
    "id": "2c5443cc-2a29-40fd-b435-8a6069d2da6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSlime",
    "eventList": [
        {
            "id": "6251db20-829c-456f-9505-b5df061c54e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2c5443cc-2a29-40fd-b435-8a6069d2da6f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "317f94d3-00c1-40dd-9361-bfb4d319971c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "b04dea41-401c-4700-894d-964898cc5ceb",
            "propertyId": "a2a53b8f-e304-40ec-9735-9178507df706",
            "value": "15"
        }
    ],
    "parentObjectId": "b04dea41-401c-4700-894d-964898cc5ceb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1276d53-3c4a-4ba9-ba57-0d0e4fc7d8fa",
    "visible": true
}
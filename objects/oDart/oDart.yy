{
    "id": "64bcc7f5-5a2b-4b6e-9088-a5ffeeb8ffc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDart",
    "eventList": [
        {
            "id": "d30146ca-ab75-44e4-a2a3-a8de41655e58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "64bcc7f5-5a2b-4b6e-9088-a5ffeeb8ffc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "cca9e1c5-ad1b-4e37-af18-45e376b380fc",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "812b8de9-a883-40d0-93ff-cbbf6638a6d4",
            "propertyId": "3fcbbedb-7d84-4b7a-a527-f5eea3eaf532",
            "value": "2"
        }
    ],
    "parentObjectId": "812b8de9-a883-40d0-93ff-cbbf6638a6d4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f2be4f1d-d0b0-4f86-b9b1-8e51f94b16d1",
    "visible": true
}
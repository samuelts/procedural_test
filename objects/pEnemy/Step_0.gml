
/// @desc Parent mob object step event

// Vector variables
var _vector2x = 0;
var _vector2y = 1;

t++;
//depth = -y;

if(t >= 60) {
	dir = choose(0,90,180,270);
	t = 0;
}

//Mob movement params
var _horimove = 0;
var _vertmove = 0;

velocity[_vector2x] += velocity_x_frac;
velocity[_vector2y] += velocity_y_frac;

velocity_x_frac = velocity[_vector2x] - (floor(abs(velocity[_vector2x])) * sign(velocity[_vector2x]));
velocity[_vector2x] -= velocity_x_frac;
velocity_y_frac = velocity[_vector2y] - (floor(abs(velocity[_vector2y])) * sign(velocity[_vector2y]));
velocity[_vector2y] -= velocity_y_frac;

if (point_distance(x,y,oPlayer.x,oPlayer.y) < 50) {
	temp_aggro = true;
}if (point_distance(x,y,oPlayer.x,oPlayer.y) > 100) {
	temp_aggro = false;
}
if (!stun) {
	if (aggro || temp_aggro) {
		get_path_to_point(oPlayer.x,oPlayer.y);
		max_velocity_mod = 2;
	} else {
		switch(dir) {
			case 0:
				_horimove = acceleration;
				break;
			case 90:
				_vertmove = -acceleration;
				break;
			case 180:
				_horimove = -acceleration;
				break;
			case 270:
			default:
				_vertmove = acceleration;
				break;
		}
		max_velocity_mod = 1;
	}

	velocity[_vector2x] = clamp(velocity[_vector2x] + _horimove, -max_velocity[_vector2x], max_velocity[_vector2x]);
	velocity[_vector2y] = clamp(velocity[_vector2y] + _vertmove, -max_velocity[_vector2y], max_velocity[_vector2y]);

	// Move and contact tiles
	move_and_contact_tiles(collision_tile_map_id, 32, velocity);

	//If stationary pick a new direction
	if (velocity[_vector2x] == 0) && (velocity[_vector2y] == 0) {
		newdir = dir;
		while (newdir == dir) {
			newdir = choose(0,90,180,270);
		}
		dir = newdir;
	}
	
} else {
	image_speed = 0;
	stun_timer--;
	path_end();
	if (stun_timer % 60 == 0) {
		with instance_create_layer(x,y+25,"Hand",oStunNum) {
			stunNum = (other.stun_timer / 60) + 1;
		}
	}
	if (stun_timer <= 0) {
		stun_timer = 300;
		stun = false;
		image_speed = 1;
		get_path_to_point(oPlayer.x,oPlayer.y);
	}
}

//Player collision
if(place_meeting(x,y,oPlayer)) {	
	if (collision_time >= 60) {
		with (oPlayer) {
			dmg = other.dmg;
			hp-=dmg;
			flash = 3;
			with instance_create_layer(x,y+25,"Hand",oDamageNum) {
				damageNum = other.dmg;
			}
		}
		collision_time = 0;
	} else {
		collision_time++;
	}
} else {
	collision_time = 60;
}

//Animation
image_speed = 1;

if (velocity[_vector2x] != 0) {
	image_xscale = -sign(velocity[_vector2x]);
}

if (x != xprevious) {
	image_xscale = sign(x - xprevious);
}


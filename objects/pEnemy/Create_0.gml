/// @desc Parent mob object create event

// Get tilemap id
var _layer_id = layer_get_id("WallTiles");
collision_tile_map_id = layer_tilemap_get_id(_layer_id);

// Initialize variables
dir = choose(0,90,180,270);
velocity = [0,0];
max_velocity = [0.2, 0.2];
max_velocity_mod = 1;
acceleration = 1.1;

velocity_x_frac = 0;
velocity_y_frac = 0;

//depth = -y;
path = path_add();
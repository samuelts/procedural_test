{
    "id": "ed87031a-5730-4eab-a572-d9ae4dd0275a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSkeleton",
    "eventList": [
        {
            "id": "eed0b3ab-e171-42d7-bf00-932735e1c136",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ed87031a-5730-4eab-a572-d9ae4dd0275a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        
    ],
    "parentObjectId": "b04dea41-401c-4700-894d-964898cc5ceb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "8aceee6d-1327-49d4-8848-0355af030d53",
    "visible": true
}
{
    "id": "db7914d3-cfd4-4294-b6b1-07f40d758fdb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLevelGen",
    "eventList": [
        {
            "id": "0b898cc0-2e71-4597-92ac-4393fdeab5c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "db7914d3-cfd4-4294-b6b1-07f40d758fdb"
        },
        {
            "id": "19527bbc-9389-490f-b9eb-db91ba48d302",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "db7914d3-cfd4-4294-b6b1-07f40d758fdb"
        },
        {
            "id": "d1f3c607-c4e6-4425-87af-5ad9b83d8dad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "db7914d3-cfd4-4294-b6b1-07f40d758fdb"
        },
        {
            "id": "494506be-dab9-4190-8e5e-033128a2a6af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "db7914d3-cfd4-4294-b6b1-07f40d758fdb"
        },
        {
            "id": "81d2d4e0-14e0-4362-99cf-4f1ab9805126",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "db7914d3-cfd4-4294-b6b1-07f40d758fdb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "937308c9-8ccd-4836-96b8-a06aadd62519",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "False",
            "varName": "playerMade",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
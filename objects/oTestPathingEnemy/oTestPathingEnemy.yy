{
    "id": "7a83b6d5-e7e5-41ed-a4fa-31c37ebb2c29",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTestPathingEnemy",
    "eventList": [
        {
            "id": "0d040f15-62d9-41b9-ab0f-a390003ef507",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7a83b6d5-e7e5-41ed-a4fa-31c37ebb2c29"
        },
        {
            "id": "464f41c9-9a23-428a-af0e-f976d44d3ec5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7a83b6d5-e7e5-41ed-a4fa-31c37ebb2c29"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "9d3614cd-f778-41f8-b2b1-aee2ad91a909",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.2",
            "varName": "maxspd",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "c491ff75-136c-4f2f-a71e-8df2372c0618",
    "visible": true
}
{
    "id": "341d85eb-bd74-4e20-af52-2f60079b3660",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oArrow",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "3b84353e-e355-437e-a364-481433caa835",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "812b8de9-a883-40d0-93ff-cbbf6638a6d4",
            "propertyId": "3fcbbedb-7d84-4b7a-a527-f5eea3eaf532",
            "value": "1"
        }
    ],
    "parentObjectId": "812b8de9-a883-40d0-93ff-cbbf6638a6d4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "41956611-ac23-48bb-860a-90e4e942fcd3",
    "visible": true
}
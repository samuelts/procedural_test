{
    "id": "0f98994a-6c48-4564-b65d-13db0afe1419",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oOgre",
    "eventList": [
        {
            "id": "b1c61ea3-87f0-44b3-8c30-653803a5d900",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f98994a-6c48-4564-b65d-13db0afe1419"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "fb388a99-9113-4fbd-9498-68f28a59c396",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "b04dea41-401c-4700-894d-964898cc5ceb",
            "propertyId": "a2a53b8f-e304-40ec-9735-9178507df706",
            "value": "20"
        }
    ],
    "parentObjectId": "b04dea41-401c-4700-894d-964898cc5ceb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "4f7d4ded-993c-4a6a-a23b-e831fe8fbcd5",
    "visible": true
}
{
    "id": "b1902ee3-a57f-4fa4-81de-0d399c218697",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCursor",
    "eventList": [
        {
            "id": "eaf13561-91b9-49f3-871a-8a63035876b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b1902ee3-a57f-4fa4-81de-0d399c218697"
        },
        {
            "id": "cfd55438-e188-4689-8629-6db0e0d8dcc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1902ee3-a57f-4fa4-81de-0d399c218697"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "02692e2c-fc0f-4104-8301-b7fe9ed4b429",
    "visible": true
}
/// @description Insert description here
// You can write your code in this editor

x = mouse_x;
y = mouse_y;

x = clamp(x,oPlayer.x - 10, oPlayer.x + 10);
y = clamp(y,oPlayer.y - 3, oPlayer.y + 7);

if (mouse_check_button_pressed(mb_left)) {
	if (place_meeting(x,y,pEnemy)) {
		with instance_place(x,y,pEnemy) {
			dmg = other.dmg;
			hp-=dmg;
			flash = 3;
			with instance_create_layer(x,y+25,"Hand",oDamageNum) {
				damageNum = other.dmg;
			}
		}
	}
}

with (oPlayer) {
	if (sprite_index == sPlayerRight) || (sprite_index == sPlayerUp) || (sprite_index == sPlayerDown) {
		other.visible = false;
	} else {
		other.visible = true;
	}
}
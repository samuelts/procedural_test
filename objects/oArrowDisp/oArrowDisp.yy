{
    "id": "6da864f5-3c7b-4584-80b2-2241bc0aed09",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oArrowDisp",
    "eventList": [
        {
            "id": "04d31e50-a747-4e16-a8c1-2813199a8fd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6da864f5-3c7b-4584-80b2-2241bc0aed09"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "7fe34572-63fb-4a20-8e5e-8d133f3f0a2c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "812b8de9-a883-40d0-93ff-cbbf6638a6d4",
            "propertyId": "3fcbbedb-7d84-4b7a-a527-f5eea3eaf532",
            "value": "1"
        }
    ],
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "e79b97ac-d03b-4764-a1d9-02ff87e6948b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "dmg",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "41956611-ac23-48bb-860a-90e4e942fcd3",
    "visible": true
}
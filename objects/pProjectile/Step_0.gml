var _dist_to_player = point_distance(x,y,oPlayer.x,oPlayer.y);
var _mouse_dir = point_direction(x,y,mouse_x,mouse_y)

xTarget = oPlayerHand.x;
yTarget = oPlayerHand.y;

visible = false;

// Vector variables
var _vector2x = 0;
var _vector2y = 1;

//Throw arrow
if (mouse_check_button(mb_right)) {
	visible = true;
	thrown = true;
	active = true;
	dir_angle = _mouse_dir;
	throw_velocity = [15*cos(dir_angle*pi/180),15*sin(dir_angle*pi/180)];
	velocity = [0,0];
	speed = 15;
	direction = image_angle;
}

if (active) {
	//Enemy collision
	if(place_meeting(x,y,pEnemy)) {	
		if (collision_time >= 60) {
			with (instance_place(x,y,pEnemy)) {
				aggro = true;
				dmgRec = other.dmg;
				hp-=dmgRec;
				special_effect = true;
				flash = 3;
				with instance_create_layer(x,y+25,"Hand",oDamageNum) {
					damageNum = other.dmgRec;
				}
			}
			collision_time = 0;
		} else {
			collision_time++;
		}
	} else {
		collision_time = 60;
	}
	
	if (point_distance(x,y,xTarget,yTarget) < 15) {
		active = false;	
	}
}

if (thrown) {
	t++;
	if (t > 60) {
		thrown = false;
		t = 0;
		speed = 0;
		direction = 0;
	}
} else {
	if (x != xTarget) {
		x += (xTarget-x)/10;
	}

	if (y != yTarget) {
		y += (yTarget-y)/10
	}
	image_angle = _mouse_dir;
}
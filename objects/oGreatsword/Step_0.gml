/// @desc sword 1 object
swingdelay--;

if (sprite_index = sGreatsword) {
	x = oPlayerHand.x;
	y = oPlayerHand.y;
} else {
	x = oPlayer.x;
	y = oPlayer.y;	
}

//image_angle = point_direction(x,y,mouse_x,mouse_y);

if (oPlayer.image_xscale < 0) {
	image_xscale = 1;
} else {
	image_xscale = -1;
}

image_angle = point_direction(x,y,mouse_x,mouse_y)-90;

if (mouse_check_button_pressed(mb_left) && swingdelay < 0) {
	swingdelay = 30;
	
	if (place_meeting(x,y,pEnemy)) {
		with instance_place(x,y,pEnemy) {
			dmgRec = other.dmg;
			hp-=dmgRec;
			flash = 3;
			with instance_create_layer(x,y+25,"Hand",oDamageNum) {
				damageNum = other.dmgRec;
			}
		}
	}
	
}
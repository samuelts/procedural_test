{
    "id": "b8449d9b-bf83-41f9-bafd-353ea99a599b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGreatsword",
    "eventList": [
        {
            "id": "eac9b0c0-dc45-4ed5-81f7-75120142b34f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b8449d9b-bf83-41f9-bafd-353ea99a599b"
        },
        {
            "id": "b2af6401-3349-41b2-8c1d-d9a07f76c54b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "b8449d9b-bf83-41f9-bafd-353ea99a599b"
        },
        {
            "id": "a531bc49-2644-4850-b31f-3bc370a9a651",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8449d9b-bf83-41f9-bafd-353ea99a599b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "2b2319fb-0ce2-4161-8938-bf603bbb7401",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "4",
            "varName": "dmg",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "12249b72-fac2-4489-b0de-d619b395f76d",
    "visible": true
}
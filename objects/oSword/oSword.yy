{
    "id": "0a4b92a6-73af-4528-8dd0-3644800cee62",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSword",
    "eventList": [
        {
            "id": "95950c96-33be-4630-b05d-79f54ecd54ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0a4b92a6-73af-4528-8dd0-3644800cee62"
        },
        {
            "id": "ad43582c-d932-45e0-87a3-53f4f863e908",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a4b92a6-73af-4528-8dd0-3644800cee62"
        },
        {
            "id": "0698d252-1cf2-43c7-8b9c-eacac904bdb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "0a4b92a6-73af-4528-8dd0-3644800cee62"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "cd017953-2c70-45f5-8e36-acf801f8fc53",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2",
            "varName": "dmg",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "7193e23f-33c0-44de-be1b-bc838cc541db",
    "visible": true
}
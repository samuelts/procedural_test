/// @desc Generate a new floor
if (keyboard_check_released(vk_space)) {
	with (oPlayer) {
		global.playerHP = hp;
		global.playerMana = mana;
		global.playerScore = pScore;
	}
	with (pBow) {
		global.arrowCount = arrowCount;
	}
	global.floorNum++;
	room_restart();
}
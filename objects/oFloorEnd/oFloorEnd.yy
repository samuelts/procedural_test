{
    "id": "bfcaf237-647e-4209-bd4c-cb0b8f7205cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFloorEnd",
    "eventList": [
        {
            "id": "81993fcd-a08a-4136-a742-687768078305",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8a393b40-7c88-4e24-9a1d-23d1d798d90c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bfcaf237-647e-4209-bd4c-cb0b8f7205cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f506ee51-fd62-4c00-a9de-9a6fd4990372",
    "visible": true
}
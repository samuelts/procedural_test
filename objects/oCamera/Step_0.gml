/// @description Update Camera

//Update destination
if (!instance_exists(follow)) exit;

x = lerp(x, follow.x, 0.1);
y = lerp(y, follow.y, 0.1);

//Keep camera inside room
x = clamp(x,view_w_half+CELL_WIDTH,room_width-view_w_half-CELL_WIDTH);
y = clamp(y,view_h_half+CELL_HEIGHT,room_height-view_h_half-CELL_HEIGHT);

x += random_range(-shake_remain,shake_remain);
y += random_range(-shake_remain,shake_remain);
shake_remain = max(0,shake_remain-((1/shake_length)*shake_magnitude));

//Update camera view
camera_set_view_pos(cam, x-view_w_half, y-view_h_half);
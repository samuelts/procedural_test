{
    "id": "410ce8cd-5c76-4bb9-8e5f-ec8bd49e60a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDamageNum",
    "eventList": [
        {
            "id": "632aff94-d95d-4da8-b834-8b80bf6010ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "410ce8cd-5c76-4bb9-8e5f-ec8bd49e60a0"
        },
        {
            "id": "81d0541b-0a77-4f50-bf4b-543da7b01a7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "410ce8cd-5c76-4bb9-8e5f-ec8bd49e60a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
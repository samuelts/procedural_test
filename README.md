# procedural_test
An exercise in creating a game around a procedural level generation algorithm using a grid data structure


# Current Status
Procedural level generation is complete with switches for randomness, 48-tile bitmasking for the walls, and 16-tile bitmasking for the floors.

# Images
Player View:
![Player View](https://i.imgur.com/XyK8HRP.png)

Higher Randomness:
![Level Generated 1](https://i.imgur.com/VC1cPAQ.png)

![Level Generated 2](https://i.imgur.com/LwkB4TD.png)

![Level Generated 3](https://i.imgur.com/jE7pWKV.png)

![Level Generated 4](https://i.imgur.com/oXpGAiS.png)

![Level Generated 5](https://i.imgur.com/eLoIU6Q.png)

Lower Randomness:
![Level Generated 6](https://i.imgur.com/V0FOat6.png)

![Level Generated 7](https://i.imgur.com/mHLVaDD.png)

![Level Generated 8](https://i.imgur.com/xI7w6K6.png)

![Level Generated 9](https://i.imgur.com/2Y08eL4.png)
